<h1>Image processing and video streaming C library</h1>

<h2>Build not functional for gui module which is in update to complain to new intrusive list architecture</h2>

<h3>Build instructions:</h3>
<p>
$ cd [cloned git repository]<br/>
$ mkdir build               <br/>
$ cmake .. [options...]     <br/>
$ make                      <br/>
$ sudo make install         <br/>
</p>

<h3>CMake options</h3>
<h4>Architecture option</h4>
<p>
-DBUILD_ARM32=[ON/OFF] (Default OFF) : Build for armv7 (hf)<br/>
-DBUILD_ARM64=[ON/OFF] (Default OFF) : Build for aarch64<br/>
</p>
<h4>Optional optimisations:</h4>
<p>
-DMULTI_THREAD=[ON/OFF] (Default OFF) : Enable multithreading optimisations<br/>
</p>
<h4>Unit tests powerred by ctest</h4>
<p>
-DBUILD_TESTS=[ON/OFF] (Default OFF) : Build unit and regression tests<br/>
-DSAVE_TESTS_CSV=[ON/OFF] (Default OFF) : Save tests results in csv files<br/>
-DVERBOSE_TESTS=[ON/OFF] (Default ON) : Print verbose trace for tests<br/>
Build all unit tests: $ make tests<br/>
Run all unit tests: $ make check<br/>
</p>
<h4>Benchmarks</h4>
<p>
-DBUILD_BENCHMARKS=[ON/OFF] (Default OFF) : Build performance benchmarks<br/>
-DBENCH_ALWAYS_PRINT_HEADERS=[ON/OFF] (Default OFF) : Print legend of benchmarks table for each benchmark<br/>
-DSAVE_BECNHMARKS_CSV=[ON/OFF] (Default OFF) : Save benchmarks results in csv files<br/>
Build all benchmarks: $ make bench<br/>
Run all unit benchmarks: $ make check_bench<br/>
</p>
<h4>Samples (non automatic tests)</h4>
<p>
-DBUILD_SAMPLES=[ON/OFF] (Default OFF) : Build non automatic tests<br/>
Build all samples: $ make samples<br/>
</p>
<h4>Documentation</h4>
<p>
-DBUILD_DOC=[ON/OFF] (Default OFF) : Build documentation<br/>
Build doc: $ make doc
</p>

<h3>Folders</h3>
<p>
-include: c headers<br/>
-src: c sources<br/>
-test: units tests c sources<br/>
-bench: benchmarks c sources<br/>
-samples: non automatic tests and examples<br/>
-data_test: images used for tests<br/>
-xktest: Framework for tests and microbenchmarking<br/>
</p>

<h3>Dependancies</h3>
<p>
-SDL2<br/>
-SDL2_ttf<br/>
-libpcre<br/>
</p>

<h3>Install dependancies</h3>
<h4>Via APT (debian and ubuntu only)</h4>
<p> [xk_home] # bash script/debian_deps.sh </p>
<h4>Via compilation (all others)</h4>
<p> [xk_home] $ bash script/build_deps.sh </p>

<h3>Packages generation</h3>
<h4>APT packages</h4>
<p> [xk_home] $ bash script/build_deb_pkg.sh </p>
<h4>CPack packages</h4>
<p> [xk_build] $ cpack </p>

<h3>Functionalities</h3>
<h4>Image processing</h4>
<p>
-Sigma-delta algorithm (motion detector)<br/>
-Morphology filters<br/>
-Bilinear interpolation<br/>
-color to grayscale and grayscale to color conversion<br/>
</p>
<h4>Image codecs</h4>
<p>
-PGM<br/>
-PPM<br/>
</p>
<h4>Network</h4>
<p>
-TCP/IP<br/>
-UDP/IP<br/>
-Image streaming via UDP<br/>
</p>
<h4>Gui</h4>
<p>
-Minimalist GUI using SDL2<br/>
</p>
<h4>Testing (xktest)</h4>
<p>
-Units tests<br/>
-Microbenchmarking<br/>
</p>