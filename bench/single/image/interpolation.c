#include <xk/image/blit.h>
#include <xk/image/color.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xk/image/interpolation.h>
#include <xktest/bench.h>

int main(int argc, char** argv)
{
  uint8_t **in, **out;
  uint32_t w,h;
  float factor = 0.5; 
  w = 2048;
  h = 2048;
  xkt_benchmark_args(argc, argv);
  in  = xk_u8_matrix(0, w-1, 0, h-1);
  out = xk_u8_matrix(0, w-1, 0, h-1);
  XKT_BENCHMARK( "bilinear interpolation"
               , 5
               , 4
               , w*h
               , 1
               , xk_bilinear_interpolation(in, out, w, h, factor));
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(out, 0, 0);
}
