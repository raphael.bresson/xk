#include <xk/image/morph.h>
#include <xk/image/matrix.h>
#include <xktest/bench.h>
#include <xk/gpu/image/morph.h>
#include <xk/gpu/image/run.h>

int main(int argc, char** argv)
{
  uint32_t w, h;
  w = 8064;
  h = 768;
  uint8_t **in, **out0, **out1;
  xkt_benchmark_args(argc, argv);
  in   = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out0 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out1 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  xk_gpu_env_t* env = xk_gpu_env_default();
  xk_gpu_task_t* task = xk_gpu_dilate_task(env, in, out1, w, h, 1, 1);
  cl_command_queue queue = xk_gpu_command_queue(env->context, env->device);
  XKT_BENCHMARK_VS( "morph gpu"
                  , 4
                  , 5
                  , w*h
                  , 1
                  , xk_u8_dilate_3x3_scalar_no_padding(in, out0, w, h)
                  , xk_gpu_image_run(task, queue, in, out1));
  xk_gpu_command_queue_del(queue);
  xk_gpu_task_del(task);
  xk_gpu_env_del(env);
  xk_u8_matrix_del(in,0,0);
  xk_u8_matrix_del(out0,0,0);
  xk_u8_matrix_del(out1,0,0);
  return 0;
}
