#include <xk/image/sigma_delta.h>
#include <xk/image/matrix.h>
#include <xktest/bench.h>
#include <xk/gpu/image/sigma_delta.h>
#include <xk/gpu/image/run.h>

int main(int argc, char** argv)
{
  uint32_t w, h;
  w = 8064;
  h = 768;
  xk_sigma_delta_config sg_conf;
  uint8_t **in, **out1;
  xkt_benchmark_args(argc, argv);
  in   = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out1 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  xk_sigma_delta_config_init(&sg_conf, in, 4, w, h);
  xk_gpu_env_t* env = xk_gpu_env_default();
  xk_gpu_sigma_delta_task_t* task = xk_gpu_sigma_delta_task(env, in, out1, w, h, 4);
  cl_command_queue queue = xk_gpu_command_queue(env->context, env->device);
  XKT_BENCHMARK_VS( "sigma-delta gpu"
                  , 4
                  , 5
                  , w*h
                  , 1
                  , xk_sigma_delta_scalar(in, &sg_conf)
                  , xk_gpu_image_run(task, queue, in, out1));
  xk_gpu_command_queue_del(queue);
  xk_gpu_sigma_delta_task_del(task);
  xk_gpu_env_del(env);
  xk_u8_matrix_del(in,0,0);
  xk_u8_matrix_del(out1,0,0);
  return 0;
}
