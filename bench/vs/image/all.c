#include <xk/image/matrix.h>
#include <xk/image/sigma_delta.h>
#include <xk/image/morph.h>
#include <xk/image/color.h>

#include <xktest/bench.h>

void with_padding(uint32_t w, int32_t h)
{
  uint8_t **in, **ing0, **ing1, **e0, **e1, **out0, **out1;
  struct sigma_delta_config conf_scal, conf_simd;
  in   = xk_u8_matrix(0, w*3-1, 0, h-1);
  ing0 = xk_u8_matrix(0, w-1  , 0, h-1);
  ing1 = xk_u8_matrix(0, w-1  , 0, h-1);
  out0 = xk_u8_matrix(-16, w+15  , -1, h);
  out1 = xk_u8_matrix(-16, w+15  , -1, h);
  e0   = xk_u8_matrix(-16, w+15  , -1, h);
  e1   = xk_u8_matrix(-16, w+15  , -1, h);
  xk_sigma_delta_config_ext_out_init(&conf_scal,ing0, e0,4, w, h);
  xk_sigma_delta_config_ext_out_init(&conf_simd,ing1, e1,4, w, h);
  
  XKT_BENCHMARK_VS( "all"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , { xk_rgb_to_gray_scalar  (in, ing0, w*3, h);
                      xk_sigma_delta_scalar  (ing0, &conf_scal);
                      xk_u8_erode_3x3_scalar (e0, out0, w, h);
                      xk_u8_dilate_3x3_scalar(out0, e0, w, h);
                      xk_u8_dilate_3x3_scalar(e0, out0, w, h);
                      xk_u8_erode_3x3_scalar (out0, e0, w, h);
                    }
                  , {  xk_rgb_to_gray  (in, ing1, w*3, h);
                       xk_sigma_delta  (ing1, &conf_simd);
                       xk_u8_erode_3x3 (e1, out1, w, h);
                       xk_u8_dilate_3x3(out1, e1, w, h);
                       xk_u8_dilate_3x3(e1, out1, w, h);
                       xk_u8_erode_3x3 (out1, e1, w, h);
                    });
  
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(ing0, 0, 0);
  xk_u8_matrix_del(ing1, 0, 0);
  xk_u8_matrix_del(out0, -16, -1);
  xk_u8_matrix_del(out1, -16, -1);
  xk_u8_matrix_del(e0, -16, -1);
  xk_u8_matrix_del(e1, -16, -1);
  xk_sigma_delta_config_ext_out_del(&conf_scal);
  xk_sigma_delta_config_ext_out_del(&conf_simd);
}


void without_padding(uint32_t w, int32_t h)
{
  uint8_t **in, **ing0, **ing1, **out0, **out1, **ext_etq0, **ext_etq1;
  struct sigma_delta_config conf_scal, conf_simd;
  in   = xk_u8_matrix(0, w*3-1, 0, h-1);
  ing0 = xk_u8_matrix(0, w-1  , 0, h-1);
  ing1 = xk_u8_matrix(0, w-1  , 0, h-1);
  out0 = xk_u8_matrix(0, w-1  , 0, h-1);
  out1 = xk_u8_matrix(0, w-1  , 0, h-1);
  ext_etq0 = xk_u8_matrix(0, w-1  , 0, h-1);
  ext_etq1 = xk_u8_matrix(0, w-1  , 0, h-1);
  xk_sigma_delta_config_ext_out_init(&conf_scal, ext_etq0,ing0,4, w, h);
  xk_sigma_delta_config_ext_out_init(&conf_simd, ext_etq1,ing1,4, w, h);
  
  XKT_BENCHMARK_VS( "all no padding"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , { xk_rgb_to_gray_scalar  (in, ing0, w*3, h);
                      xk_sigma_delta_scalar  (ing0, &conf_scal);
                      xk_u8_erode_3x3_scalar_no_padding (conf_scal.etq, out0, w, h);
                      xk_u8_dilate_3x3_scalar_no_padding(out0, conf_scal.etq, w, h);
                      xk_u8_dilate_3x3_scalar_no_padding(conf_scal.etq, out0, w, h);
                      xk_u8_erode_3x3_scalar_no_padding (out0, conf_scal.etq, w, h);
                    }
                  , {  xk_rgb_to_gray  (in, ing1, w*3, h);
                       xk_sigma_delta  (ing1, &conf_simd);
                       xk_u8_erode_3x3_no_padding (conf_simd.etq, out1, w, h);
                       xk_u8_dilate_3x3_no_padding(out1, conf_simd.etq, w, h);
                       xk_u8_dilate_3x3_no_padding(conf_simd.etq, out1, w, h);
                       xk_u8_erode_3x3_no_padding (out1, conf_simd.etq, w, h);
                    });
  
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(ing0, 0, 0);
  xk_u8_matrix_del(ing1, 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
  xk_sigma_delta_config_ext_out_del(&conf_scal);
  xk_sigma_delta_config_ext_out_del(&conf_simd);
  xk_u8_matrix_del(ext_etq0, 0, 0);
  xk_u8_matrix_del(ext_etq1, 0, 0);
}

int main(int argc, char** argv)
{
  uint32_t w, h;
  w = 2048;
  h = 2048;
  xkt_benchmark_args(argc, argv);
  with_padding(w,h);
  xkt_benchmark_init(0, NULL);
  without_padding(w,h);
  return 0;
}
