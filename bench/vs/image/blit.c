#include <xk/image/blit.h>
#include <xk/image/color.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/bench.h>

int main(int argc, char** argv)
{
  uint8_t **in, **ing, **out0, **out1;
  uint32_t w,h;
  w = 2048;
  h = 2048;
  xkt_benchmark_args(argc, argv);
  in   = xk_u8_matrix(0, w*3-1, 0, h-1);
  ing  = xk_u8_matrix(0, w-1  , 0, h-1);
  out0 = xk_u8_matrix(0, w*3-1, 0, h-1);
  out1 = xk_u8_matrix(0, w*3-1, 0, h-1);
  XKT_BENCHMARK_VS( "blit"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_blit_binary_to_rgb_scalar(in, ing, out0, 0x00, 0xFF, 0x00, w, h)
                  , xk_blit_binary_to_rgb(in, ing, out0, 0x00, 0xFF, 0x00, w, h));
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(ing, 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
}
