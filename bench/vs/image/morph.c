#include <xk/image/morph.h>
#include <xk/image/matrix.h>
#include <xktest/bench.h>

void with_padding(uint32_t w, uint32_t h)
{
  uint8_t **in, **out0, **out1;
  in   = xk_u8_matrix(-16, w+15 , -1, h  );
  out0 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out1 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  XKT_BENCHMARK_VS( "morph"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_u8_dilate_3x3_scalar(in, out0, w, h)
                  , xk_u8_dilate_3x3(in, out1, w, h));
  xk_u8_matrix_del(in, -16, -1);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
}


void without_padding(uint32_t w, uint32_t h)
{
  uint8_t **in, **out0, **out1;
  in   = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out0 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  out1 = xk_u8_matrix(0  , w-1  , 0 , h-1);
  XKT_BENCHMARK_VS( "morph no padding"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_u8_dilate_3x3_scalar_no_padding(in, out0, w, h)
                  , xk_u8_dilate_3x3_no_padding(in, out1, w, h));
  xk_u8_matrix_del(in  , 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
}

int main(int argc, char** argv)
{
  uint32_t w, h;
  w = 2048;
  h = 2048;
  xkt_benchmark_args(argc, argv);
  with_padding(w,h);
  xkt_benchmark_init(0, NULL);
  without_padding(w,h);
  return 0;
}
