#include <xk/image/color.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/bench.h>

int main(int argc, char** argv)
{
  uint8_t **in, **out0, **out1;
  uint32_t w,h;
  w = 2048;
  h = 2048;
  xkt_benchmark_args(argc, argv);
  #ifdef __ARM_NEON__
  uint8_t **out2;
  #endif
  in   = xk_u8_matrix(0, w*3-1, 0, h-1);
  out0 = xk_u8_matrix(0, w-1, 0, h-1);
  out1 = xk_u8_matrix(0, w-1, 0, h-1);
  #ifdef __ARM_NEON__
  out2 = xk_u8_matrix(0, w-1, 0, h-1);
  #endif
  XKT_BENCHMARK_VS( "rgb2gray intrin"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_rgb_to_gray_scalar(in, out0, w*3, h)
                  , xk_rgb_to_gray(in, out1, w*3, h));
  #ifdef __ARM_NEON__
  XKT_BENCHMARK_VS( "rgb2gray asm"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_rgb_to_gray_scalar(in, out0, w*3, h)
                  , xk_rgb_to_gray_asm(in, out2, w*3, h));
  #endif
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
  #ifdef __ARM_NEON__
  xk_u8_matrix_del(out2, 0, 0);
  #endif
  return 0;
}
