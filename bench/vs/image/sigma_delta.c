#include <xk/image/morph.h>
#include <xk/image/matrix.h>
#include <xk/image/sigma_delta.h>
#include <xktest/bench.h>

#define N 2

int main(int argc, char** argv)
{
  uint8_t **in;
  uint32_t w, h;
  xkt_benchmark_args(argc, argv);
  w = 2048;
  h = 2048;
  in   = xk_u8_matrix(0, w-1 , 0, h-1);
  struct sigma_delta_config conf_scal, conf_simd;
  xk_sigma_delta_config_init(&conf_scal,in,N, w, h);
  xk_sigma_delta_config_init(&conf_simd,in,N, w, h);
  XKT_BENCHMARK_VS( "sigma-delta"
                  , 5
                  , 4
                  , w*h
                  , 1
                  , xk_sigma_delta_scalar(in, &conf_scal)
                  , xk_sigma_delta(in, &conf_simd));
  xk_u8_matrix_del(in, 0, 0);
  xk_sigma_delta_config_del(&conf_scal);
  xk_sigma_delta_config_del(&conf_simd);
  return 0;
}
