#ifndef XK_CONTAINER_LIST_H
#define XK_CONTAINER_LIST_H

#include <xk/common.h>

#ifdef __cplusplus
extern "C" {
#endif

struct xk_list_node
{
  void                *userdata;
  struct xk_list_node *prev;
  struct xk_list_node *next;
  struct xk_list      *parent;
};

struct xk_list
{
  struct xk_list_node *begin;
  struct xk_list_node *end;
  size_t               count;
};

static inline void xk_list_node_init(struct xk_list_node *node, void *userdata)
{
  node->userdata = userdata;
  node->prev = node->next = NULL;
  node->parent = NULL;
}

static inline void xk_list_init(struct xk_list *list)
{
  list->begin = list->end = NULL;
  list->count = 0;
}

void xk_list_append( struct xk_list      *list
                   , struct xk_list_node *node);

void xk_list_prepend( struct xk_list * list
                    , struct xk_list_node *node);

void xk_list_remove( struct xk_list * list
                   , struct xk_list_node * node);

void * xk_list_pop(struct xk_list *list);

#define xk_list_is_empty(list) ((list)->begin == NULL)

#define xk_list_foreach_node(list, node) \
  for((node) = (list)->begin ; (node) ; (node) = (node)->next)

static inline void *xk_list_next( struct xk_list_node **node)
{
  if(!(*node)->next)
  {
    *node = NULL;
    return NULL;
  }
  *node = (*node)->next;
  return (*node)->userdata;
}

static inline void *xk_list_prev( struct xk_list_node **node)
{
  if(!(*node)->prev)
  {
    *node = NULL;
    return NULL;
  }
  *node = (*node)->prev;
  return (*node)->userdata;
}

#define xk_list_foreach(list, elem, node) \
    for( node = (list)->begin, elem = node->userdata \
       ; node != NULL \
       ; elem = xk_list_next(&node))

#define xk_list_rforeach(list, elem, node) \
    for( node = (list)->end, elem = node->userdata \
       ; node != NULL \
       ; elem = xk_list_prev(&node))


void xk_list_node_rec( struct xk_list_node *node
                      , void (*func)( void *listdata
                                    , void *userdata)
                      , void *userdata);

void xk_list_node_rrec( struct xk_list_node *node
                      , void (*func)( void *listdata
                                    , void *userdata)
                      , void *userdata);

static inline void xk_list_rec( struct xk_list * list
                              , void (*func)( void *listdata
                                            , void *userdata)
                              , void *userdata)
{
  xk_list_node_rec(list->end,func,userdata);
}

static inline void xk_list_rrec( struct xk_list * list
                               , void (*func)( void *listdata
                                             , void *userdata)
                               , void *userdata)
{
  xk_list_node_rrec(list->begin,func,userdata);
}

#ifdef __cplusplus
}
#endif

#endif
