#ifndef XK_CONTAINER_QUAD_TREE_H_INCLUDED
#define XK_CONTAINER_QUAD_TREE_H_INCLUDED

#include <xk/container/list.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>

#ifdef __cplusplus
extern "C" {
#endif

struct xk_quad_tree_node
{
  struct xk_quad_tree *container;
  struct xk_quad_tree_node *parent;
  struct xk_quad_tree_node *ne;
  struct xk_quad_tree_node *se;
  struct xk_quad_tree_node *sw;
  struct xk_quad_tree_node *nw;
  size_t x;
  size_t y;
  size_t w;
  size_t h;
  struct xk_list data_list;
  int is_leaf;
};

struct xk_quad_tree
{
  struct xk_quad_tree_node *head;
  struct xk_quad_tree_node *node_collection;
  size_t depth;
  size_t nnodes;
};

int xk_quad_tree_init( struct xk_quad_tree * tree
                      , size_t depth, size_t w, size_t h);

void xk_quad_tree_release(struct xk_quad_tree *tree);

void xk_quad_tree_append( struct xk_quad_tree *tree
                        , struct xk_list_node *node
                        , size_t x, size_t y
                        , size_t w, size_t h);

void xk_quad_tree_foreach( struct xk_quad_tree *tree
                         , int (*func)( void *datalist
                                      , void *userdata)
                         , void *userdata);

struct xk_quad_tree_node *xk_quad_tree_find( struct xk_quad_tree *tree
                                           , size_t x, size_t y
                                           , size_t w, size_t h);

void xk_quad_tree_remove( struct xk_quad_tree *tree
                        , struct xk_list_node *node
                        , size_t x, size_t y, size_t w, size_t h);

void xk_quad_tree_print( struct xk_quad_tree *tree
                       , void (*print_func)(FILE *stream, void *data)
                       , FILE *stream);

#ifdef __cplusplus
}
#endif

#endif
