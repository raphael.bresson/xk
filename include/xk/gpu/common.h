#ifndef XK_GPU_COMMON_H_INCLUDED
#define XK_GPU_COMMON_H_INCLUDED

#include <CL/cl.h>
#include <xk/common.h>

cl_platform_id* xk_gpu_platforms(cl_uint* count);

cl_device_id* xk_gpu_devices( cl_platform_id platform
                            , cl_uint*       count);

cl_context xk_gpu_context( cl_platform_id platform
                         , cl_device_id*  devices
                         , cl_uint        num_devices);

cl_mem xk_gpu_buffer( cl_context context
                    , void*      data
                    , size_t   size
                    , cl_uint    flags);

cl_program xk_gpu_program( cl_context    context
                         , cl_device_id* devices
                         , cl_uint       num_devices
                         , const char*   src
                         , const char*   options);

cl_kernel xk_gpu_kernel( cl_program  program
                       , const char* name);

cl_command_queue xk_gpu_command_queue( cl_context   context
                                     , cl_device_id device);

void xk_gpu_append_buffer_arg(cl_kernel kernel, cl_mem buffer, cl_uint id);

void xk_gpu_run_kernel1D( cl_kernel kernel
                        , cl_command_queue queue
                        , size_t size);

void xk_gpu_run_kernel2D( cl_kernel kernel
                        , cl_command_queue queue
                        , size_t width
                        , size_t height);

void xk_gpu_get_buffer( cl_command_queue queue
                      , cl_mem           buffer
                      , uint8_t          *data
                      , size_t           size);

void xk_gpu_set_buffer( cl_command_queue queue
                      , cl_mem           buffer
                      , uint8_t          *data
                      , size_t           size);

void xk_gpu_platforms_del( cl_platform_id* ids
                         , cl_uint         num_platforms);

void xk_gpu_devices_del( cl_device_id* ids
                       , cl_uint       num_devices);

void xk_gpu_devices_del_excluding_one( cl_device_id* ids
                                     , cl_device_id  excluded_device
                                     , cl_uint       num_platforms);

void xk_gpu_context_del(cl_context context);

void xk_gpu_buffer_del(cl_mem buffer);

void xk_gpu_program_del(cl_program program);

void xk_gpu_kernel_del(cl_kernel kernel);

void xk_gpu_command_queue_del(cl_command_queue queue);

#endif
