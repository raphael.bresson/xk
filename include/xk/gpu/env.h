#ifndef XK_GPU_ENV_H_INCLUDED
#define XK_GPU_ENV_H_INCLUDED

#include <xk/gpu/common.h>

typedef struct xk_gpu_env_t
{
  cl_platform_id platform;
  cl_device_id   device;
  cl_context     context;
}xk_gpu_env_t;

xk_gpu_env_t* xk_gpu_env_default();

xk_gpu_env_t* xk_gpu_env_from_platform(cl_platform_id platform);

xk_gpu_env_t* xk_gpu_env_from_device( cl_platform_id platform
                                    , cl_device_id device);

xk_gpu_env_t* xk_gpu_env_from_context( cl_platform_id platform
                                     , cl_device_id   device
                                     , cl_context     context);

void xk_gpu_env_del(xk_gpu_env_t* env);

#endif
