#ifndef XK_GPU_IMAGE_H_INCLUDED
#define XK_GPU_IMAGE_H_INCLUDED

#include <xk/gpu/image/run.h>
#include <xk/gpu/image/sigma_delta.h>
#include <xk/gpu/image/morph.h>

#endif
