#ifndef XK_GPU_KERNELS_ERODE_H_INCLUDED
#define XK_GPU_KERNELS_ERODE_H_INCLUDED

static const char* xk_gpu_erode_src = "\n"
               "void kernel erode_gray( global const uchar* input\n"
               "                      , global uchar* output)\n"
               "{\n"
               "  const int2 pos = { get_global_id(0), get_global_id(1) };\n"
               "  uchar result = 0xFF;\n"
               "  int y0 = pos.y * (get_global_size(0));\n"
               "  int index0 = y0 + pos.x;\n"
               "  int xmin = -MORPH_WIDTH, xmax = MORPH_WIDTH, ymin = -MORPH_HEIGHT, ymax = MORPH_HEIGHT;\n"
               "" // vertical padding
               "  if(pos.y - MORPH_HEIGHT < 0)\n"
               "  {\n"
               "    ymin = -MORPH_HEIGHT + (MORPH_HEIGHT - pos.y);\n"
               "  }else if(pos.y + MORPH_HEIGHT > get_global_size(1))\n"
               "  {\n"
               "    ymax = MORPH_HEIGHT - ((MORPH_HEIGHT + pos.y) - get_global_size(1));\n"
               "  }\n"
               "" // horizontal padding
               "  if(pos.x - MORPH_WIDTH < 0)\n"
               "  {\n"
               "    xmin = -MORPH_WIDTH + (MORPH_WIDTH - pos.x);\n"
               "  }else if(pos.x + MORPH_WIDTH > get_global_size(0))\n"
               "  {\n"
               "    xmax = MORPH_WIDTH - ((MORPH_WIDTH + pos.x) - get_global_size(0));\n"
               "  }\n"
               "" // filtering operation
               "  for(int y = ymin ; y < ymax ; y++)\n"
               "  {\n"
               "    int y1 = index0 + y * get_global_size(0);\n"
               "    for(int x = xmin ; x < xmax ; x++)\n"
               "    {\n"
               "      int index1 = y1 + x;\n"
               "      result = min(result, input[index1]);\n"
               "    }\n"
               "  }\n"
               "  output[index0 + pos.y] = result;\n"
               "}\n";
             
#endif
