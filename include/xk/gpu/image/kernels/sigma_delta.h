#ifndef XK_GPU_KERNELS_SIGMA_DELTA_H_INCLUDED
#define XK_GPU_KERNELS_SIGMA_DELTA_H_INCLUDED

static const char* xk_gpu_sigma_delta_src 
             = "void kernel sigma_delta_gray( global const uchar* I\n"
               "                            , global uchar*       M\n"
               "                            , global uchar*       O\n"
               "                            , global uchar*       V\n"
               "                            , global uchar*       E)\n"
               "{\n"
               "  const int2 pos = { get_global_id(0), get_global_id(1) };\n"
               "  uchar result = 0;\n"
               "  int y0 = pos.y * get_global_size(0);\n"
               "  int index0 = y0 + pos.x;\n"
               "  uchar m, i, v;\n"
               "  uint no;\n"
               "  m = M[index0];\n"
               "  i = I[index0];\n"
               "  v = V[index0];\n"
               "  if(m < i)\n"
               "    m++;\n"
               "  else if(m > i)\n"
               "    m--;\n"
               "  O[index0] =  abs_diff( M[index0], I[index0]);\n"
               "  no = O[index0] * N;\n"
               "  if(v < no)\n"
               "    v++;\n"
               "  else if(v > no)\n"
               "    v--;\n"
               "  E[index0+pos.y] = -(v < O[index0]);\n"
               "  M[index0] = m;\n"
               "  V[index0] = v;\n"
               "}\n";
             
#endif
