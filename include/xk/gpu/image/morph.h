#ifndef XK_GPU_IMAGE_MORPH_H_INCLUDED
#define XK_GPU_IMAGE_MORPH_H_INCLUDED

#include <xk/gpu/env.h>
#include <xk/gpu/task.h>

xk_gpu_task_t* xk_gpu_dilate_task( xk_gpu_env_t* env
                                 , uint8_t**     in
                                 , uint8_t**     out
                                 , uint32_t      image_width
                                 , uint32_t      image_height
                                 , uint32_t      morph_width
                                 , uint32_t      morph_height);

xk_gpu_task_t* xk_gpu_erode_task( xk_gpu_env_t* env
                                , uint8_t**     in
                                , uint8_t**     out
                                , uint32_t      image_width
                                , uint32_t      image_height
                                , uint32_t      morph_width
                                , uint32_t      morph_height);

xk_gpu_task_t* xk_gpu_dilate_task_from_buffers( xk_gpu_env_t* env
                                              , cl_mem        in
                                              , cl_mem        out
                                              , uint32_t      image_width
                                              , uint32_t      image_height
                                              , uint32_t      morph_width
                                              , uint32_t      morph_height);

xk_gpu_task_t* xk_gpu_erode_task_from_buffers( xk_gpu_env_t* env
                                             , cl_mem        in
                                             , cl_mem        out
                                             , uint32_t      image_width
                                             , uint32_t      image_height
                                             , uint32_t      morph_width
                                             , uint32_t      morph_height);


#endif
