#ifndef XK_GPU_IMAGE_RUN_H_INCLUDED
#define XK_GPU_IMAGE_RUN_H_INCLUDED

#include <xk/gpu/task.h>

void _xk_gpu_image_run( xk_gpu_task_t*   task
                      , cl_command_queue queue
                      , uint8_t**        in
                      , uint8_t**        out);

void _xk_gpu_image_run_output_only( xk_gpu_task_t*   task
                                  , cl_command_queue queue
                                  , uint8_t**        out);

void _xk_gpu_image_run_input_only( xk_gpu_task_t*   task
                                 , cl_command_queue queue
                                 , uint8_t**        in);

#define xk_gpu_image_run(task, queue, in, out) \
  _xk_gpu_image_run((xk_gpu_task_t*)task,queue,in,out)


#define xk_gpu_image_run_input_only(task, queue, in) \
  _xk_gpu_image_run_input_only((xk_gpu_task_t*)task,queue,in)  

#define xk_gpu_image_run_output_only(task, queue, out) \
  _xk_gpu_image_run_output_only((xk_gpu_task_t*)task,queue,out)
  
#endif
