#ifndef XK_GPU_IMAGE_SIGMA_DELTA_H_INCLUDED
#define XK_GPU_IMAGE_SIGMA_DELTA_H_INCLUDED

#include <xk/gpu/env.h>
#include <xk/gpu/task.h>

typedef struct xk_gpu_sigma_delta_task_t
{
  XK_GPU_TASK
  uint8_t** M;
  uint8_t** O;
  uint8_t** V;
} xk_gpu_sigma_delta_task_t;

#define XK_GPU_SIGMA_DELTA_TASK \
  XK_GPU_TASK                   \
  uint8_t** M;                  \
  uint8_t** O;                  \
  uint8_t** V;


xk_gpu_sigma_delta_task_t* xk_gpu_sigma_delta_task( xk_gpu_env_t* env
                                                  , uint8_t**     in
                                                  , uint8_t**     out
                                                  , uint32_t      image_width
                                                  , uint32_t      image_height
                                                  , uint32_t      N);

void xk_gpu_sigma_delta_task_init( xk_gpu_sigma_delta_task_t* task
                                 , xk_gpu_env_t* env
                                 , uint8_t**     in
                                 , uint8_t**     out
                                 , uint32_t      image_width
                                 , uint32_t      image_height
                                 , uint32_t      N);

void xk_gpu_sigma_delta_task_del(xk_gpu_sigma_delta_task_t* task);

#endif
