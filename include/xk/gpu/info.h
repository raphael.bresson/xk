#ifndef XK_GPU_INFO_H_INCLUDED
#define XK_GPU_INFO_H_INCLUDED

#include <xk/gpu/common.h>

typedef struct xk_gpu_platform_info_t
{
  char* name;
  char* vendor;
  char* version;
  char* profile;
  char* extensions;
}xk_gpu_platform_info_t;

typedef struct xk_gpu_device_info_t
{
  char*   name;
  char*   device_version;
  char*   driver_version;
  char*   cl_compiler_version;
  cl_uint max_compute_units;
}xk_gpu_device_info_t;

void xk_gpu_platform_info(cl_platform_id platform, xk_gpu_platform_info_t* info);

void xk_gpu_platform_info_del(xk_gpu_platform_info_t* info);

void xk_gpu_platform_info_print(xk_gpu_platform_info_t* info);

void xk_gpu_device_info(cl_device_id device, xk_gpu_device_info_t* info);

void xk_gpu_device_info_del(xk_gpu_device_info_t* info);

void xk_gpu_device_info_print(xk_gpu_device_info_t* info);

#endif
