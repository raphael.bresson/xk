#ifndef XK_GPU_TASK_H_INCLUDED
#define XK_GPU_TASK_H_INCLUDED

#include <xk/gpu/env.h>

typedef struct xk_gpu_task_t
{
  cl_program program;
  cl_kernel  kernel;
  uint32_t   num_inputs;
  uint32_t   num_inouts;
  uint32_t   num_outputs;
  cl_mem*    inputs;
  cl_mem*    inouts;
  cl_mem*    outputs;
  uint32_t*  in_sizes;
  uint32_t*  inout_sizes;
  uint32_t*  out_sizes; 
  uint32_t*  work_size;
  uint32_t   work_dims;
} xk_gpu_task_t;

#define XK_GPU_TASK        \
  cl_program program;      \
  cl_kernel  kernel;       \
  uint32_t   num_inputs;   \
  uint32_t   num_inouts;   \
  uint32_t   num_outputs;  \
  cl_mem*    inputs;       \
  cl_mem*    inouts;       \
  cl_mem*    outputs;      \
  uint32_t*  in_sizes;     \
  uint32_t*  inout_sizes;  \
  uint32_t*  out_sizes;    \
  uint32_t*  work_size;    \
  uint32_t   work_dims;    
  
xk_gpu_task_t* xk_gpu_task( xk_gpu_env_t*   env
                          , const char*     sources
                          , const char*     c_flags
                          , const char*     kernel_name
                          , uint32_t        num_cpu_in_buffers
                          , void**          cpu_in_buffers
                          , const uint32_t* cpu_in_buffers_sizes
                          , uint32_t        num_cpu_inout_buffers
                          , void**          cpu_inout_buffers
                          , const uint32_t* cpu_inout_buffers_sizes
                          , uint32_t        num_cpu_out_buffers
                          , void**          cpu_out_buffers
                          , const uint32_t* cpu_out_buffers_sizes
                          , const uint32_t* work_size
                          , const uint32_t  work_dims);

void xk_gpu_task_init( xk_gpu_task_t* task
                     , xk_gpu_env_t*   env
                     , const char*     sources
                     , const char*     c_flags
                     , const char*     kernel_name
                     , uint32_t        num_cpu_in_buffers
                     , void**          cpu_in_buffers
                     , const uint32_t* cpu_in_buffers_sizes
                     , uint32_t        num_cpu_inout_buffers
                     , void**          cpu_inout_buffers
                     , const uint32_t* cpu_inout_buffers_sizes
                     , uint32_t        num_cpu_out_buffers
                     , void**          cpu_out_buffers
                     , const uint32_t* cpu_out_buffers_sizes
                     , const uint32_t* work_size
                     , const uint32_t  work_dims);

xk_gpu_task_t* xk_gpu_task_from_buffers( xk_gpu_env_t*   env
                                       , const char*     sources
                                       , const char*     c_flags
                                       , const char*     kernel_name
                                       , uint32_t        num_cpu_in_buffers
                                       , cl_mem*         cpu_in_buffers
                                       , const uint32_t* cpu_in_buffers_sizes
                                       , uint32_t        num_cpu_inout_buffers
                                       , cl_mem*         cpu_inout_buffers
                                       , const uint32_t* cpu_inout_buffers_sizes
                                       , uint32_t        num_cpu_out_buffers
                                       , cl_mem*         cpu_out_buffers
                                       , const uint32_t* cpu_out_buffers_sizes
                                       , const uint32_t* work_size
                                       , const uint32_t  work_dims);

void xk_gpu_task_init_from_buffers( xk_gpu_task_t* task
                                  , xk_gpu_env_t*   env
                                  , const char*     sources
                                  , const char*     c_flags
                                  , const char*     kernel_name
                                  , uint32_t        num_cpu_in_buffers
                                  , cl_mem*         cpu_in_buffers
                                  , const uint32_t* cpu_in_buffers_sizes
                                  , uint32_t        num_cpu_inout_buffers
                                  , cl_mem*         cpu_inout_buffers
                                  , const uint32_t* cpu_inout_buffers_sizes
                                  , uint32_t        num_cpu_out_buffers
                                  , cl_mem*         cpu_out_buffers
                                  , const uint32_t* cpu_out_buffers_sizes
                                  , const uint32_t* work_size
                                  , const uint32_t  work_dims);

void xk_gpu_task_set_in_buffer( xk_gpu_task_t* task
                              , uint32_t index
                              , void*    data
                              , cl_command_queue queue);

void xk_gpu_task_set_inout_buffer( xk_gpu_task_t*   task
                                 , uint32_t         index
                                 , void*            data
                                 , cl_command_queue queue);

void xk_gpu_task_get_inout_buffer( xk_gpu_task_t*   task
                                 , uint32_t         index
                                 , void*            data
                                 , cl_command_queue queue);

void xk_gpu_task_get_out_buffer( xk_gpu_task_t*   task
                               , uint32_t         index
                               , void*            data
                               , cl_command_queue queue);

void xk_gpu_task_run( xk_gpu_task_t* task
                    , cl_command_queue queue);

void xk_gpu_task_del(xk_gpu_task_t* task);

#endif
