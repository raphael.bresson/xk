#ifndef XK_GUI_H_INCLUDED
#define XK_GUI_H_INCLUDED

#include <xk/gui/sdl_utils.h>
#include <xk/gui/input.h>
#include <xk/gui/widget.h>
#include <xk/gui/widget_list.h>
#include <xk/gui/label.h>
#include <xk/gui/button.h>
#include <xk/gui/image.h>
#include <xk/gui/image_stream.h>

#endif
