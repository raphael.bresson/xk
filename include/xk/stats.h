#ifndef XK_STATS_H_INCLUDED
#define XK_STATS_H_INCLUDED

#include <xk/stats/mean.h>
#include <xk/stats/stddev.h>
#include <xk/stats/variance.h>
#include <xk/stats/weighted_mean.h>

#endif
