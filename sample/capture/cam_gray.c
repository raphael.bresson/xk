#include <xk/capture/cam.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xk/image/color.h>

uint8_t** matv = NULL;
uint8_t** matr = NULL;
int count = 200;

void process_image(struct xk_cam_capture_t* capture, uint32_t index, void* args)
{
  char fn[255];
  size_t w = capture->w *  2;
  xk_u8_matrix_view_update(capture->buffers[index].start, matv, w, capture->h);
  xk_yuv_to_gray(matv, matr, capture->w, capture->h);
  sprintf(fn, "cap_gray_%d.pgm", 200 - count);
  xk_write_pgm(fn, matr, capture->w, capture->h);
  count--;
  if(!count)
    capture->running = 0;
}

int main()
{
  uint32_t w = 160, h = 120;
 
  xk_cam_capture_t *cap = xk_cam_capture( "/dev/video0", IO_METHOD_MMAP, process_image, 1, &w, &h);
  matv = xk_u8_matrix(0,w*2 -1, 0, h-1);
  matr = xk_u8_matrix(0,w -1, 0, h-1);
  xk_cam_capture_start(cap);
  xk_cam_capture_loop(cap, NULL);
  xk_cam_capture_stop(cap);
  xk_cam_capture_del(cap);
  xk_u8_matrix_del(matv,0,0);
  xk_u8_matrix_del(matr,0,0);
}
