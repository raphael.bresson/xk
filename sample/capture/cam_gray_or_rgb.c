#include <xk/capture/cam.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xk/image/color.h>
#include <xk/gui/image.h>
#include <xk/gui/input.h>
#include <xk/gui/button.h>

typedef struct sample_utils
{
  SDL_Window       * win;
  xk_widget_list_t * list;
  xk_gui_input_t   * input_listener;
  xk_image_widget_t* imgw;
  xk_button_t      * button;
  uint8_t** matv;
  uint8_t** matr;
  uint8_t** matg;
  int is_gray;
} sample_utils;

void callback_click(void* args)
{
}

void callback_release(void* args)
{
  if(args)
  {
    sample_utils* params = (sample_utils*)args;
    params->is_gray = ~params->is_gray;
  }
}

void process_image(xk_cam_capture_t* capture, uint32_t index, void* args)
{
  uint32_t x, y;
  sample_utils* params = (sample_utils*) args; 
  xk_u8_matrix_view_update(capture->buffers[index].start,params->matv, capture->w * 2, capture->h);
  if(params->is_gray)
  {
    xk_yuv_to_gray(params->matv, params->matg, capture->w, capture->h);
    xk_image_widget_set_gray(params->imgw, params->matg, capture->w, capture->h);
  }else
  {
    xk_yuv_to_rgb(params->matv, params->matr, capture->w, capture->h);
    xk_image_widget_set_rgb(params->imgw, params->matr, capture->w, capture->h);
  }
  xk_gui_input_update(params->input_listener);
  xk_widget_list_draw_all(params->list, SDL_GetWindowSurface(params->win));
  if(xk_mouse_clicked(params->input_listener, &x, &y))
      xk_widget_list_on_click(params->list, x, y);
  if(xk_mouse_released(params->input_listener, &x, &y))
      xk_widget_list_on_release(params->list, x, y);
  xk_swap_window(params->win);
  usleep(10000);
  capture->running = params->input_listener->running;
}


int main()
{
  xk_gui_init();
  sample_utils params;
  params.win  = xk_window("rgb or gray camera", 1024, 600);
  params.list = xk_widget_list();
  params.input_listener = xk_gui_input();
  params.is_gray = 0;
  params.button = xk_button( "Click me"
                           , "../share/font/calibri.ttf"
                           , 24
                           , 820
                           , 100);
  xk_button_set_onclick_args(params.button, &params);
  xk_button_set_onclick(params.button, callback_click);
  xk_button_set_onrelease_args(params.button, &params);
  xk_button_set_onrelease(params.button, callback_release);
 
//   
  uint32_t w = 800, h = 600;
  xk_cam_capture_t *cap = xk_cam_capture( "/dev/video0", IO_METHOD_MMAP, process_image, 1, &w, &h);

  params.imgw = xk_image_widget(0,0,w,h);
  xk_widget_list_append(params.list, params.imgw);
  xk_widget_list_append(params.list, params.button);
  params.matv = xk_u8_matrix_view(w*2, h);
  params.matr = xk_u8_matrix(0,w*3-1, 0, h-1);
  params.matg = xk_u8_matrix(0,w-1, 0, h-1);
  
  xk_cam_capture_start(cap);
  xk_cam_capture_loop(cap, &params);
  xk_cam_capture_stop(cap);
  xk_cam_capture_del(cap);
  xk_u8_matrix_view_del(params.matv);
  xk_u8_matrix_del(params.matr,0,0);
  
  xk_widget_list_del(params.list);
  xk_gui_input_del(params.input_listener);
  SDL_DestroyWindow(params.win);
  xk_gui_close();
}
