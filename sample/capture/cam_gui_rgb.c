#include <xk/capture/cam.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xk/image/color.h>
#include <xk/gui/image.h>
#include <xk/gui/input.h>

typedef struct sample_utils
{
  SDL_Window       * win;
  xk_widget_list_t * list;
  xk_gui_input_t   * input_listener;
  xk_image_widget_t* imgw;
  uint8_t** matv;
  uint8_t** matr;
} sample_utils;

void process_image(xk_cam_capture_t* capture, uint32_t index, void* args)
{
  sample_utils* params = (sample_utils*) args; 
  size_t w = capture->w *  2;
  xk_u8_matrix_view_update(capture->buffers[index].start, params->matv, w, capture->h);
  xk_yuv_to_rgb(params->matv, params->matr, capture->w, capture->h);
  xk_image_widget_set_rgb(params->imgw, params->matr, capture->w, capture->h);
  xk_gui_input_update(params->input_listener);
  xk_widget_list_draw_all(params->list, SDL_GetWindowSurface(params->win));
  xk_swap_window(params->win);
  usleep(20000);
  capture->running = params->input_listener->running;
}

int main()
{
  xk_gui_init();
  sample_utils params;
  params.win  = xk_window("gray camera", 800, 600);
  params.list = xk_widget_list();
  params.input_listener = xk_gui_input();
  
  uint32_t w = 800, h = 600;
  xk_cam_capture_t *cap = xk_cam_capture( "/dev/video0", IO_METHOD_MMAP, process_image, 1, &w, &h);

  params.imgw = xk_image_widget(0,0,w,h);
  xk_widget_list_append(params.list, params.imgw);
  
  params.matv = xk_u8_matrix(0,w*2 -1, 0, h-1);
  params.matr = xk_u8_matrix(0,w*3-1, 0, h-1);
  
  xk_cam_capture_start(cap);
  xk_cam_capture_loop(cap, &params);
  xk_cam_capture_stop(cap);
  xk_cam_capture_del(cap);
  xk_u8_matrix_del(params.matv,0,0);
  xk_u8_matrix_del(params.matr,0,0);
  
  xk_widget_list_del(params.list);
  xk_gui_input_del(params.input_listener);
  SDL_DestroyWindow(params.win);
  xk_gui_close();
}
