#include <xk/image/matrix.h>
#include <xk/image/sigma_delta.h>
#include <xk/network/image_stream.h>
#include <xk/image/blit.h>

typedef struct
{
  char* local_addr;
  uint16_t local_port_in0;
  uint16_t local_port_in1;
  uint16_t local_port_view;
  
  char* in0_addr;
  uint16_t in0_port;
  char* in1_addr;
  uint16_t in1_port;
  
  char* viewer_addr;
  uint16_t viewer_port;
}sample_utils;

void* ctrl_callback(void* data)
{
  int* running = (int*)data;
  while(getchar() != 'q');
  *running = 0;
  return NULL;
}

void process_args(sample_utils* params, int argc, char** argv)
{
  int i;
  for(i = 1 ; i < argc - 1 ; i++)
  {
    if((strcmp(argv[i], "--addr_local") == 0))
      params->local_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_local_in0") == 0))
      params->local_port_in0 = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local_in1") == 0))
      params->local_port_in1 = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local_view") == 0))
      params->local_port_view = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_in0") == 0))
      params->in0_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_in0") == 0))
      params->in0_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_in1") == 0))
      params->in1_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_in1") == 0))
      params->in1_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_view") == 0))
      params->viewer_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_view") == 0))
      params->viewer_port = atoi(argv[i+1]);
  }
}

int main(int argc, char** argv)
{
  uint32_t w = 640, h = 480;
  pthread_t th;
  int running = 1;
  xk_image_stream_t *in0, *in1, *viewer; 
  
  sample_utils params;
  process_args(&params, argc, argv);
  
  in0    = xk_input_image_stream(w, h, params.local_addr, params.local_port_in0, params.in0_addr, params.in0_port);
  in1    = xk_input_image_stream(w*3, h, params.local_addr, params.local_port_in1, params.in1_addr, params.in1_port);
  viewer = xk_output_image_stream(w*3, h, params.local_addr, params.local_port_view, params.viewer_addr, params.viewer_port);
  pthread_create(&th, NULL, ctrl_callback, &running);
  uint8_t** in_buffer0 = NULL, **in_buffer1 = NULL;
  while(running)
  {
    in_buffer0 = xk_input_image_stream_next_frame(in0);
    in_buffer1 = xk_input_image_stream_next_frame(in1);
    if(in_buffer0 && in_buffer1)
    {
      uint8_t **ms0 = xk_u8_matrix(0, w*3-1, 0, h);
      xk_blit_binary_to_rgb(in_buffer1, in_buffer0, ms0, 0, 255, 0, w, h); 
      xk_output_image_stream_next_frame(viewer, ms0);
      xk_u8_matrix_del(in_buffer0,0,0);
      xk_u8_matrix_del(in_buffer1,0,0);
    }
  }
  
  pthread_join(th, NULL);
  
  xk_image_stream_del(in0);
  xk_image_stream_del(in1);
  xk_image_stream_del(viewer);
}
