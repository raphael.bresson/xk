#include <xk/capture/cam.h>
#include <xk/image/matrix.h>
#include <xk/image/color.h>
#include <xk/network/image_stream.h>

typedef struct
{
  xk_image_stream_t* out0;
  xk_image_stream_t* out1;
  xk_image_stream_t* out_viewer;
  uint8_t** matv;
  xk_cam_capture_t *cap;
  
  char* local_addr;
  uint16_t local_port0;
  uint16_t local_port1;
  uint16_t local_port_view;
  
  char* out0_addr;
  uint16_t out0_port;
  char* out1_addr;
  uint16_t out1_port;
  
  char* viewer_addr;
  uint16_t viewer_port;
}sample_utils;

void* ctrl_callback(void* data)
{
  sample_utils* params = (sample_utils*)data;
  while(getchar() != 'q');
  params->cap->running = 0;
  return NULL;
}

void process_image(struct xk_cam_capture_t* capture, uint32_t index, void* args)
{
  uint8_t **mat0, **mat1, **matr;
  sample_utils* params = (sample_utils*) args; 
  size_t w = capture->w *  2;
  matr     = xk_u8_matrix(0,capture->w-1, 0, capture->h-1);
  mat0     = xk_u8_matrix(0,capture->w*3-1, 0, capture->h-1);
  mat1     = xk_u8_matrix(0,capture->w*3-1, 0, capture->h-1);
  xk_u8_matrix_view_update(capture->buffers[index].start, params->matv, w, capture->h);
  xk_yuv_to_gray(params->matv, matr, capture->w, capture->h);
  xk_yuv_to_rgb(params->matv, mat0, capture->w, capture->h);
  memcpy(mat1[0], mat0[0], capture->w * 3 * capture->h);
  xk_output_image_stream_next_frame(params->out0, matr);
  xk_output_image_stream_next_frame(params->out1, mat0);
  xk_output_image_stream_next_frame(params->out_viewer, mat1);
}

void process_args(sample_utils* params, int argc, char** argv)
{
  int i;
  for(i = 1 ; i < argc - 1 ; i++)
  {
    if((strcmp(argv[i], "--addr_local") == 0))
      params->local_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_local0") == 0))
      params->local_port0 = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local1") == 0))
      params->local_port1 = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local_view") == 0))
      params->local_port_view = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_out0") == 0))
      params->out0_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_out0") == 0))
      params->out0_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_out1") == 0))
      params->out1_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_out1") == 0))
      params->out1_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_view") == 0))
      params->viewer_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_view") == 0))
      params->viewer_port = atoi(argv[i+1]);
  }
}

int main(int argc, char** argv)
{
  uint32_t w = 640, h = 480;
  pthread_t th;
  
  sample_utils params;
  process_args(&params, argc, argv);
  
  params.cap        = xk_cam_capture( "/dev/video0", IO_METHOD_MMAP, process_image, 1, &w, &h);
  params.out0       = xk_output_image_stream(w, h, params.local_addr, params.local_port0, params.out0_addr, params.out0_port);
  params.out1       = xk_output_image_stream(w*3, h, params.local_addr, params.local_port1, params.out1_addr, params.out1_port);
  params.out_viewer = xk_output_image_stream(w*3, h, params.local_addr, params.local_port_view, params.viewer_addr, params.viewer_port);
  params.matv       = xk_u8_matrix_view(w*2, h);
  
  pthread_create(&th, NULL, ctrl_callback, &params);
  
  xk_cam_capture_start(params.cap);
  xk_cam_capture_loop(params.cap, &params);
  xk_cam_capture_stop(params.cap);
  
  pthread_join(th, NULL);
  
  xk_cam_capture_del(params.cap);
  xk_image_stream_del(params.out0);
  xk_image_stream_del(params.out1);
  xk_image_stream_del(params.out_viewer);
  xk_u8_matrix_view_del(params.matv);
  free(params.local_addr);
  free(params.out0_addr);
  free(params.out1_addr);
  free(params.viewer_addr);
  return 0;
}
