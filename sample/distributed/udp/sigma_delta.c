#include <xk/image/matrix.h>
#include <xk/image/sigma_delta.h>
#include <xk/network/image_stream.h>

typedef struct
{
  char* local_addr;
  uint16_t local_port_in;
  uint16_t local_port_out;
  uint16_t local_port_view;
  
  char* in_addr;
  uint16_t in_port;
  char* out_addr;
  uint16_t out_port;
  
  char* viewer_addr;
  uint16_t viewer_port;
}sample_utils;

void* ctrl_callback(void* data)
{
  int* running = (int*)data;
  while(getchar() != 'q');
  *running = 0;
  return NULL;
}

void process_args(sample_utils* params, int argc, char** argv)
{
  int i;
  for(i = 1 ; i < argc - 1 ; i++)
  {
    if((strcmp(argv[i], "--addr_local") == 0))
      params->local_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_local_in") == 0))
      params->local_port_in = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local_out") == 0))
      params->local_port_out = atoi(argv[i+1]);
    if((strcmp(argv[i], "--port_local_view") == 0))
      params->local_port_view = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_in") == 0))
      params->in_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_in") == 0))
      params->in_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_out") == 0))
      params->out_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_out") == 0))
      params->out_port = atoi(argv[i+1]);
    
    if((strcmp(argv[i], "--addr_view") == 0))
      params->viewer_addr = strdup(argv[i+1]);
    if((strcmp(argv[i], "--port_view") == 0))
      params->viewer_port = atoi(argv[i+1]);
  }
}

int main(int argc, char** argv)
{
  uint32_t w = 640, h = 480;
  pthread_t th;
  int first_time = 1;
  int running = 1;
  xk_sigma_delta_config config;
  xk_image_stream_t *in, *out, *viewer; 
  
  sample_utils params;
  process_args(&params, argc, argv);
  
  in  = xk_input_image_stream(w, h, params.local_addr, params.local_port_in, params.in_addr, params.in_port);
  out = xk_output_image_stream(w, h, params.local_addr, params.local_port_out, params.out_addr, params.out_port);
  viewer = xk_output_image_stream(w, h, params.local_addr, params.local_port_view, params.viewer_addr, params.viewer_port);
  pthread_create(&th, NULL, ctrl_callback, &running);
  uint8_t** in_buffer = NULL;
  while(running)
  {
    in_buffer = xk_input_image_stream_next_frame(in);
    if(in_buffer)
    {
      if(first_time)
      {
        xk_sigma_delta_config_init(&config, in_buffer, 4, w, h);
        first_time = 0;
      }
      uint8_t **ms0 = xk_u8_matrix(0, w-1, 0, h);
      uint8_t **ms1 = xk_u8_matrix(0, w-1, 0, h); 
      xk_sigma_delta(in_buffer, &config);
      
      memcpy(ms0[0], config.etq[0], w*h);
      memcpy(ms1[0], config.etq[0], w*h);
      xk_output_image_stream_next_frame(out, ms0);
      xk_output_image_stream_next_frame(viewer, ms1);
      xk_u8_matrix_del(in_buffer,0,0);
    }
  }
  
  pthread_join(th, NULL);
  
  xk_image_stream_del(in);
  xk_image_stream_del(out);
  xk_image_stream_del(viewer);
}
