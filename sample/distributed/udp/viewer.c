#include <xk/gui/image_stream.h>
#include <xk/gui/input.h>

int main()
{
  xk_gui_init();
  SDL_Window* win = xk_window("image stream widget", 1280, 860);
  xk_widget_list_t* list = xk_widget_list();
  xk_gui_input_t * input_listener = xk_gui_input();
  xk_image_stream_widget_t* cam_stream = xk_image_stream_widget(0,0,640,480, "127.0.0.1", 51112, "127.0.0.1", 51111, XK_RGB);
  xk_image_stream_widget_t* sigma_delta_stream = xk_image_stream_widget(640,0,640,480, "127.0.0.1", 52112, "127.0.0.1", 52111, XK_GRAY);
  xk_image_stream_widget_t* morph_stream = xk_image_stream_widget(0,480,640,480, "127.0.0.1", 53112, "127.0.0.1", 53111, XK_GRAY);
  xk_image_stream_widget_t* blit_stream = xk_image_stream_widget(640,480,640,480, "127.0.0.1", 54112, "127.0.0.1", 54111, XK_RGB);
  xk_widget_list_append(list, cam_stream);
  xk_widget_list_append(list, sigma_delta_stream);
  xk_widget_list_append(list, morph_stream);
  xk_widget_list_append(list, blit_stream);
  while(input_listener->running)
  {
    xk_gui_input_update(input_listener);
    xk_widget_list_draw_all(list, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    usleep(20000);
  }
  xk_widget_list_del(list);
  xk_gui_input_del(input_listener);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
