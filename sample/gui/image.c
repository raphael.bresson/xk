#include <xk/image/image.h>
#include <xk/image/matrix.h>
#include <xk/gui/image.h>
#include <xk/gui/input.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  xk_gui_init();
  SDL_Window* win = xk_window("image widget", 800, 600);
  xk_widget_list_t* list = xk_widget_list();
  xk_gui_input_t * input_listener = xk_gui_input();
  xk_image_widget_t* imgw = xk_image_widget(0,0,800,600);
  uint32_t w,h;
  uint8_t** img = xk_load_pgm("../data_test/testin.pgm", 0,0, &w, &h);
  xk_image_widget_set_gray(imgw, img, w, h);
  xk_widget_list_append(list, imgw);
  while(input_listener->running)
  {
    xk_gui_input_update(input_listener);
    xk_widget_list_draw_all(list, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    usleep(20000);
  }
  xk_widget_list_del(list);
  xk_gui_input_del(input_listener);
  xk_u8_matrix_del(img, 0, 0);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
