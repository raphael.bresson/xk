#include <xk/gui/image_stream.h>
#include <xk/gui/input.h>

int main()
{
  xk_gui_init();
  SDL_Window* win = xk_window("image stream widget", 800, 600);
  xk_widget_list_t* list = xk_widget_list();
  xk_gui_input_t * input_listener = xk_gui_input();
  xk_image_stream_widget_t* img_stream = xk_image_stream_widget(0,0,320,240, "127.0.0.1", 51112, "127.0.0.1", 51111, XK_GRAY);
  xk_widget_list_append(list, img_stream);
  while(input_listener->running)
  {
    xk_gui_input_update(input_listener);
    xk_widget_list_draw_all(list, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    usleep(20000);
  }
  xk_widget_list_del(list);
  xk_gui_input_del(input_listener);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
