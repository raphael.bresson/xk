#include <xk/gui/label.h>
#include <xk/gui/sdl_utils.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  xk_gui_init();
  SDL_Window* win = xk_window("severals labels", 320, 240);
  xk_widget_list_t* list = xk_widget_list();
  xk_label_t* l0 = xk_label( "Hello!"
                           , "../share/font/calibri.ttf"
                           , 16
                           , 40
                           , 50);
  
  xk_label_t* l1 = xk_label( "Hello!"
                           , "../share/font/calibri.ttf"
                           , 16
                           , 120
                           , 100);
  
  xk_label_t* l2 = xk_label( "Hello!"
                           , "../share/font/calibri.ttf"
                           , 8
                           , 200
                           , 150);
  
  xk_widget_list_append(list, l0);
  xk_widget_list_append(list, l1);
  xk_widget_list_append(list, l2);
  
  int running = 1;
  while(running)
  {
    SDL_Event e;
    xk_widget_list_draw_all(list, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    while(SDL_PollEvent(&e))
    {
      if(e.type == SDL_WINDOWEVENT)
      {
        if(e.window.event == SDL_WINDOWEVENT_CLOSE)
        {
          running = 0;
          break;
        }
      }
    }
    usleep(20000);
  }
  xk_widget_list_del(list);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
