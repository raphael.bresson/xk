#include <xk/gui/label.h>
#include <xk/gui/sdl_utils.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  xk_gui_init();
  SDL_Window* win = xk_window("simple label", 320, 240);
  xk_label_t* label = xk_label( "Hello!"
                             , "../share/font/calibri.ttf"
                             , 16
                             , 120
                             , 100);
  int running = 1;
  while(running)
  {
    SDL_Event e;
    xk_widget_draw(label, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    while(SDL_PollEvent(&e))
    {
      if(e.type == SDL_WINDOWEVENT)
      {
        if(e.window.event == SDL_WINDOWEVENT_CLOSE)
        {
          running = 0;
          break;
        }
      }
    }
    usleep(20000);
  }
  xk_widget_del(label);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
