#include <xk/gui/scroll_bar.h>
#include <xk/gui/input.h>
#include <unistd.h>
#include <stdio.h>

void value_changed(int value, void* data)
{
  printf("value changed: %d\n", value);
}

int main(int argc, char** argv)
{
  xk_gui_init();
  SDL_Window* win = xk_window("simple label", 320, 240);
  xk_widget_list_t* list = xk_widget_list();
  xk_gui_input_t * input_listener = xk_gui_input();
  xk_scroll_bar_t* bar = xk_scroll_bar(10,50,100,20);
  xk_scroll_bar_set_on_value_changed(bar, value_changed, NULL);
  xk_widget_list_append(list, bar);
  uint32_t x=0,y=0;
  while(input_listener->running)
  {
    xk_gui_input_update(input_listener);
    if(xk_mouse_clicked(input_listener, &x, &y))
      xk_widget_list_on_click(list, x, y);
    if(xk_mouse_released(input_listener, &x, &y))
      xk_widget_list_on_release(list, x, y);
    
    xk_widget_list_draw_all(list, SDL_GetWindowSurface(win));
    xk_swap_window(win);
    usleep(20000);
  }
  xk_widget_list_del(list);
  xk_gui_input_del(input_listener);
  SDL_DestroyWindow(win);
  xk_gui_close();
  return 0;
}
