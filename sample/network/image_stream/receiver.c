#include <xk/network/image_stream.h>
#include <xk/gui/sdl_utils.h>
#include <xk/image/matrix.h>
#include <time.h>

int main(int argc, char** argv)
{
  xk_image_stream_t* stream = NULL;
  uint8_t** buffer = NULL;
  stream = xk_input_image_stream(240,320, "127.0.0.1", 51112, "127.0.0.1", 51111);
  xk_gui_init();
  SDL_Window* win = xk_window("image stream receiver", 320, 240);
  SDL_Surface* blit_buffer = xk_grayscale_blit_buffer(320,240);
  int running = 1;
  while(running)
  {
    SDL_Event e;
    buffer = xk_input_image_stream_next_frame(stream);
    if(buffer != NULL)
    {
      xk_gui_blit(win, blit_buffer, buffer, 0, 0, 320, 240);
      xk_swap_window(win);
      xk_u8_matrix_del(buffer, 0, 0);
    }
    buffer = NULL;
    while(SDL_PollEvent(&e))
    {
      if(e.type == SDL_WINDOWEVENT)
      {
        if(e.window.event == SDL_WINDOWEVENT_CLOSE)
        {
          running = 0;
          break;
        }
      }
    }
    usleep(20000);
  }
  SDL_FreeSurface(blit_buffer);
  xk_image_stream_del(stream);
  SDL_DestroyWindow(win);
  xk_gui_close();
  if(buffer)
    xk_u8_matrix_del(buffer, 0, 0);
  return 0;
}
