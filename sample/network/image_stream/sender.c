#include <xk/network/image_stream.h>
#include <xk/image/image.h>
#include <xk/image/matrix.h>
#include <time.h>

int running = 1;

uint8_t** get_image(uint32_t i)
{
  char name[255];
  uint32_t w, h;
  sprintf(name,"../data_test/car_3%03d.pgm", i);
  return xk_load_pgm(name, 0, 0, &w, &h);
}

void* run_callback(void* data)
{
  while(getchar() != 'q');
  running = 0;
  return NULL;
}

int main(int argc, char** argv)
{
  xk_image_stream_t* stream = NULL;
  uint8_t** image = NULL;
  stream = xk_output_image_stream(320,240, "127.0.0.1", 51111, "127.0.0.1", 51112);
  pthread_t th;
  pthread_create(&th, NULL, run_callback, NULL);
  while(running)
  {
    for(int i = 0 ; (i < 200) && running; i++)
    {
      image = get_image(i);
      xk_output_image_stream_next_frame(stream, image);
      image = NULL;
      usleep(20000);
    }
  }
  xk_image_stream_del(stream);
  if(image)
    xk_u8_matrix_del(image, 0, 0);
  pthread_join(th, NULL);
  return 0;
}
