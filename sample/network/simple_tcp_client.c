#include <xk/network/tcp.h>

int main()
{
  int sockfd = xk_tcp_socket();
  char buff[5];
  xk_connect_socket(sockfd, "127.0.0.1", 51111);
  xk_socket_send(sockfd, "toto", 5);
  xk_socket_recv(sockfd, buff, 4);
  buff[4] = '\0';
  printf("%s\n", buff);
  close(sockfd);
  return 0;
}
