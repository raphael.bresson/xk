#include <xk/network/tcp.h>

int main()
{
  char buff[5];
  int sockfd = xk_tcp_socket();
  xk_set_local_port(sockfd, 51111);
  xk_tcp_listen(sockfd, 5);
  int client = xk_tcp_accept(sockfd);
  xk_socket_recv(client, buff, 4);
  buff[4] = '\0';
  printf("%s\n", buff);
  xk_socket_send(client,"titi", 5);
  close(sockfd);
  return 0;
}
