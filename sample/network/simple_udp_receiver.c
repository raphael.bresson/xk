#include <xk/network/udp_image.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>

int main() {
  int sockfd = xk_udp_socket();
  uint16_t peer_port = 51111;
  char address[128];
  strcpy(address, "127.0.0.1");
  xk_set_local_port(sockfd, 51112);
  uint32_t w = 320, h = 240;
  uint8_t** image = xk_u8_matrix(0,w-1,0,h-1);
  xk_udp_recv_u8_image(sockfd, image, w, h, address, &peer_port);
  xk_write_pgm("udp_image.pgm", image, w, h);
  xk_u8_matrix_del(image, 0, 0);
  close(sockfd);
  return 0;
}
