#include <xk/network/udp_image.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>

#define XK_UDP_IMAGE_CHUNK 1024

int main() {
  int sockfd = xk_udp_socket();
  xk_set_local_port(sockfd, 51111);
  const char* addr = "127.0.0.1";
  uint16_t port = 51112;
  uint32_t w, h;
  uint8_t** image = xk_load_pgm("../data_test/testin.pgm", 0, 0, &w, &h);
  xk_udp_send_u8_image(sockfd, image, w, h, addr, port); 
  close(sockfd);
  xk_u8_matrix_del(image, 0, 0);
  return 0;
}
