#!/bin/bash

usage()
{
 echo "Usage: $0 version architecture [multithreading] [build_doc]"
 echo "Supported architectures: i386, amd64, armhf, aarch64"
 echo "Possible values for multithreading: 1: activated, 0: non activated [default is 0]"
 echo "Possible values for build_doc: 1: activated, 0: non activated [default is 1]"
}

if [ "$#" -lt 2 ]; then
  usage
  exit 1
fi

# XK_VERSION = "0"
EXE_NAME=$0
VERSION=$1
ARCH=$2
MULTITHREAD=$3
BUILD_DOC=$4

build_xk_deb_pkg() 
{
  mkdir -p build_deb_$2
  cd build_deb_$2
  mkdir -p libxk/DEBIAN
  mkdir -p libxk/usr/local
  touch libxk/DEBIAN/control
  printf "Package: libxk\nVersion: $1\nMaintainer: Raphael Bresson raphael.bresson@sfr.fr\nArchitecture: $2\nDescription: Image processing and video streaming\n" > libxk/DEBIAN/control
  cmake -DCMAKE_INSTALL_PREFIX=libxk/usr/local $3 ..
  make -j4
  make install
  dpkg-deb --build libxk
  echo "Package generated : $(pwd)/libxk.deb"
  cd ..
}

ARMv7_OPTION="-DBUILD_ARM32=ON"
ARM64_OPTION="-DBUILD_ARM64=ON"
MULTITHREAD_OPTION="-DMULTI_THREAD=ON"
NO_BUILD_DOC="-DBUILD_DOC=OFF"

OPTION=""



if [ "$MULTITHREAD" == "1" ]; then
  OPTION="$OPTION $MULTITHREAD_OPTION"
fi

if [ -e "$BUILD_DOC" ]; then
  if [ "$BUILD_DOC" == "0" ]; then
    OPTION="$OPTION $NO_BUILD_DOC"
  fi
fi

i386_OPTION="$OPTION"
amd64_OPTION="$OPTION"
arm32_OPTION="$OPTION $ARMv7_OPTION"
arm64_OPTION="$OPTION $ARM64_OPTION"

if [ "$ARCH" == "all" ]; then
  build_xk_deb_pkg $VERSION "armhf" $arm32_OPTION
  build_xk_deb_pkg $VERSION "aarch64" $arm64_OPTION
  build_xk_deb_pkg $VERSION "i386" $i386_OPTION
  build_xk_deb_pkg $VERSION "amd64" $amd64_OPTION
elif [ "$ARCH" == "armhf" ]; then
  build_xk_deb_pkg "$VERSION" "armhf" "$arm32_OPTION"
elif [ "$ARCH" == "aarch64" ]; then
  build_xk_deb_pkg "$VERSION" "aarch64" "$arm64_OPTION"
elif [ "$ARCH" == "i386" ]; then
  build_xk_deb_pkg "$VERSION" "i386" "$i386_OPTION"
elif [ "$ARCH" == "amd64" ]; then
  build_xk_deb_pkg "$VERSION" "amd64" "$amd_OPTION"
else
  echo "Not recognized architecture: $ARCH"
  usage
  exit 1
fi
