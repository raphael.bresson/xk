#!/bin/bash

XK_HOME=$(pwd)

mkdir dependancies && cd dependancies
#svn co http://vcs.exim.org/pcre/code/trunk pcre
wget https://sourceforge.net/projects/pcre/files/pcre/8.42/pcre-8.42.tar.gz
tar xf pcre-8.42.tar.gz && rm pcre-8.42.tar.gz
cd pcre-8.42
mkdir build && cd build
cmake -DCMAKE_C_FLAGS="-fPIC" -DCMAKE_CXX_FLAGS="-fPIC" -DCMAKE_INSTALL_PREFIX=$XK_HOME/dependancies ..
make -j16 && make install
cd .. && rm -rf pcre-8.42

wget https://www.libsdl.org/release/SDL2-2.0.9.tar.gz
tar xf SDL2-2.0.9.tar.gz && rm SDL2-2.0.9.tar.gz
cd SDL2-2.0.9
./configure --prefix=$XK_HOME/dependancies
make -j16 && make install
cd .. && rm -rf SDL2-2.0.9

wget https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.14.tar.gz
tar xf SDL2_ttf-2.0.14.tar.gz && rm SDL2_ttf-2.0.14.tar.gz
cd SDL2_ttf-2.0.14
./configure --prefix=$XK_HOME/dependancies
make -j16 && make install
cd .. && rm -rf SDL2_ttf-2.0.14
cd $XK_HOME
