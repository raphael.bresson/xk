#include <xk/capture/cam.h>
#include "common.h"

xk_cam_capture_t* xk_cam_capture( const char* dev
                                , enum io_method io
                                , void (*process_image)(xk_cam_capture_t* capture, uint32_t index, void* args)
                                , int force_format
                                , uint32_t *w
                                , uint32_t *h)
{
  xk_cam_capture_t* capture = (xk_cam_capture_t*)malloc(sizeof(xk_cam_capture_t));
  XK_ERROR(capture, "XK_CAPTURE_ERROR: Cannot allocate capture\n");
  capture->process_image = process_image;
  switch(io)
  {
    case IO_METHOD_READ:
      _cam_capture_read_init_fnct(dev, capture, w, h, force_format);
      break;
    case IO_METHOD_MMAP:
      _cam_capture_mmap_init_fnct(dev, capture, w, h, force_format);
      break;
    case IO_METHOD_USERPTR:
      _cam_capture_userptr_init_fnct(dev, capture, w, h, force_format);
      break;
    default:
      XK_ERROR(0, "XK_CAPTURE_ERROR: Method not supported\n");
  }
  capture->running = 1;
  *w = capture->w;
  *h = capture->h;
  printf("%d/%d\n", *w, *h);
  return capture;
}

void xk_cam_capture_loop(xk_cam_capture_t* capture, void* args)
{
  int ret;
  fd_set fds;
  struct timeval tv;
  while(capture->running)
  {
    do
    {
      FD_ZERO(&fds);
      FD_SET(capture->fd, &fds);
      tv.tv_sec = XK_V4L2_SELECT_TIMEOUT_SEC;
      tv.tv_usec = XK_V4L2_SELECT_TIMEOUT_USEC;
      ret = select(capture->fd+1, &fds, NULL, NULL, &tv);
      if(EINTR == errno) continue;
      XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: select() : %s\n", strerror(errno));
      XK_ERROR(ret != 0, "XK_CAPTURE_ERROR: select timeout elapsed \n");
    }while(!capture->read_frame(capture, args));
  }
}

