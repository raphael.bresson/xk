#include <xk/capture/cam.h>

#define __XK_CAPTURE_CAM_DO_NOTHING__
#define __XK_CAPTURE_INTERNAL__
#include "common.h"

// read frame using memory mapped method 
int _cam_frame_mmap(xk_cam_capture_t* capture, void* args)
{
  struct v4l2_buffer buf;
  memset(&buf, 0, sizeof(struct v4l2_buffer));
  buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  if(xioctl(capture->fd, VIDIOC_DQBUF, &buf) < 0)
  {
    switch(errno)
    {
      case EAGAIN:
        return 0;
      case EIO:
//             break;
      default:
        XK_ERROR(0, "XK_CAPTURE_ERROR: VIDIOC_DQBUF : %s\n", strerror(errno));
    }
  }
  assert(buf.index < capture->n_buffers);
  capture->buffers[buf.index].bytesused = buf.bytesused;
  capture->process_image(capture, buf.index, args);
  XK_ERROR(xioctl(capture->fd, VIDIOC_QBUF, &buf) >= 0, "XK_CAPTURE_ERROR: VIDIOC_QBUF : %s\n", strerror(errno));
  return 1;
}


void _cam_start_mmap(xk_cam_capture_t* capture)
{
  int i;
  enum v4l2_buf_type type;
  for (i = 0; i < capture->n_buffers; ++i) 
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(struct v4l2_buffer));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = i;
    XK_ERROR(xioctl(capture->fd, VIDIOC_QBUF, &buf) >= 0, "XK_CAPTURE_ERROR: VIDIOC_QBUF : %s\n", strerror(errno));
  }
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  XK_ERROR(xioctl(capture->fd, VIDIOC_STREAMON, &type) >= 0, "XK_CAPTURE_ERROR: VIDIOC_STREAMON : %s\n", strerror(errno));
}

void _cam_init_mmap(xk_cam_capture_t* capture, unsigned int buffer_size)
{
  struct v4l2_requestbuffers req;
  int ret;
  uint32_t i;
  memset(&req, 0, sizeof(struct v4l2_requestbuffers));
  req.count = 4;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;
  ret = xioctl(capture->fd, VIDIOC_REQBUFS, &req);
  XK_ERROR((ret >= 0) || (errno != EINVAL), "XK_CAPTURE_ERROR: Device does not support memory mapping I/O\n");
  XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: VIDIOC_REQBUFS : %s\n", strerror(errno));
  XK_ERROR(req.count >= 2, "XK_CAPTURE_ERROR: Insufficient buffer memory on device\n");
  capture->buffers = calloc(req.count, sizeof(struct cam_buffer));
  XK_ERROR(capture->buffers, "XK_CAPTURE_ERROR: Cannot allocate buffers\n");
  for(i = 0 ; i < req.count ; i++)
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(struct v4l2_buffer));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = i;
    XK_ERROR( xioctl(capture->fd, VIDIOC_QUERYBUF, &buf) >= 0
            , "XK_CAPTURE_ERROR: VIDIOC_QUERYBUF : %s\n", strerror(errno));
    capture->buffers[i].length = buf.length;
    
    capture->buffers[i].start = mmap( NULL, buf.length, PROT_READ | PROT_WRITE
                                    , MAP_SHARED, capture->fd, buf.m.offset);
    XK_ERROR( capture->buffers[i].start != MAP_FAILED
            , "XK_CAPTURE_ERROR: Cannot map memory : %s\n", strerror(errno));
  }
  capture->n_buffers = req.count;
}

void _cam_init_cap_mmap(xk_cam_capture_t* capture, const char* dev)
{
  struct v4l2_capability cap;
  int ret;
  ret = xioctl(capture->fd, VIDIOC_QUERYCAP, &cap); 
  XK_ERROR((ret >= 0) || (errno == EINVAL), "XK_CAPTURE_ERROR: `%s` is not a V4L2 device\n", dev);
  XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: VIDIOC_QUERYCAP : %s\n", strerror(errno));
  XK_ERROR(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE, "XK_CAPTURE_ERROR: `%s` is not a video capture device\n", dev);
}

void _cam_init_device_mmap(xk_cam_capture_t* capture, const char* dev, uint32_t *w, uint32_t *h, int force_format)
{
   struct v4l2_format fmt;
  _cam_open(capture, dev);
  _cam_init_cap_mmap(capture, dev);
  _cam_init_cropcap_fmt(capture, dev, w, h, force_format, &fmt);
  _cam_init_mmap(capture, fmt.fmt.pix.sizeimage);
}

void _cam_mmap_del(xk_cam_capture_t* capture)
{
  uint32_t i;
  for (i = 0; i < capture->n_buffers; ++i)
  {
    XK_ERROR( munmap(capture->buffers[i].start, capture->buffers[i].length) >=0
            , "XK_CAPTURE_ERROR: Munmap failed : %s\n", strerror(errno));
  }
  free(capture->buffers);
  XK_ERROR(close(capture->fd) != -1, "XK_CAPTURE_ERROR: close() failed %s\n", strerror(errno));
  free(capture);
}

void _cam_capture_mmap_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format)
{
  capture->read_frame = _cam_frame_mmap;
  capture->start = _cam_start_mmap;
  capture->stop = _cam_do_nothing;
  capture->del = _cam_mmap_del;
  _cam_init_device_mmap(capture, dev, w, h, force_format);
}
