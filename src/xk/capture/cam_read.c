#include <xk/capture/cam.h>

#define __XK_CAPTURE_CAM_DO_NOTHING__
#define __XK_CAPTURE_INTERNAL__
#include "common.h"

// read frame using basic read method
int _cam_frame_read(xk_cam_capture_t* capture, void* args)
{
  if(read(capture->fd, capture->buffers[0].start, capture->buffers[0].length) < 0)
  {
    switch(errno)
    {
      case EAGAIN:
        return 0;
      case EIO:
//             break;
      default:
        XK_ERROR(0, "XK_CAPTURE_ERROR: read : %s\n", strerror(errno));
    }
  }
  capture->process_image(capture, 0, args);
  return 1;
}

void _cam_init_read(xk_cam_capture_t* capture, unsigned int buffer_size)
{
  capture->buffers = calloc(1, sizeof(struct cam_buffer));
  XK_ERROR(capture->buffers, "XK_CAPTURE_ERROR: Cannot allocate buffer array\n");
  capture->buffers[0].length = buffer_size;
  capture->buffers[0].start = malloc(buffer_size);
  capture->buffers[0].bytesused = buffer_size;
  XK_ERROR(capture->buffers[0].start, "XK_CAPTURE_ERROR: Cannot allocate buffer data\n");
}

void _cam_init_cap_read(xk_cam_capture_t* capture, const char* dev)
{
  struct v4l2_capability cap;
  int ret;
  ret = xioctl(capture->fd, VIDIOC_QUERYCAP, &cap); 
  XK_ERROR((ret >= 0) || (errno == EINVAL), "XK_CAPTURE_ERROR: `%s` is not a V4L2 device\n", dev);
  XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: VIDIOC_QUERYCAP : %s\n", strerror(errno));
  XK_ERROR(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE, "XK_CAPTURE_ERROR: `%s` is not a video capture device\n", dev);
  XK_ERROR(cap.capabilities & V4L2_CAP_READWRITE, "XK_CAPTURE_ERROR: `%s` does not support read I/O\n", dev);
}

void _cam_init_device_read(xk_cam_capture_t* capture, const char* dev, uint32_t *w, uint32_t *h, int force_format)
{
   struct v4l2_format fmt;
  _cam_open(capture, dev);
  _cam_init_cap_read(capture, dev);
  _cam_init_cropcap_fmt(capture, dev, w, h, force_format, &fmt);
  
  _cam_init_read(capture, fmt.fmt.pix.sizeimage);
}

void _cam_read_del(xk_cam_capture_t* capture)
{
  free(capture->buffers[0].start);
  free(capture->buffers);
  XK_ERROR(close(capture->fd) != -1, "XK_CAPTURE_ERROR: close() failed %s\n", strerror(errno));
  free(capture);
}

void _cam_capture_read_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format)
{
  capture->read_frame = _cam_frame_read;
  capture->start = _cam_do_nothing;
  capture->stop = _cam_do_nothing;
  capture->del = _cam_read_del;
  _cam_init_device_read(capture,dev, w, h, force_format);
}
