#include <xk/capture/cam.h>

#define __XK_CAPTURE_INTERNAL__
#include "common.h"



// read frame using user pointer method
int _cam_frame_userptr(xk_cam_capture_t* capture, void* args)
{
  struct v4l2_buffer buf;
  uint32_t i;
  memset(&buf, 0, sizeof(struct v4l2_buffer));
  buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;
  if(xioctl(capture->fd, VIDIOC_DQBUF, &buf) < 0)
  {
    switch(errno)
    {
      case EAGAIN:
        return 0;
      case EIO:
//             break;
      default:
        XK_ERROR(0, "XK_CAPTURE_ERROR: VIDIOC_DQBUF : %s\n", strerror(errno));
    }
  }
  for (i = 0; i < capture->n_buffers; ++i)
  {
    if (  buf.m.userptr == (size_t)capture->buffers[i].start
       && buf.length == capture->buffers[i].length)
      break;
  }
  capture->process_image(capture, i, args);
  XK_ERROR(xioctl(capture->fd, VIDIOC_QBUF, &buf) >= 0, "XK_CAPTURE_ERROR: VIDIOC_QBUF : %s\n", strerror(errno));
  return 1;
}


void _cam_start_userptr(xk_cam_capture_t* capture)
{
  int i;
  enum v4l2_buf_type type;
  for (i = 0; i < capture->n_buffers; ++i) 
  {
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(struct v4l2_buffer));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_USERPTR;
    buf.index = i;
    buf.m.userptr = (unsigned long)capture->buffers[i].start;
    buf.length = capture->buffers[i].length;
    XK_ERROR(xioctl(capture->fd, VIDIOC_QBUF, &buf) >= 0, "XK_CAPTURE_ERROR: VIDIOC_QBUF : %s\n", strerror(errno));
  }
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  XK_ERROR(xioctl(capture->fd, VIDIOC_STREAMON, &type) >= 0, "XK_CAPTURE_ERROR: VIDIOC_STREAMON : %s\n", strerror(errno));
}

void _cam_stop(xk_cam_capture_t* capture)
{
  enum v4l2_buf_type type;
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  XK_ERROR(xioctl(capture->fd, VIDIOC_STREAMOFF, &type) >= 0, "XK_CAPTURE_ERROR: VIDIOC_STREAMOFF : %s\n", strerror(errno));
}

void _cam_init_userptr(xk_cam_capture_t* capture, unsigned int buffer_size)
{
  struct v4l2_requestbuffers req;
  int ret;
  uint32_t i;
  memset(&req, 0, sizeof(struct v4l2_requestbuffers));
  req.count = 4;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_USERPTR;
  ret = xioctl(capture->fd, VIDIOC_REQBUFS, &req);
  XK_ERROR((ret >= 0) || (errno != EINVAL), "XK_CAPTURE_ERROR: Device does not support userptr I/O\n");
  XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: VIDIOC_REQBUFS : %s\n", strerror(errno));
  capture->buffers = calloc(4, sizeof(struct cam_buffer));
  XK_ERROR(capture->buffers, "XK_CAPTURE_ERROR: Cannot allocate buffers\n");
  for(i = 0 ; i < req.count ; i++)
  {
    capture->buffers[i].length = buffer_size;
    capture->buffers[i].bytesused = buffer_size;
    capture->buffers[i].start = malloc(buffer_size);
    XK_ERROR( capture->buffers[i].start
            , "XK_CAPTURE_ERROR: Cannot allocate memory\n");
  }
  capture->n_buffers = req.count;
}


void _cam_init_cap_userptr(xk_cam_capture_t* capture, const char* dev)
{
  struct v4l2_capability cap;
  int ret;
  ret = xioctl(capture->fd, VIDIOC_QUERYCAP, &cap); 
  XK_ERROR((ret >= 0) || (errno == EINVAL), "XK_CAPTURE_ERROR: `%s` is not a V4L2 device\n", dev);
  XK_ERROR(ret >= 0, "XK_CAPTURE_ERROR: VIDIOC_QUERYCAP : %s\n", strerror(errno));
  XK_ERROR(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE, "XK_CAPTURE_ERROR: `%s` is not a video capture device\n", dev);
  XK_ERROR(cap.capabilities & V4L2_CAP_STREAMING, "XK_CAPTURE_ERROR: `%s` does not support streaming\n", dev);
}

void _cam_init_device_userptr(xk_cam_capture_t* capture, const char* dev, uint32_t *w, uint32_t *h, int force_format)
{
   struct v4l2_format fmt;
  _cam_open(capture, dev);
  _cam_init_cap_userptr(capture, dev);
  _cam_init_cropcap_fmt(capture, dev, w, h, force_format, &fmt);
  printf("%d : w*h = %d, w*h*3= %d\n", fmt.fmt.pix.sizeimage, *w * *h, *h * *h * 3);
  _cam_init_userptr(capture, fmt.fmt.pix.sizeimage);
}

void _cam_userptr_del(xk_cam_capture_t* capture)
{
  uint32_t i;
  for (i = 0; i < capture->n_buffers; ++i)
    free(capture->buffers[i].start);
  free(capture->buffers);
  XK_ERROR(close(capture->fd) != -1, "XK_CAPTURE_ERROR: close() failed %s\n", strerror(errno));
  free(capture);
}

void _cam_capture_userptr_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format)
{
  capture->read_frame = _cam_frame_userptr;
  capture->start = _cam_start_userptr;
  capture->stop = _cam_stop;
  capture->del = _cam_userptr_del;
  _cam_init_device_userptr(capture,dev, w, h, force_format);
}
