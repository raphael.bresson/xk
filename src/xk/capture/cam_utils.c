#include <xk/capture/cam_utils.h>

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/videodev2.h>

#define __XK_CAPTURE_INTERNAL__
#define __XK_NO_CAPTURE_CONTROL__
#include "common.h"

void xk_cam_capture_print_supported_formats(xk_cam_capture_t* capture)
{
  struct v4l2_fmtdesc fmtdesc = {0};
  fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  char fourcc[5] = {0};
  char c, e;
  printf("  FMT : CE Desc\n--------------------\n");
  while (0 == xioctl(capture->fd, VIDIOC_ENUM_FMT, &fmtdesc))
  {
    strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
    c = fmtdesc.flags & 1? 'C' : ' ';
    e = fmtdesc.flags & 2? 'E' : ' ';
    printf("  %s: %c%c %s\n", fourcc, c, e, fmtdesc.description);
    fmtdesc.index++;
  }
}
