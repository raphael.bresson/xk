#ifndef XK_SRC_CAPTURE_CAM_COMMON_H_INCLUDED
#define XK_SRC_CAPTURE_CAM_COMMON_H_INCLUDED

void _cam_capture_read_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format);
void _cam_capture_mmap_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format);
void _cam_capture_userptr_init_fnct(const char* dev, xk_cam_capture_t* capture, uint32_t *w, uint32_t *h, int force_format);

#ifdef __XK_CAPTURE_INTERNAL__

static int xioctl(int fh, int request, void *arg)
{
  int r;
  do {
          r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);
  return r;
}

#ifdef __XK_CAPTURE_CAM_DO_NOTHING__

static void _cam_do_nothing(xk_cam_capture_t* capture)
{
}

#endif

#ifndef __XK_NO_CAPTURE_CONTROL__

static void _cam_open(xk_cam_capture_t* capture, const char* dev)
{
  struct stat st;
  XK_ERROR(stat(dev, &st) >= 0, "XK_CAPTURE_ERROR: Cannot identify `%s` : %s\n", dev, strerror(errno));
  XK_ERROR(S_ISCHR(st.st_mode) != 0, "XK_CAPTURE_ERROR: %s is not a device\n", dev);
  capture->fd = open(dev, O_RDWR | O_NONBLOCK);
  XK_ERROR(capture->fd >= 0, "XK_CAPTURE_ERROR: Cannot open `%s`: %s\n", dev, strerror(errno));
}

static void _cam_init_cropcap_fmt(xk_cam_capture_t* capture, const char* dev, uint32_t *w, uint32_t *h, int force_format, struct v4l2_format *fmt)
{
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  int ret;
  
  memset(&cropcap, VIDIOC_CROPCAP, sizeof(struct v4l2_cropcap));
  memset(&crop, VIDIOC_CROPCAP, sizeof(struct v4l2_crop));
  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if(xioctl(capture->fd, VIDIOC_CROPCAP, &cropcap) == 0)
  {
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect;
    ret = xioctl(capture->fd, VIDIOC_S_CROP, &crop);
    if(ret < 0)
    {
      XK_WARNING((errno != EINVAL), "XK_CAPTURE_WARNING: Cropping not supported\n");
      XK_WARNING((errno == EINVAL), "XK_CAPTURE_WARNING: VIDIOC_S_CROP: %s\n", strerror(errno));
    }
  }else
    XK_WARNING(0, "XK_CAPTURE_WARNING: VIDIOC_CROPCAP failed: %s\n", strerror(errno));
  
  memset(fmt, 0, sizeof(struct v4l2_format));
  fmt->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if(force_format)
  {
    XK_INFO("Set YUYV Format -> width: %d, height: %d\n", *w, *h);
    fmt->fmt.pix.width = *w;
    fmt->fmt.pix.height = *h;
    fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt->fmt.pix.field = V4L2_FIELD_ANY;
    fmt->fmt.pix.sizeimage = fmt->fmt.pix.bytesperline * fmt->fmt.pix.height;
    XK_ERROR( xioctl(capture->fd, VIDIOC_S_FMT, fmt) >= 0
            , "XK_CAPTURE_ERROR: VIDIOC_S_FMT : %s\n", strerror(errno));
    if(fmt->fmt.pix.pixelformat != V4L2_PIX_FMT_YUYV)
    {
      XK_WARNING(0,"YUYV format setting failed: not supported, pixelformat : %d != %d\n", fmt->fmt.pix.pixelformat, V4L2_PIX_FMT_RGB24);
      goto raw_fmt_set;
    }
  }else
  {
raw_fmt_set:
    XK_ERROR( xioctl(capture->fd, VIDIOC_G_FMT, fmt) >= 0
            , "XK_CAPTURE_ERROR: VIDIOC_G_FMT : %s\n", strerror(errno));
  }
  fmt->fmt.pix.bytesperline = max(fmt->fmt.pix.width*2, fmt->fmt.pix.bytesperline);
  fmt->fmt.pix.sizeimage = fmt->fmt.pix.bytesperline * fmt->fmt.pix.height;
  capture->w = fmt->fmt.pix.width;
  capture->h = fmt->fmt.pix.height;
  capture->depth = fmt->fmt.pix.bytesperline / capture->w;
}

#endif

#endif

#endif
