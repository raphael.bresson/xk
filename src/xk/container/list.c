#include <xk/container/list.h>

void xk_list_node_rec( struct xk_list_node *node
                     , void (*func)( void *listdata
                                   , void *userdata)
                     , void *userdata)
{
  if(node->prev)
    xk_list_node_rec(node->prev,func, userdata);
  func(node->userdata, userdata);
}

void xk_list_node_rrec( struct xk_list_node *node
                      , void (*func)( void *listdata
                                   , void *userdata)
                      , void *userdata)
{
  if(node->next)
    xk_list_node_rrec(node->next,func, userdata);
  func(node->userdata, userdata);
}

void xk_list_remove( struct xk_list * list
                   , struct xk_list_node * node)
{
  XK_ASSERT(list->end && list->begin, "list begin and end must be defined\n");
  XK_ASSERT(node->parent == list, "node parent must be the current list\n");
  if(list->end == list->begin)
  {
    XK_ASSERT(list->end == node, "list end must be the node\n");
    list->end = list->begin = NULL;
  }else
  {
    if(!node->prev)
    {
      XK_ASSERT(list->begin == node, "node must be the begin of the list\n");
      list->begin = node->next;
      node->next->prev = NULL;
    }else if(!node->next)
    {
      XK_ASSERT(list->end == node, "node must be the end of the list\n");
      list->end = node->prev;
      node->prev->next = NULL;
    }else
    {
      node->prev->next = node->next;
      node->next->prev = node->prev;
    }
  }
  node->parent = NULL;
  node->next = node->prev = NULL;
  list->count --;
}

void xk_list_append( struct xk_list      *list
                   , struct xk_list_node *node)
{
  if(!list->end)
  {
    XK_ASSERT(!list->begin, "Invalid list, end is NULL while begin isn't\n");
    list->begin = list->end = node;
  }else
  {
    list->end->next = node;
    node->prev = list->end;
    list->end = node;
  }
  node->parent = list;
  list->count++;
}

void xk_list_prepend( struct xk_list * list
                    , struct xk_list_node *node)
{
  if(list->begin == NULL)
  {
    XK_ASSERT(!list->end, "Invalid list, begin is NULL while end isn't\n");
    list->begin = list->end = node;
  }else
  {
    list->end->next = node;
    node->prev = list->end;
    list->end = node;
  }
  node->parent = list;
  list->count++;
}

void * xk_list_pop(struct xk_list *list)
{
  struct xk_list_node * node;
  node = list->begin;
  if(node)
  {
    xk_list_remove(list, node);
    return node->userdata;
  }
  return NULL;
}
