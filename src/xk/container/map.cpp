#include <unordered_map>
#include <vector>
#include <xk/common.h>
#include <xk/container/map.h>

using umap_t = std::unordered_map<std::string, std::vector<char>>;

xk_map_t* xk_map()
{
  xk_map_t* map = new umap_t;
  XK_ERROR(map, "XK_CONTAINER_ERROR: Cannot allocate a unordered_map\n");
  return map;
}

void _xk_map_put(xk_map_t* map, const char* key, void* element, uint32_t data_len)
{
  umap_t* m = (reinterpret_cast<umap_t*>(map));
  std::vector<char> vec(data_len);
  memcpy(vec.data(), element, data_len);
  m->emplace(key,vec);
}

void* xk_map_get(xk_map_t* map, const char* key)
{
  umap_t* umap = reinterpret_cast<umap_t*>(map);
  auto res = umap->find(key);
  if(res != umap->end())
    return res->second.data();
  else
    return NULL;
}

void xk_map_del(xk_map_t * map)
{
  delete reinterpret_cast<umap_t*>(map);
}
