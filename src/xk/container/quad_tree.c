#include <xk/container/quad_tree.h>

static inline void xk_quad_tree_node_init( struct xk_quad_tree_node * node
                                         , size_t x, size_t y
                                         , size_t w, size_t h)
{
  node->container = NULL;
  node->parent = node->ne = node->se = node->sw = node->nw = NULL;
  node->x = x; node->y = y; node->w = w; node->h = h;
  xk_list_init(&node->data_list);
}

void xk_quad_tree_node_append( struct xk_quad_tree_node *parent
                             , struct xk_quad_tree_node *node)
{
  if(node->x > parent->x)
  {
    if(node->y > parent->y)
    {
      if(parent->se)
        xk_quad_tree_node_append(parent->se, node);
      else
      {
        parent->se = node;
        node->parent = parent;
      }
    }else
    {
      if(parent->ne)
        xk_quad_tree_node_append(parent->ne, node);
      else
      {
        parent->ne = node;
        node->parent = parent;
      }
    }
  }else
  {
    if(node->y > parent->y)
    {
      if(parent->sw)
        xk_quad_tree_node_append(parent->sw, node);
      else
      {
        parent->sw = node;
        node->parent = parent;
      }
    }else
    {
      if(parent->nw)
        xk_quad_tree_node_append(parent->nw, node);
      else
      {
        parent->nw = node;
        node->parent = parent;
      }
    }
  }
}

struct xk_quad_tree_node * xk_quad_tree_node_build_tree( struct xk_quad_tree_node *head
                                                       , struct xk_quad_tree_node *nodes
                                                       , size_t current_depth
                                                       , size_t max_depth
                                                       , size_t x
                                                       , size_t y
                                                       , size_t w
                                                       , size_t h)
{
  struct xk_quad_tree_node *node;
  size_t wc, hc;
  wc = w >> 1;
  hc = h >> 1;
  xk_quad_tree_node_init(&nodes[0], x, y, wc, hc);
  xk_quad_tree_node_init(&nodes[1], x + wc, y, wc, hc);
  xk_quad_tree_node_init(&nodes[2], x, y + hc, wc, hc);
  xk_quad_tree_node_init(&nodes[3], x + wc, y + hc, wc, hc);
  xk_quad_tree_node_append(head,&nodes[0]);
  xk_quad_tree_node_append(head,&nodes[1]);
  xk_quad_tree_node_append(head,&nodes[2]);
  xk_quad_tree_node_append(head,&nodes[3]);
  if(current_depth < max_depth - 1)
  {
    nodes[0].is_leaf = 0;
    nodes[1].is_leaf = 0;
    nodes[2].is_leaf = 0;
    nodes[3].is_leaf = 0;
    node = xk_quad_tree_node_build_tree( &nodes[0], &nodes[4], current_depth + 1
                                       , max_depth, x, y, wc, hc);
    node = xk_quad_tree_node_build_tree( &nodes[1], node, current_depth + 1
                                       , max_depth, x + wc, y, wc, hc);
    node = xk_quad_tree_node_build_tree( &nodes[2], node, current_depth + 1
                                       , max_depth, x, y + hc, wc, hc);
    node = xk_quad_tree_node_build_tree( &nodes[3], node, current_depth + 1
                                       , max_depth, x + wc, y + hc, wc, hc);
    return node;
  }else
  {
    nodes[0].is_leaf = 1;
    nodes[1].is_leaf = 1;
    nodes[2].is_leaf = 1;
    nodes[3].is_leaf = 1;
    return &nodes[4];
  }
}

int xk_quad_tree_init( struct xk_quad_tree *tree, size_t depth, size_t w, size_t h)
{
  size_t i;
  tree->head = NULL; tree->depth = depth;
  tree->nnodes = pow(4,depth);
  tree->node_collection = (struct xk_quad_tree_node*) malloc(sizeof(struct xk_quad_tree_node) * tree->nnodes);
  if(!tree->node_collection)
  {
    return -ENOMEM;
  }
  for(i = 0 ; i < tree->nnodes; i++)
    tree->node_collection[i].container = tree;
  xk_quad_tree_node_init(&tree->node_collection[0], 0, 0, w, h);
  xk_quad_tree_node_build_tree( &tree->node_collection[0], &tree->node_collection[1]
                              , 1, depth, 0, 0, w, h);
  tree->head = &tree->node_collection[0];
  return 0;
}

void xk_quad_tree_release(struct xk_quad_tree *tree)
{
  free(tree->node_collection);
  tree->head = NULL;
  tree->depth = 0;
  tree->nnodes = 0;
  tree->node_collection = NULL;
}


void xk_quad_tree_append( struct xk_quad_tree *tree
                        , struct xk_list_node *node
                        , size_t x, size_t y
                        , size_t w, size_t h)
{
  struct xk_quad_tree_node *tnode;
  tnode = xk_quad_tree_find(tree,x,y,w,h);
  xk_list_append(&tnode->data_list, node);
}


void xk_quad_tree_node_foreach( struct xk_quad_tree_node *node
                              , int (*func)( void *datalist
                                           , void *userdata)
                              , void * userdata)
{
  struct xk_list_node *lnode;
  void *datalist;
  if(!xk_list_is_empty(&node->data_list))
  {
    xk_list_foreach(&node->data_list, datalist, lnode)
    {
      if(func(&node->data_list, userdata) <= 0)
        return;
    }
  }
  if(!node->is_leaf)
  {
    if(node->se)
      xk_quad_tree_node_foreach(node->se, func, userdata);
    if(node->ne)
      xk_quad_tree_node_foreach(node->ne, func, userdata);
    if(node->sw)
      xk_quad_tree_node_foreach(node->sw, func, userdata);
    if(node->nw)
      xk_quad_tree_node_foreach(node->nw, func, userdata);
  }
}

void xk_quad_tree_foreach( struct xk_quad_tree *tree
                         , int (*func)( void *datalist
                                      , void *userdata)
                         , void *userdata)
{
  xk_quad_tree_node_foreach(tree->head, func, userdata);
}

struct xk_quad_tree_node *xk_quad_tree_node_find( struct xk_quad_tree_node *parent
                                                , size_t x, size_t y, size_t w, size_t h)
{
  if(parent->is_leaf)
    return parent;
  if(((x + w) > (parent->x + parent->w)) || (y + h) > (parent->y + parent->h))
    return parent->parent;
  if(x > parent->x)
  {
    if(y > parent->y)
      return xk_quad_tree_node_find(parent->se,x,y,w,h);
    else
      return xk_quad_tree_node_find(parent->ne,x,y,w,h);
  }else
  {
    if(y > parent->y)
      return xk_quad_tree_node_find(parent->sw,x,y,w,h);
    else
      return xk_quad_tree_node_find(parent->nw,x,y,w,h);
  }
}

struct xk_quad_tree_node *xk_quad_tree_find( struct xk_quad_tree *tree
                                           , size_t x, size_t y
                                           , size_t w, size_t h)
{
  return xk_quad_tree_node_find(tree->head, x, y,w,h);
}

void xk_quad_tree_remove( struct xk_quad_tree *tree
                        , struct xk_list_node *node
                        , size_t x, size_t y, size_t w, size_t h)
{
  struct xk_quad_tree_node *tnode;
  tnode = xk_quad_tree_find(tree, x, y, w, h);
  xk_list_remove(&tnode->data_list, node);
}

void xk_quad_tree_node_print( struct xk_quad_tree_node *tnode
                            , void (*print_func)(FILE *stream, void *data)
                            , FILE *stream, size_t depth)
{
  size_t d;
  void *data;
  struct xk_list_node *lnode;
  for(d = 0 ; d < depth ; d++)
    fprintf(stream, "--");
  fprintf(stream, "--> (%lu,%lu;%lu,%lu):", tnode->x, tnode->y, tnode->w, tnode->h);
  if(xk_list_is_empty(&tnode->data_list))
    fprintf(stream, "empty\n");
  else
  {
    if(print_func)
    {
      xk_list_foreach(&tnode->data_list, data, lnode)
        print_func(stream,data);
      fprintf(stream, "\n");
    }else
      fprintf(stream, "not empty\n");
  }
  if(tnode->se)
    xk_quad_tree_node_print(tnode->se, print_func, stream, depth + 1);
  if(tnode->ne)
    xk_quad_tree_node_print(tnode->ne, print_func, stream, depth + 1);
  if(tnode->sw)
    xk_quad_tree_node_print(tnode->sw, print_func, stream, depth + 1);
  if(tnode->nw)
    xk_quad_tree_node_print(tnode->nw, print_func, stream, depth + 1);
}

void xk_quad_tree_print( struct xk_quad_tree *tree
                       , void (*print_func)(FILE *stream, void *data)
                       , FILE *stream)
{
  xk_quad_tree_node_print(tree->head, print_func, stream, 0);
}
