#include <xk/debug.h>
#include <stdio.h>
#if defined(__ARM_NEON__) || defined(__aarch64__)

void xk_print_vu8(uint8x16_t v)
{
  int i;
  union 
  {
    uint8x16_t vec;
    unsigned char scal[16];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 16 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vu16(uint16x8_t v)
{
  int i;
  union 
  {
    uint16x8_t vec;
    unsigned short scal[8];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 8 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vu32(uint32x4_t v)
{
  int i;
  union 
  {
    uint32x4_t vec;
    unsigned int scal[4];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 4 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi8(int8x16_t v)
{
  int i;
  union 
  {
    int8x16_t vec;
    signed char scal[16];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 16 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi16(int16x8_t v)
{
  int i;
  union 
  {
    int16x8_t vec;
    signed short scal[8];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 8 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi32(int32x4_t v)
{
  int i;
  union 
  {
    int32x4_t vec;
    signed int scal[4];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 4 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

#else

void xk_print_vu8(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    unsigned char scal[16];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 16 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vu16(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    unsigned short scal[8];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 8 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vu32(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    unsigned int scal[4];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 4 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi8(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    signed char scal[16];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 16 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi16(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    signed short scal[8];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 8 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

void xk_print_vi32(__m128i v)
{
  int i;
  union 
  {
    __m128i vec;
    signed int scal[4];
  } t;
  t.vec = v;
  printf("[ %d "   , t.scal[0]);
  for(i= 1 ; i < 4 ; i++)
    printf(", %d "   , t.scal[i]);
  printf(" ]\n");
}

#endif
