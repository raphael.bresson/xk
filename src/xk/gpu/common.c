#include <xk/gpu/common.h>

cl_platform_id* xk_gpu_platforms(cl_uint * count)
{
  clGetPlatformIDs(0, NULL, count);
  cl_platform_id* ids = (cl_platform_id*)malloc(*count * sizeof(cl_platform_id));
  XK_ERROR(ids, "XK_GPU_ERROR: Cannot allocate platform ids\n");
  clGetPlatformIDs(*count, ids, NULL);
  return ids;
}

cl_device_id* xk_gpu_devices( cl_platform_id platform
                            , cl_uint * count)
{
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, count);
  cl_device_id* ids = (cl_device_id*)malloc(*count * sizeof(cl_device_id));
  XK_ERROR(ids, "XK_GPU_ERROR: Cannot allocate platform ids\n");
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, *count, ids, NULL);
  return ids;
}

cl_context xk_gpu_context( cl_platform_id platform
                         , cl_device_id*  devices
                         , cl_uint        num_devices)
{
  const cl_context_properties properties [] = { CL_CONTEXT_PLATFORM, (cl_context_properties) platform, 0, 0 };
  cl_int err = CL_SUCCESS;
  cl_context context = clCreateContext(properties, num_devices, devices, NULL, NULL, &err);
  XK_ERROR(err == CL_SUCCESS, "XK_GPU_ERROR: Cannot create context: cl_error: %d\n", err);
  return context;
}

cl_mem xk_gpu_buffer( cl_context context
                    , void*      data
                    , size_t   size
                    , cl_uint    flags)
{
  cl_int err = CL_SUCCESS;
  cl_mem buffer = clCreateBuffer(context, flags, size, data, &err);
  XK_ERROR(err == CL_SUCCESS, "XK_GPU_ERROR: Cannot create buffer: cl_error: %d\n", err);
  return buffer;
}

cl_program xk_gpu_program( cl_context    context
                         , cl_device_id* devices
                         , cl_uint       num_devices
                         , const char*   src
                         , const char*   options)
{
  size_t lenghts[1] = { strlen(src) };
  const char* sources[1] = { src };
  cl_int err = CL_SUCCESS;
  cl_program program = clCreateProgramWithSource(context, 1, sources, lenghts, &err);
  XK_ERROR(err == CL_SUCCESS, "XK_GPU_ERROR: Cannot create program: cl_error: %d\n", err);
  err = clBuildProgram(program, num_devices, devices, options, NULL, NULL);
  if(err != CL_SUCCESS)
  {
    size_t serr = 512;
    char str[512];
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, serr, str, NULL);
    XK_ERROR(0, "XK_GPU_ERROR: Cannot build OpenCL program %d : %s\n", err, str);
  }
  return program;
}

cl_kernel xk_gpu_kernel( cl_program  program
                       , const char* name)
{
  cl_int err;
  cl_kernel kernel = clCreateKernel (program, name, &err);
  XK_ERROR(err == CL_SUCCESS, "XK_GPU_ERROR: Cannot create kernel: cl_error: %d\n", err);
  return kernel;
}

cl_command_queue xk_gpu_command_queue( cl_context    context
                                     , cl_device_id  device)
{
  cl_int err;
  cl_command_queue queue = clCreateCommandQueueWithProperties(context, device, NULL, &err);
  XK_ERROR(err == CL_SUCCESS, "XK_GPU_ERROR: Cannot create command queue: cl_error: %d\n", err);
  return queue;
}













void xk_gpu_append_buffer_arg(cl_kernel kernel, cl_mem buffer, cl_uint id)
{
  clSetKernelArg (kernel, id, sizeof (cl_mem), &buffer);
}

void xk_gpu_run_kernel1D( cl_kernel kernel
                        , cl_command_queue queue
                        , size_t size)
{
  size_t offs[2] = { 0, 0 };
  size_t sizes[2] = { size, 1 };
  XK_ERROR( clEnqueueNDRangeKernel (queue, kernel, 1, offs, sizes, NULL, 0, NULL, NULL) == CL_SUCCESS
          , "XK_GPU_ERROR: Cannot append run task to command queue\n");
}

void xk_gpu_run_kernel2D( cl_kernel kernel
                        , cl_command_queue queue
                        , size_t width
                        , size_t height)
{
  size_t offs[3] = { 0, 0, 0 };
  size_t size[3] = { width, height, 0 };
  size_t loc_size[3] = { 1, 1, 0 };
  XK_ERROR( clEnqueueNDRangeKernel (queue, kernel, 2, offs, size, loc_size, 0, NULL, NULL) == CL_SUCCESS
          , "XK_GPU_ERROR: Cannot append run task to command queue\n");
}

void xk_gpu_get_buffer( cl_command_queue queue
                      , cl_mem           buffer
                      , uint8_t          *data
                      , size_t           size)
{
  cl_int err = clEnqueueReadBuffer(queue, buffer, 1, 0, size, data, 0, NULL, NULL);
  XK_ERROR( err == CL_SUCCESS
          , "XK_GPU_ERROR: Cannot append read task to command queue : %d\n",err);
}

void xk_gpu_set_buffer( cl_command_queue queue
                      , cl_mem           buffer
                      , uint8_t          *data
                      , size_t           size)
{
  XK_ERROR( clEnqueueWriteBuffer(queue, buffer, 1, 0, size, data, 0, NULL, NULL) == CL_SUCCESS
          , "XK_GPU_ERROR: Cannot append write task to command queue\n");
}

void xk_gpu_platforms_del( cl_platform_id* ids
                         , cl_uint         num_platforms)
{
  free(ids);
}

void xk_gpu_devices_del( cl_device_id* ids
                       , cl_uint       num_platforms)
{
  uint32_t i = 0;
  for(i = 0 ; i < num_platforms ; i++)
    clReleaseDevice(ids[i]);
  free(ids);
}

void xk_gpu_devices_del_excluding_one( cl_device_id* ids
                                     , cl_device_id  excluded_device
                                     , cl_uint       num_platforms)
{
  uint32_t i = 0;
  for(i = 0 ; i < num_platforms ; i++)
  {
    if(excluded_device != ids[i])
      clReleaseDevice(ids[i]);
  }
  free(ids);
}

void xk_gpu_context_del(cl_context context)
{
  XK_ERROR(clReleaseContext(context) == CL_SUCCESS, "XK_GPU_ERROR: Cannot release context\n");
}

void xk_gpu_buffer_del(cl_mem buffer)
{
  XK_ERROR(clReleaseMemObject(buffer) == CL_SUCCESS, "XK_GPU_ERROR: Cannot release buffer\n");
}

void xk_gpu_program_del(cl_program program)
{
  XK_ERROR(clReleaseProgram(program) == CL_SUCCESS, "XK_GPU_ERROR: Cannot release program\n");
}

void xk_gpu_kernel_del(cl_kernel kernel)
{
  XK_ERROR(clReleaseKernel(kernel) == CL_SUCCESS, "XK_GPU_ERROR: Cannot release kernel\n");
}

void xk_gpu_command_queue_del(cl_command_queue queue)
{
  XK_ERROR(clReleaseCommandQueue(queue) == CL_SUCCESS, "XK_GPU_ERROR: Cannot release command queue\n");
}
