#include <xk/gpu/env.h>

static inline xk_gpu_env_t* _xk_gpu_env()
{
  xk_gpu_env_t* env = (xk_gpu_env_t*)malloc(sizeof(xk_gpu_env_t));
  XK_ERROR(env, "XK_GPU_ERROR: Cannot allocate GPU environement\n");
  return env;
}

xk_gpu_env_t* xk_gpu_env_default()
{
  cl_uint nb_platforms = 0, nb_devices = 0;
  xk_gpu_env_t* env = _xk_gpu_env();
  cl_platform_id* platforms = xk_gpu_platforms(&nb_platforms);
  cl_device_id*   devices   = xk_gpu_devices(platforms[0], &nb_devices);
  cl_context      context   = xk_gpu_context(platforms[0], devices, nb_devices);
  env->platform = platforms[0];
  env->device   = devices[0];
  env->context  = context;
  xk_gpu_devices_del_excluding_one(devices, env->device, nb_devices);
  xk_gpu_platforms_del(platforms, nb_platforms);
  return env;
}

xk_gpu_env_t* xk_gpu_env_from_platform(cl_platform_id platform)
{
  cl_uint nb_devices = 0;
  xk_gpu_env_t* env = _xk_gpu_env();
  cl_device_id*   devices   = xk_gpu_devices(platform, &nb_devices);
  cl_context      context   = xk_gpu_context(platform, devices, nb_devices);
  env->platform = platform;
  env->device   = devices[0];
  env->context  = context;
  xk_gpu_devices_del_excluding_one(devices, env->device, nb_devices);
  return env;
}

xk_gpu_env_t* xk_gpu_env_from_device( cl_platform_id platform
                                    , cl_device_id device)
{
  xk_gpu_env_t* env = _xk_gpu_env();
  cl_context      context   = xk_gpu_context(platform, &device, 1);
  env->platform = platform;
  env->device   = device;
  env->context  = context;
  return env;
}

xk_gpu_env_t* xk_gpu_env_from_context( cl_platform_id platform
                                     , cl_device_id   device
                                     , cl_context     context)
{
  xk_gpu_env_t* env = _xk_gpu_env();
  env->platform = platform;
  env->device   = device;
  env->context  = context;
  return env;
}

void xk_gpu_env_del(xk_gpu_env_t* env)
{
  xk_gpu_context_del(env->context);
  clReleaseDevice(env->device);
  free(env);
}
