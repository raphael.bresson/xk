#include <xk/gpu/image/morph.h>
#include <xk/gpu/image/kernels/dilate.h>
#include <xk/gpu/image/kernels/erode.h>

xk_gpu_task_t* xk_gpu_dilate_task( xk_gpu_env_t* env
                                 , uint8_t**     in
                                 , uint8_t**     out
                                 , uint32_t      image_width
                                 , uint32_t      image_height
                                 , uint32_t      morph_width
                                 , uint32_t      morph_height)
{
  char c_flags[512];
  uint32_t buf_size = image_width * image_height;
  const uint32_t work[] = { image_width, image_height };
  sprintf(c_flags, "-DMORPH_WIDTH=%d -DMORPH_HEIGHT=%d", morph_width, morph_height);
  return xk_gpu_task(env, xk_gpu_dilate_src, c_flags, "dilate_gray", 1, (void**)in, &buf_size, 0, NULL, NULL, 1, (void**)out, &buf_size, work, 2);
}

xk_gpu_task_t* xk_gpu_erode_task( xk_gpu_env_t* env
                                , uint8_t**     in
                                , uint8_t**     out
                                , uint32_t      image_width
                                , uint32_t      image_height
                                , uint32_t      morph_width
                                , uint32_t      morph_height)
{
  char c_flags[512];
  uint32_t buf_size = image_width * image_height;
  const uint32_t work[] = { image_width, image_height };
  sprintf(c_flags, "-DMORPH_WIDTH=%d -DMORPH_HEIGHT=%d", morph_width, morph_height);
  return xk_gpu_task(env, xk_gpu_erode_src, c_flags, "erode_gray", 1, (void**)in, &buf_size, 0, NULL, NULL, 1, (void**)out, &buf_size, work, 2);
}


xk_gpu_task_t* xk_gpu_dilate_task_from_buffers( xk_gpu_env_t* env
                                              , cl_mem        in
                                              , cl_mem        out
                                              , uint32_t      image_width
                                              , uint32_t      image_height
                                              , uint32_t      morph_width
                                              , uint32_t      morph_height)
{
  char c_flags[512];
  uint32_t buf_size = image_width * image_height;
  const uint32_t work[] = { image_width, image_height };
  sprintf(c_flags, "-DMORPH_WIDTH=%d -DMORPH_HEIGHT=%d", morph_width, morph_height);
  return xk_gpu_task_from_buffers(env, xk_gpu_dilate_src, c_flags, "dilate_gray", 1, &in, &buf_size, 0, NULL, NULL, 1, &out, &buf_size, work, 2);
}

xk_gpu_task_t* xk_gpu_erode_task_from_buffers( xk_gpu_env_t* env
                                             , cl_mem        in
                                             , cl_mem        out
                                             , uint32_t      image_width
                                             , uint32_t      image_height
                                             , uint32_t      morph_width
                                             , uint32_t      morph_height)
{
  char c_flags[512];
  uint32_t buf_size = image_width * image_height;
  const uint32_t work[] = { image_width, image_height };
  sprintf(c_flags, "-DMORPH_WIDTH=%d -DMORPH_HEIGHT=%d", morph_width, morph_height);
  return xk_gpu_task_from_buffers(env, xk_gpu_erode_src, c_flags, "erode_gray", 1, &in, &buf_size, 0, NULL, NULL, 1, &out, &buf_size, work, 2);
}

