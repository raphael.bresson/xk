#include <xk/gpu/image/run.h>

void _xk_gpu_image_run( xk_gpu_task_t*   task
                     , cl_command_queue queue
                     , uint8_t**        in
                     , uint8_t**        out)
{
  xk_gpu_task_set_in_buffer(task, 0, in[0], queue);
  xk_gpu_task_run(task, queue);
  xk_gpu_task_get_out_buffer(task, 0, out[0], queue);
}


void _xk_gpu_image_run_output_only( xk_gpu_task_t*   task
                                  , cl_command_queue queue
                                  , uint8_t**        out)
{
  xk_gpu_task_run(task, queue);
  xk_gpu_task_get_out_buffer(task, 0, out[0], queue);
}

void _xk_gpu_image_run_input_only( xk_gpu_task_t*   task
                                 , cl_command_queue queue
                                 , uint8_t**        in)
{
  xk_gpu_task_set_in_buffer(task, 0, in[0], queue);
  xk_gpu_task_run(task, queue);
}
