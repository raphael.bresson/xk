#include <xk/gpu/image/sigma_delta.h>
#include <xk/gpu/image/kernels/sigma_delta.h>
#include <xk/image/matrix.h>

xk_gpu_sigma_delta_task_t* xk_gpu_sigma_delta_task( xk_gpu_env_t* env
                                                  , uint8_t**     in
                                                  , uint8_t**     out
                                                  , uint32_t      image_width
                                                  , uint32_t      image_height
                                                  , uint32_t      N)
{
  xk_gpu_sigma_delta_task_t* task = (xk_gpu_sigma_delta_task_t*)malloc(sizeof(xk_gpu_sigma_delta_task_t));
  XK_ERROR(task, "XK_GPU_ERROR: Cannot allocate sigma delta task\n");
  xk_gpu_sigma_delta_task_init(task, env, in, out, image_width, image_height, N);
  return task;
}

void xk_gpu_sigma_delta_task_init( xk_gpu_sigma_delta_task_t* task
                                 , xk_gpu_env_t* env
                                 , uint8_t**     in
                                 , uint8_t**     out
                                 , uint32_t      image_width
                                 , uint32_t      image_height
                                 , uint32_t      N)
{
  char c_flags[512];
  uint32_t buf_size = image_width * image_height;
  const uint32_t work[] = { image_width, image_height };
  sprintf(c_flags, "-DN=%d", N);
  task->M = xk_u8_matrix(0, image_width-1, 0, image_height-1);
  task->O = xk_u8_matrix(0, image_width-1, 0, image_height-1);
  task->V = xk_u8_matrix(0, image_width-1, 0, image_height-1);
  uint32_t inoutsize[3] = { buf_size, buf_size, buf_size };
  uint8_t* inouts[3] = { task->M[0], task->O[0], task->V[0] };
  xk_gpu_task_init((xk_gpu_task_t*)task,env, xk_gpu_sigma_delta_src, c_flags, "sigma_delta_gray", 1, (void**)in, &buf_size, 3, (void**)inouts, inoutsize, 1, (void**)out, &buf_size, work, 2);
}

                                      
void xk_gpu_sigma_delta_task_del(xk_gpu_sigma_delta_task_t* task)
{
  xk_u8_matrix_del(task->M,0,0);
  xk_u8_matrix_del(task->O,0,0);
  xk_u8_matrix_del(task->V,0,0);
  xk_gpu_task_del((xk_gpu_task_t*)task);
}
