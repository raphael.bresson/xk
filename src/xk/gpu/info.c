#include <xk/gpu/info.h>

void xk_gpu_platform_info(cl_platform_id platform, xk_gpu_platform_info_t* info)
{
  size_t size_name, size_vendor, size_version, size_profile, size_extensions;
  clGetPlatformInfo(platform, CL_PLATFORM_NAME      , 0, NULL, &size_name);
  clGetPlatformInfo(platform, CL_PLATFORM_VENDOR    , 0, NULL, &size_vendor);
  clGetPlatformInfo(platform, CL_PLATFORM_VERSION   , 0, NULL, &size_version);
  clGetPlatformInfo(platform, CL_PLATFORM_PROFILE   , 0, NULL, &size_profile);
  clGetPlatformInfo(platform, CL_PLATFORM_EXTENSIONS, 0, NULL, &size_extensions);
  info->name       = (char*) malloc(size_name);
  info->vendor     = (char*) malloc(size_vendor);
  info->version    = (char*) malloc(size_version);
  info->profile    = (char*) malloc(size_profile);
  info->extensions = (char*) malloc(size_extensions);
  clGetPlatformInfo(platform, CL_PLATFORM_NAME      , size_name      , info->name      , NULL);
  clGetPlatformInfo(platform, CL_PLATFORM_VENDOR    , size_vendor    , info->vendor    , NULL);
  clGetPlatformInfo(platform, CL_PLATFORM_VERSION   , size_version   , info->version   , NULL);
  clGetPlatformInfo(platform, CL_PLATFORM_PROFILE   , size_profile   , info->profile   , NULL);
  clGetPlatformInfo(platform, CL_PLATFORM_EXTENSIONS, size_extensions, info->extensions, NULL);
}

void xk_gpu_platform_info_del(xk_gpu_platform_info_t* info)
{
  free(info->name);
  free(info->vendor);
  free(info->version);
  free(info->profile);
  free(info->extensions);
}

void xk_gpu_platform_info_print(xk_gpu_platform_info_t* info)
{
  printf("Platform: %s\n"
         "\tVendor : %s\n"
         "\tVersion: %s\n"
         "\tProfile: %s\n"
         "\tExtensions: %s\n"
         , info->name
         , info->vendor
         , info->version
         , info->profile
         , info->extensions);
}

void xk_gpu_device_info(cl_device_id device, xk_gpu_device_info_t* info)

{
  size_t size_name, size_dev_version, size_dri_version, size_cl_version;
  clGetDeviceInfo(device, CL_DEVICE_NAME            , 0, NULL, &size_name);
  clGetDeviceInfo(device, CL_DEVICE_VERSION         , 0, NULL, &size_dev_version);
  clGetDeviceInfo(device, CL_DRIVER_VERSION         , 0, NULL, &size_dri_version);
  clGetDeviceInfo(device, CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &size_cl_version);
  info->name = (char*) malloc(size_name);
  info->device_version = (char*) malloc(size_dev_version);
  info->driver_version = (char*) malloc(size_dri_version);
  info->cl_compiler_version = (char*) malloc(size_cl_version);
  clGetDeviceInfo(device, CL_DEVICE_NAME             , size_name       , info->name               , NULL);
  clGetDeviceInfo(device, CL_DEVICE_VERSION          , size_dev_version, info->device_version     , NULL);
  clGetDeviceInfo(device, CL_DRIVER_VERSION          , size_dri_version, info->driver_version     , NULL);
  clGetDeviceInfo(device, CL_DEVICE_OPENCL_C_VERSION , size_cl_version , info->cl_compiler_version, NULL);
  clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint) , &info->max_compute_units , NULL);
}

void xk_gpu_device_info_del(xk_gpu_device_info_t* info)
{
  free(info->name);
  free(info->device_version);
  free(info->driver_version);
  free(info->cl_compiler_version);
}

void xk_gpu_device_info_print(xk_gpu_device_info_t* info)
{
  printf("Device: %s\n"
         "\tdevice version     : %s\n"
         "\tdriver version     : %s\n"
         "\tCL compiler version: %s\n"
         "\tMax compute units  : %d\n"
         , info->name
         , info->device_version
         , info->driver_version
         , info->cl_compiler_version
         , info->max_compute_units);
}
