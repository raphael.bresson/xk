#include <xk/gpu/task.h>

xk_gpu_task_t* xk_gpu_task( xk_gpu_env_t*   env
                          , const char*     sources
                          , const char*     c_flags
                          , const char*     kernel_name
                          , uint32_t        num_cpu_in_buffers
                          , void**          cpu_in_buffers
                          , const uint32_t* cpu_in_buffers_sizes
                          , uint32_t        num_cpu_inout_buffers
                          , void**          cpu_inout_buffers
                          , const uint32_t* cpu_inout_buffers_sizes
                          , uint32_t        num_cpu_out_buffers
                          , void**          cpu_out_buffers
                          , const uint32_t* cpu_out_buffers_sizes
                          , const uint32_t* work_size
                          , const uint32_t  work_dims)
{
  xk_gpu_task_t* task = (xk_gpu_task_t*) malloc (sizeof(xk_gpu_task_t));
  XK_ERROR(task, "XK_GPU_ERROR: Cannot allocate task\n");
  xk_gpu_task_init( task
                  , env
                  , sources
                  , c_flags
                  , kernel_name
                  , num_cpu_in_buffers
                  , cpu_in_buffers
                  , cpu_in_buffers_sizes
                  , num_cpu_inout_buffers
                  , cpu_inout_buffers
                  , cpu_inout_buffers_sizes
                  , num_cpu_out_buffers
                  , cpu_out_buffers
                  , cpu_out_buffers_sizes
                  , work_size
                  , work_dims);
  return task;
}

xk_gpu_task_t* xk_gpu_task_from_buffers( xk_gpu_env_t*   env
                                       , const char*     sources
                                       , const char*     c_flags
                                       , const char*     kernel_name
                                       , uint32_t        num_cpu_in_buffers
                                       , cl_mem*         cpu_in_buffers
                                       , const uint32_t* cpu_in_buffers_sizes
                                       , uint32_t        num_cpu_inout_buffers
                                       , cl_mem*         cpu_inout_buffers
                                       , const uint32_t* cpu_inout_buffers_sizes
                                       , uint32_t        num_cpu_out_buffers
                                       , cl_mem*         cpu_out_buffers
                                       , const uint32_t* cpu_out_buffers_sizes
                                       , const uint32_t* work_size
                                       , const uint32_t  work_dims)
{
  xk_gpu_task_t* task = (xk_gpu_task_t*) malloc (sizeof(xk_gpu_task_t));
  XK_ERROR(task, "XK_GPU_ERROR: Cannot allocate task\n");
  xk_gpu_task_init_from_buffers( task
                               , env
                               , sources
                               , c_flags
                               , kernel_name
                               , num_cpu_in_buffers
                               , cpu_in_buffers
                               , cpu_in_buffers_sizes
                               , num_cpu_inout_buffers
                               , cpu_inout_buffers
                               , cpu_inout_buffers_sizes
                               , num_cpu_out_buffers
                               , cpu_out_buffers
                               , cpu_out_buffers_sizes
                               , work_size
                               , work_dims);
  return task;
}


void xk_gpu_task_init( xk_gpu_task_t*  task
                     , xk_gpu_env_t*   env
                     , const char*     sources
                     , const char*     c_flags
                     , const char*     kernel_name
                     , uint32_t        num_cpu_in_buffers
                     , void**          cpu_in_buffers
                     , const uint32_t* cpu_in_buffers_sizes
                     , uint32_t        num_cpu_inout_buffers
                     , void**          cpu_inout_buffers
                     , const uint32_t* cpu_inout_buffers_sizes
                     , uint32_t        num_cpu_out_buffers
                     , void**          cpu_out_buffers
                     , const uint32_t* cpu_out_buffers_sizes
                     , const uint32_t* work_size
                     , const uint32_t  work_dims)
{
  uint32_t i;
  task->program     = xk_gpu_program(env->context, &env->device, 1, sources, c_flags);
  task->kernel      = xk_gpu_kernel(task->program, kernel_name);
  task->num_inputs  = num_cpu_in_buffers;
  task->num_inouts  = num_cpu_inout_buffers;
  task->num_outputs = num_cpu_out_buffers;
  task->work_dims   = work_dims;
  task->inputs      = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_in_buffers);
  XK_ERROR(task->inputs, "XK_GPU_ERROR: Cannot allocate task inputs array\n");
  task->inouts      = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_inout_buffers);
  XK_ERROR(task->inouts, "XK_GPU_ERROR: Cannot allocate task inputs/outputs array\n");
  task->outputs     = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_out_buffers);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task outputs array\n");
  task->in_sizes    = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_in_buffers);
  XK_ERROR(task->inputs, "XK_GPU_ERROR: Cannot allocate task inputs sizes array\n");
  task->inout_sizes = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_inout_buffers);
  XK_ERROR(task->inouts, "XK_GPU_ERROR: Cannot allocate task inputs/outputs sizes array\n");
  task->out_sizes   = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_out_buffers);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task outputs sizes array\n");
  task->work_size   = (uint32_t*)malloc(sizeof(uint32_t) * work_dims);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task work size array\n");
  for(i = 0 ; i < num_cpu_in_buffers ; i++)
  {
    task->inputs[i]   = xk_gpu_buffer(env->context, &cpu_in_buffers[i], cpu_in_buffers_sizes[i], CL_MEM_READ_ONLY);
    task->in_sizes[i] = cpu_in_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->inputs[i], i);
  }
  for(i = 0 ; i < num_cpu_inout_buffers ; i++)
  {
    task->inouts[i]      = xk_gpu_buffer(env->context, &cpu_inout_buffers[i], cpu_inout_buffers_sizes[i], CL_MEM_READ_WRITE);
    task->inout_sizes[i] = cpu_inout_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->inouts[i], i + num_cpu_in_buffers);
  }
  for(i = 0 ; i < num_cpu_out_buffers ; i++)
  {
    task->outputs[i]   = xk_gpu_buffer(env->context, &cpu_out_buffers[i], cpu_out_buffers_sizes[i], CL_MEM_WRITE_ONLY);
    task->out_sizes[i] = cpu_out_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->outputs[i], i + num_cpu_in_buffers + num_cpu_inout_buffers);
  }
  for(i = 0 ; i < work_dims ; i++)
    task->work_size[i] = work_size[i];
}

void xk_gpu_task_init_from_buffers( xk_gpu_task_t*  task
                                  , xk_gpu_env_t*   env
                                  , const char*     sources
                                  , const char*     c_flags
                                  , const char*     kernel_name
                                  , uint32_t        num_cpu_in_buffers
                                  , cl_mem*         cpu_in_buffers
                                  , const uint32_t* cpu_in_buffers_sizes
                                  , uint32_t        num_cpu_inout_buffers
                                  , cl_mem*         cpu_inout_buffers
                                  , const uint32_t* cpu_inout_buffers_sizes
                                  , uint32_t        num_cpu_out_buffers
                                  , cl_mem*         cpu_out_buffers
                                  , const uint32_t* cpu_out_buffers_sizes
                                  , const uint32_t* work_size
                                  , const uint32_t  work_dims)
{
  uint32_t i;
  task->program     = xk_gpu_program(env->context, &env->device, 1, sources, c_flags);
  task->kernel      = xk_gpu_kernel(task->program, kernel_name);
  task->num_inputs  = num_cpu_in_buffers;
  task->num_inouts  = num_cpu_inout_buffers;
  task->num_outputs = num_cpu_out_buffers;
  task->work_dims   = work_dims;
  task->inputs      = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_in_buffers);
  XK_ERROR(task->inputs, "XK_GPU_ERROR: Cannot allocate task inputs array\n");
  task->inouts      = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_inout_buffers);
  XK_ERROR(task->inouts, "XK_GPU_ERROR: Cannot allocate task inputs/outputs array\n");
  task->outputs     = (cl_mem*) malloc(sizeof(cl_mem) * num_cpu_out_buffers);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task outputs array\n");
  task->in_sizes    = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_in_buffers);
  XK_ERROR(task->inputs, "XK_GPU_ERROR: Cannot allocate task inputs sizes array\n");
  task->inout_sizes = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_inout_buffers);
  XK_ERROR(task->inouts, "XK_GPU_ERROR: Cannot allocate task inputs/outputs sizes array\n");
  task->out_sizes   = (uint32_t*) malloc(sizeof(uint32_t) * num_cpu_out_buffers);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task outputs sizes array\n");
  task->work_size   = (uint32_t*)malloc(sizeof(uint32_t) * work_dims);
  XK_ERROR(task->outputs, "XK_GPU_ERROR: Cannot allocate task work size array\n");
  for(i = 0 ; i < num_cpu_in_buffers ; i++)
  {
    task->inputs[i]   = cpu_in_buffers[i];
    task->in_sizes[i] = cpu_in_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->inputs[i], i);
  }
  for(i = 0 ; i < num_cpu_inout_buffers ; i++)
  {
    task->inouts[i]      = cpu_inout_buffers[i];
    task->inout_sizes[i] = cpu_inout_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->inouts[i], i + num_cpu_in_buffers);
  }
  for(i = 0 ; i < num_cpu_out_buffers ; i++)
  {
    task->outputs[i]   = cpu_out_buffers[i];
    task->out_sizes[i] = cpu_out_buffers_sizes[i];
    xk_gpu_append_buffer_arg(task->kernel, task->outputs[i], i + num_cpu_in_buffers + num_cpu_inout_buffers);
  }
  for(i = 0 ; i < work_dims ; i++)
    task->work_size[i] = work_size[i];
}

void xk_gpu_task_set_in_buffer( xk_gpu_task_t* task
                              , uint32_t index
                              , void*    data
                              , cl_command_queue queue)
{
  xk_gpu_set_buffer(queue, task->inputs[index], data, task->in_sizes[index]);
}

void xk_gpu_task_set_inout_buffer( xk_gpu_task_t*   task
                                 , uint32_t         index
                                 , void*            data
                                 , cl_command_queue queue)
{
  xk_gpu_set_buffer(queue, task->inouts[index], data, task->inout_sizes[index]);
}

void xk_gpu_task_get_inout_buffer( xk_gpu_task_t*   task
                                 , uint32_t         index
                                 , void*            data
                                 , cl_command_queue queue)
{
  xk_gpu_get_buffer(queue, task->inouts[index], data, task->inout_sizes[index]);
}

void xk_gpu_task_get_out_buffer( xk_gpu_task_t*   task
                               , uint32_t         index
                               , void*            data
                               , cl_command_queue queue)
{
  xk_gpu_get_buffer(queue, task->outputs[index], data, task->out_sizes[index]);
}

void xk_gpu_task_run( xk_gpu_task_t* task
                    , cl_command_queue queue)
{
  switch(task->work_dims)
  {
    case 1: 
      xk_gpu_run_kernel1D(task->kernel, queue, task->work_size[0]);
      break;
    case 2:
      xk_gpu_run_kernel2D(task->kernel, queue, task->work_size[0], task->work_size[1]);
      break;
    default:
      XK_ERROR(0, "XK_GPU_ERROR: Number of dimensions not supported\n");
  }
}

void xk_gpu_task_del(xk_gpu_task_t* task)
{
  uint32_t i;
  for(i = 0 ; i < task->num_inputs ; i++)
    xk_gpu_buffer_del(task->inputs[i]);
  for(i = 0 ; i < task->num_inouts ; i++)
    xk_gpu_buffer_del(task->inouts[i]);
  for(i = 0 ; i < task->num_outputs ; i++)
    xk_gpu_buffer_del(task->outputs[i]);
  free(task->inputs);
  free(task->inouts);
  free(task->outputs);
  free(task->in_sizes);
  free(task->inout_sizes);
  free(task->out_sizes);
  free(task->work_size);
  free(task);
}
