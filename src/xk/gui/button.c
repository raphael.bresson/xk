#include <xk/gui/button.h>

xk_button_t* xk_button(const char* text, const char* font, uint32_t font_size_72dpi, uint32_t x, uint32_t y)
{
  xk_button_t* button = (xk_button_t*)malloc(sizeof(xk_button_t));
  XK_ERROR(button, "XK_GUI_ERROR: Cannot allocate a button\n");
  xk_button_init(button, text, font, font_size_72dpi, x, y);
  return button;
}

void xk_button_init(xk_button_t* button, const char* text, const char* font, uint32_t font_size_72dpi, uint32_t x, uint32_t y)
{
  xk_label_init((xk_label_t*)button, text, font, font_size_72dpi, x, y);
  button->btn_surface = SDL_CreateRGBSurface(0, button->w + 10, button->h + 10, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
  button->btn_border_surface = SDL_CreateRGBSurface(0, button->w + 12, button->h + 12, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
  SDL_FillRect(button->btn_border_surface, NULL, SDL_MapRGB(button->btn_border_surface->format, 255, 255, 255));
  SDL_Color released = { 0, 0, 0};
  SDL_Color pushed = { 255, 255, 255};
  SDL_Color font_pushed = { 0, 0, 0};
  SDL_Color font_released = { 255, 255, 255};
  memcpy(&button->btn_color, &released, sizeof(SDL_Color));
  memcpy(&button->btn_pressed_color, &pushed, sizeof(SDL_Color));
  memcpy(&button->font_pressed_color, &font_pushed, sizeof(SDL_Color));
  memcpy(&button->font_released_color, &font_released, sizeof(SDL_Color));
  xk_label_set_color((xk_label_t*)button, &font_released);
  button->click_event = NULL;
  button->release_event = NULL;
  button->draw = xk_button_draw;
  button->del = xk_button_del;
  button->on_click = xk_button_on_click;
  button->on_release = xk_button_on_release;
  button->state = RELEASED;
  button->data_onclick = NULL;
  button->data_onrelease = NULL;
  SDL_FillRect( button->btn_surface
              , NULL
              , SDL_MapRGB( button->btn_surface->format
                          , button->btn_color.r
                          , button->btn_color.g
                          , button->btn_color.b));
}

void xk_button_on_click(xk_widget_t* widget, uint32_t mouse_x, uint32_t mouse_y)
{
  xk_button_t* button = (xk_button_t*)widget;
  if(button->state == RELEASED)
  {
    xk_label_set_color((xk_label_t*)button, &button->font_pressed_color);
    SDL_FillRect( button->btn_surface
                , NULL
                , SDL_MapRGB( button->btn_surface->format
                            , button->btn_pressed_color.r
                            , button->btn_pressed_color.g
                            , button->btn_pressed_color.b));
    if(button->click_event)
      xk_signal_notify(button->click_event, button->data_onclick);
    button->state = PUSHED;
  }
}


void xk_button_on_release(xk_widget_t* widget, uint32_t mouse_x, uint32_t mouse_y)
{
  xk_button_t* button = (xk_button_t*)widget;
  if(button->state == PUSHED)
  {
    xk_label_set_color((xk_label_t*)button, &button->font_released_color);
    SDL_FillRect( button->btn_surface
                , NULL
                , SDL_MapRGB( button->btn_surface->format
                            , button->btn_color.r
                            , button->btn_color.g
                            , button->btn_color.b));
    if(button->release_event)
      xk_signal_notify(button->release_event, button->data_onrelease);
    button->state = RELEASED;
  }
}

void xk_button_draw(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_button_t* button = (xk_button_t*)widget;
  SDL_Rect r0 = { button->x - 6, button->y - 6, button->x + button->w + 6, button->y + button->h + 6 };
  SDL_BlitSurface(button->btn_border_surface, NULL, screen, &r0);
  SDL_Rect r1 = { button->x - 5, button->y - 5, button->x + button->w + 5, button->y + button->h + 5 };
  SDL_BlitSurface(button->btn_surface, NULL, screen, &r1);
  xk_label_draw(widget, screen);
}

void xk_button_set_onclick(xk_button_t* button, void callback(void*))
{
  if(button->click_event)
    xk_signal_del(button->click_event);
  button->click_event = xk_signal(callback);
}

void xk_button_set_onrelease(xk_button_t* button, void callback(void*))
{
  if(button->release_event)
    xk_signal_del(button->release_event);
  button->release_event = xk_signal(callback);
}

void xk_button_del(xk_widget_t* widget)
{
  xk_button_t* btn = (xk_button_t*)widget;
  if(btn->btn_surface)
    SDL_FreeSurface(btn->btn_surface);
  if(btn->btn_border_surface)
    SDL_FreeSurface(btn->btn_border_surface);
  if(btn->click_event)
    xk_signal_del(btn->click_event);
  if(btn->release_event)
    xk_signal_del(btn->release_event);
  xk_label_del(widget);
}

void xk_button_set_onclick_args(xk_button_t* button, void* args)
{
  button->data_onclick = args;
}

void xk_button_set_onrelease_args(xk_button_t* button, void* args)
{
  button->data_onrelease = args;
}
