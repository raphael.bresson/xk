#include <xk/gui/image.h>
#include <xk/image/interpolation.h>
#include <xk/gui/sdl_utils.h>
#include <xk/image/matrix.h>
#include <xk/common.h>

xk_image_widget_t* xk_image_widget( uint32_t x, uint32_t y
                                  , uint32_t w, uint32_t h)
{
  xk_image_widget_t* image_widget = (xk_image_widget_t*)malloc(sizeof(xk_image_widget_t));
  XK_ERROR(image_widget, "XK_GUI_ERROR: Cannot allocate image widget\n");
  xk_image_widget_init(image_widget, x, y, w, h);
  return image_widget;
}

void xk_image_widget_init( xk_image_widget_t* image_widget
                         , uint32_t x, uint32_t y
                         , uint32_t w, uint32_t h)
{
  xk_widget_init((xk_widget_t*)image_widget, x, y, w, h);
  image_widget->image = NULL;
  image_widget->draw = xk_image_widget_draw;
  image_widget->del = xk_image_widget_del;
}

void xk_image_widget_draw(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_image_widget_t* image_widget = (xk_image_widget_t*)widget;
  if(image_widget->image)
  {
    SDL_Rect r0 = { image_widget->x                  , image_widget->y
                  , image_widget->x + image_widget->w, image_widget->y + image_widget->h };
    SDL_BlitSurface(image_widget->image, NULL, screen, &r0); 
  }
}

void xk_image_widget_set_gray( xk_image_widget_t* image_widget
                             , uint8_t** image
                             , uint32_t w
                             , uint32_t h)
{
  uint8_t** tmp = NULL;
  if(image_widget->image)
    SDL_FreeSurface(image_widget->image);
  if((w != image_widget->w) || (h != image_widget->h))
  {
    tmp = xk_u8_matrix(0, image_widget->w-1, 0, image_widget->h-1);
    xk_bilinear_interpolation( image, tmp, w, h
                             , min( (double)(image_widget->w) / (double)w  
                                  , (double)(image_widget->h) / (double)h));
    image_widget->image = xk_gray_matrix_to_sdl_surface(tmp,image_widget->w,image_widget->h);
    xk_u8_matrix_del(tmp,0,0);
  }else
    image_widget->image = xk_gray_matrix_to_sdl_surface(image,image_widget->w,image_widget->h);
}

void xk_image_widget_set_rgb( xk_image_widget_t* image_widget
                            , uint8_t** image
                            , uint32_t w
                            , uint32_t h)
{
  uint8_t** tmp = NULL;
  if(image_widget->image)
    SDL_FreeSurface(image_widget->image);
  if((w != image_widget->w) || (h != image_widget->h))
  {
    tmp = xk_u8_matrix(0, image_widget->w*3-1, 0, image_widget->h);
    xk_bilinear_interpolation_rgb( image, tmp, w, h
                                 , min( (double)(image_widget->w) / (double)w  
                                      , (double)(image_widget->h) / (double)h));
    image_widget->image = xk_rgb_matrix_to_sdl_surface(tmp,image_widget->w,image_widget->h);
    xk_u8_matrix_del(tmp,0,0);
  }else
    image_widget->image = xk_rgb_matrix_to_sdl_surface(image,image_widget->w,image_widget->h);
}

void xk_image_widget_set_rgba( xk_image_widget_t* image_widget
                             , uint8_t** image
                             , uint32_t w
                             , uint32_t h)
{
  uint8_t** tmp = NULL;
  if(image_widget->image)
    SDL_FreeSurface(image_widget->image);
  if((w != image_widget->w) || (h != image_widget->h))
  {
    tmp = xk_u8_matrix(0, image_widget->w*4-1, 0, image_widget->h);
    xk_bilinear_interpolation_rgba( image, tmp, w, h
                                  , min( (double)(image_widget->w) / (double)w  
                                       , (double)(image_widget->h) / (double)h));
    image_widget->image = xk_rgba_matrix_to_sdl_surface(tmp,image_widget->w,image_widget->h);
    xk_u8_matrix_del(tmp,0,0);
  }else
    image_widget->image = xk_rgba_matrix_to_sdl_surface(image,image_widget->w,image_widget->h);
}

void xk_image_widget_del(xk_widget_t* widget)
{
  xk_image_widget_t* image_widget = (xk_image_widget_t*)widget;
  if(image_widget->image)
    SDL_FreeSurface(image_widget->image);
}
