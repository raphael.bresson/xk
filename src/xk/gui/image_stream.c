#include <xk/gui/image_stream.h>
#include <xk/image/matrix.h>

xk_image_stream_widget_t* xk_image_stream_widget( uint32_t x, uint32_t y
                                                , uint32_t w, uint32_t h
                                                , const char* local_addr
                                                , uint16_t    local_port
                                                , const char* peer_addr
                                                , uint16_t    peer_port
                                                , XK_IMAGE_COLOR color)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)malloc(sizeof(xk_image_stream_widget_t));
  XK_ERROR(stream, "XK_GUI_ERROR: Cannot allocate an image stream widget\n");
  xk_image_stream_widget_init(stream,x,y,w,h,local_addr,local_port,peer_addr,peer_port,color);
  return stream;
}

void* _th_stream_widget_callback(void* data)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)data;
  while(stream->stream->running)
  {
    usleep(200);
    if(!stream->paused)
    {
      uint8_t** tmp = NULL;
      tmp = xk_input_image_stream_next_frame(stream->stream);
      stream->image_changed = tmp != NULL;
      pthread_mutex_lock(&stream->stream_mutex);
        if(stream->streamed_image)
        {
          xk_u8_matrix_del(stream->streamed_image, 0, 0);
          stream->streamed_image = NULL;
        }
        stream->streamed_image = tmp;
      pthread_mutex_unlock(&stream->stream_mutex);
    }
  }
  return stream;
}


void xk_image_stream_widget_init( xk_image_stream_widget_t* stream
                                , uint32_t x, uint32_t y
                                , uint32_t w, uint32_t h
                                , const char* local_addr
                                , uint16_t    local_port
                                , const char* peer_addr
                                , uint16_t    peer_port
                                , XK_IMAGE_COLOR color)
{
  xk_image_widget_init((xk_image_widget_t*)stream,x,y,w,h);
  stream->streamed_image = NULL;
  stream->paused = 0;
  stream->image_changed = 0;
  if(color == XK_GRAY)
  {
    stream->stream = xk_input_image_stream(w, h, local_addr, local_port, peer_addr, peer_port);
    stream->draw   = xk_image_stream_widget_draw_gray;
  }
  else if(color == XK_RGB)
  {
    stream->stream = xk_input_image_stream(w*3, h, local_addr, local_port, peer_addr, peer_port);
    stream->draw   = xk_image_stream_widget_draw_rgb;
  }
  else if(color == XK_RGBA)
  {
    stream->stream = xk_input_image_stream(w*4, h, local_addr, local_port, peer_addr, peer_port);
    stream->draw   = xk_image_stream_widget_draw_rgba;
  }
  stream->del = xk_image_stream_widget_del;
  pthread_mutex_init(&stream->stream_mutex, NULL);
  pthread_create(&stream->stream_thread, NULL, _th_stream_widget_callback, stream);
}

void xk_image_stream_widget_draw_gray(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)widget;
    if(stream->image_changed && stream->streamed_image)
    {
      pthread_mutex_lock(&stream->stream_mutex);
      xk_image_widget_set_gray((xk_image_widget_t*)stream,stream->streamed_image, stream->w, stream->h);
      pthread_mutex_unlock(&stream->stream_mutex);
    }
  xk_image_widget_draw(widget, screen);
}

void xk_image_stream_widget_draw_rgb(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)widget;
    if(stream->image_changed && stream->streamed_image)
    {
      pthread_mutex_lock(&stream->stream_mutex);
      xk_image_widget_set_rgb((xk_image_widget_t*)stream,stream->streamed_image, stream->w, stream->h);
      pthread_mutex_unlock(&stream->stream_mutex);
    }
  xk_image_widget_draw(widget, screen);
}

void xk_image_stream_widget_draw_rgba(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)widget;
    if(stream->image_changed && stream->streamed_image)
    {
      pthread_mutex_lock(&stream->stream_mutex);
      xk_image_widget_set_rgba((xk_image_widget_t*)stream,stream->streamed_image, stream->w, stream->h);
      pthread_mutex_unlock(&stream->stream_mutex);
    }
  xk_image_widget_draw(widget, screen);
}

void xk_image_stream_widget_pause(xk_image_stream_widget_t* stream)
{
  stream->paused = 1;
}

void xk_image_stream_widget_start(xk_image_stream_widget_t* stream)
{
  stream->paused = 0;
}

void xk_image_stream_widget_del(xk_widget_t* widget)
{
  xk_image_stream_widget_t* stream = (xk_image_stream_widget_t*)widget;
  xk_image_stream_del(stream->stream);
  pthread_join(stream->stream_thread, NULL);
  if(stream->streamed_image)
  {
    xk_u8_matrix_del(stream->streamed_image, 0, 0);
    stream->streamed_image = NULL;
  }
  xk_image_widget_del(widget);
}
