#include <xk/gui/input.h>
#include <xk/common.h>

xk_gui_input_t* xk_gui_input()
{
  xk_gui_input_t* input = (xk_gui_input_t*)malloc(sizeof(xk_gui_input_t));
  XK_ERROR(input, "XK_GUI_ERROR: Cannot allocate input listener\n");
  for(uint32_t i = 0 ; i < SDL_NUM_SCANCODES ; i++)
  {
    input->key_pressed[i]  = 0;
    input->key_released[i] = 0;
  }
  for(uint32_t i = 0 ; i < 8 ; i++)
  {
    input->mouse_pressed[i]  = 0;
    input->mouse_released[i] = 0;
  }
  input->mouse_x     = 0;
  input->mouse_y     = 0;
  input->mouse_x_rel = 0;
  input->mouse_y_rel = 0;
  input->running = 1;
  return input;
}

void xk_gui_input_del(xk_gui_input_t* input)
{
  free(input);
}

void xk_gui_input_update(xk_gui_input_t* input)
{
  input->mouse_x_rel = 0;
  input->mouse_y_rel = 0;
  SDL_Event e;
  while(SDL_PollEvent(&e))
  {
    switch(e.type)
    {
      case SDL_KEYDOWN:
        input->key_pressed[e.key.keysym.scancode]  = 1;
        input->key_released[e.key.keysym.scancode] = 0;
        break;
      case SDL_KEYUP:
        input->key_pressed[e.key.keysym.scancode]  = 0;
        input->key_released[e.key.keysym.scancode] = 1;
        break;
      case SDL_MOUSEBUTTONDOWN:
        input->mouse_pressed[e.button.button]  = 1;
        input->mouse_released[e.button.button] = 0;
        break;
      case SDL_MOUSEBUTTONUP:
        input->mouse_pressed[e.button.button]  = 0;
        input->mouse_released[e.button.button] = 1;
        break;
      case SDL_MOUSEMOTION:
        input->mouse_x = e.motion.x;
        input->mouse_y = e.motion.y;
        input->mouse_x_rel = e.motion.xrel;
        input->mouse_y_rel = e.motion.yrel;
        break;
      case SDL_WINDOWEVENT:
        if(e.window.event == SDL_WINDOWEVENT_CLOSE)
          input->running = 0;
    };
  }
}

uint8_t xk_mouse_moved(xk_gui_input_t * input, uint32_t* x, uint32_t* y)
{
  int _x, _y;
  SDL_GetMouseState(&_x, &_y);
  *x = (uint32_t)_x; *y = (uint32_t)_y;
  return (input->mouse_x_rel != 0) || (input->mouse_y_rel != 0);
}

uint8_t xk_mouse_clicked(xk_gui_input_t * input, uint32_t* x, uint32_t* y)
{
  int _x, _y;
  SDL_GetMouseState(&_x, &_y);
  *x = (uint32_t)_x; *y = (uint32_t)_y;
  return (input->mouse_pressed[SDL_BUTTON_LEFT]) && (!input->mouse_released[SDL_BUTTON_LEFT]);
}

uint8_t xk_mouse_released(xk_gui_input_t * input, uint32_t* x, uint32_t* y)
{
  int _x, _y;
  SDL_GetMouseState(&_x, &_y);
  *x = (uint32_t)_x; *y = (uint32_t)_y;
  return (!input->mouse_pressed[SDL_BUTTON_LEFT]) && (input->mouse_released[SDL_BUTTON_LEFT]);
}
