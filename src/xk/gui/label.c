#include <xk/gui/label.h>

xk_label_t * xk_label( const char* text
                     , const char* fontfile
                     , uint32_t font_size_72dpi
                     , uint32_t x
                     , uint32_t y)
{
  xk_label_t* label = (xk_label_t*)malloc(sizeof(xk_label_t));
  XK_ERROR(label, "XK_GUI_ERROR: Cannot allocate label\n");
  xk_label_init(label,text, fontfile, font_size_72dpi, x, y);
  return label;
}

void xk_label_init( xk_label_t* label
                  , const char* text
                  , const char* fontfile
                  , uint32_t font_size_72dpi
                  , uint32_t x
                  , uint32_t y)
{
  int w, h, ret;
  SDL_Color col = { 255,255,255};
  memcpy(&label->font_color, &col, sizeof(SDL_Color));
  label->text = strdup(text);
  label->font = TTF_OpenFont(fontfile, font_size_72dpi);
  XK_ERROR(label->font, "XK_GUI_ERROR: Cannot open font: %s\n", TTF_GetError());
  ret = TTF_SizeText(label->font, text, &w, &h);
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot get size of a text %s\n", TTF_GetError());
  xk_widget_init((xk_widget_t*)label, x, y, (uint32_t)w, (uint32_t)h);
  label->draw = xk_label_draw;
  label->del  = xk_label_del;
  label->labelSurface = TTF_RenderText_Blended(label->font, text, label->font_color);
  XK_ERROR(label->labelSurface, "XK_GUI_ERROR: Cannot create surface of a text %s\n", TTF_GetError());
}

void xk_label_draw( xk_widget_t* widget
                  , SDL_Surface* screen)
{
  xk_label_t* label = (xk_label_t*)widget;
  SDL_Rect r0 = { label->x, label->y, label->x + label->w, label->y + label->h };
  SDL_BlitSurface(label->labelSurface, NULL, screen, &r0);
}

void xk_label_set_font( xk_label_t* label
                      , const char* fontfile
                      , uint32_t font_size_72dpi)
{
  int ret, w, h;
  if(label->font)
    TTF_CloseFont(label->font);
  if(label->labelSurface)
    SDL_FreeSurface(label->labelSurface);
  label->font = TTF_OpenFont(fontfile, font_size_72dpi);
  XK_ERROR(label->font, "XK_GUI_ERROR: Cannot open font: %s\n", TTF_GetError());
  ret = TTF_SizeText(label->font, label->text, &w, &h);
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot get size of a text %s\n", TTF_GetError());
  label->labelSurface = TTF_RenderText_Blended(label->font, label->text, label->font_color);
  XK_ERROR(label->labelSurface, "XK_GUI_ERROR: Cannot create surface of a text %s\n", TTF_GetError());
  label->w = (uint32_t)w;
  label->h = (uint32_t)h;
}


void xk_label_set_color(xk_label_t* label, SDL_Color* color)
{
  int ret, w, h;
  memcpy(&label->font_color, color, sizeof(SDL_Color));
  if(label->labelSurface)
    SDL_FreeSurface(label->labelSurface);
  ret = TTF_SizeText(label->font, label->text, &w, &h);
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot get size of a text %s\n", TTF_GetError());
  label->labelSurface = TTF_RenderText_Blended(label->font, label->text, label->font_color);
  XK_ERROR(label->labelSurface, "XK_GUI_ERROR: Cannot create surface of a text %s\n", TTF_GetError());
  label->w = (uint32_t)w;
  label->h = (uint32_t)h;
}

void xk_label_set_text(xk_label_t* label
                      , const char* text)
{
  int ret, w, h;
  if(label->text)
    free(label->text);
  label->text = strdup(text);
  if(label->labelSurface)
    SDL_FreeSurface(label->labelSurface);
  ret = TTF_SizeText(label->font, label->text, &w, &h);
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot get size of a text %s\n", TTF_GetError());
  label->labelSurface = TTF_RenderText_Blended(label->font, label->text, label->font_color);
  XK_ERROR(label->labelSurface, "XK_GUI_ERROR: Cannot create surface of a text %s\n", TTF_GetError());
  label->w = (uint32_t)w;
  label->h = (uint32_t)h;
}

void xk_label_del(xk_widget_t* widget)
{
  xk_label_t* label = (xk_label_t*)widget;
  if(label->font)
    TTF_CloseFont(label->font);
  if(label->labelSurface)
    SDL_FreeSurface(label->labelSurface);
  if(label->text)
    free(label->text);
  free(label);
}

