#include <xk/gui/scroll_bar.h>

void _on_value_changed_do_nothing(int value, void* nothing)
{}

void xk_scroll_bar_on_click(xk_widget_t* widget, uint32_t mouse_x, uint32_t mouse_y)
{
  xk_scroll_bar_t* bar = (xk_scroll_bar_t*) widget;
  if(((mouse_x >= bar->cursor_pos) && (mouse_x <= bar->cursor_pos)) || bar->is_dragged)
  {
    bar->value = (int)(((float)(mouse_x - bar->x) / (float)bar->w) * (float)(bar->max_value - bar->min_value));
    bar->on_value_changed(bar->value, bar->user_data_on_value_changed);
    bar->cursor_pos = mouse_x;
    bar->is_dragged = 1;
  }else if((mouse_y <= (bar->y + (bar->h / 2) - 2)) && (mouse_y >= (bar->h) ))
  {
    // click event
    bar->value = (int)(((float)(mouse_x - bar->x) / (float)bar->w) * (float)(bar->max_value - bar->min_value));
    bar->on_value_changed(bar->value, bar->user_data_on_value_changed);
    bar->cursor_pos = mouse_x;
  }
}
  
void xk_scroll_bar_on_release(xk_widget_t* widget, uint32_t mouse_x, uint32_t mouse_y)
{
  xk_scroll_bar_t* bar = (xk_scroll_bar_t*) widget;
  if(bar->is_dragged)
  {
    // cursor event
    bar->value = (int)(((float)(mouse_x - bar->x) / (float)bar->w) * (float)(bar->max_value - bar->min_value));
    bar->on_value_changed(bar->value, bar->user_data_on_value_changed);
    bar->cursor_pos = mouse_x;
    bar->is_dragged = 0;
  }
}

xk_scroll_bar_t* xk_scroll_bar(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  xk_scroll_bar_t* bar = (xk_scroll_bar_t*)malloc(sizeof(xk_scroll_bar_t));
  XK_ERROR(bar, "XK_GUI_ERROR: Cannot allocate scroll_bar object̀\n");
  xk_scroll_bar_init(bar, x, y, w, h);
  return bar;
}

void xk_scroll_bar_init(xk_scroll_bar_t* bar, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  xk_widget_init((xk_widget_t*)bar,x,y,w,h);
  bar->value = 0;
  bar->min_value = 0;
  bar->max_value = 100;
  bar->cursor_pos = bar->x;
  bar->cursor_width = max((uint32_t)((float)(w - 10) / (float)(bar->max_value - bar->min_value)), 1);
  bar->user_data_on_value_changed = NULL;
  bar->is_dragged = 0;
  bar->surface_bar = SDL_CreateRGBSurface(0, bar->w, 2, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
  bar->surface_cursor = SDL_CreateRGBSurface(0, bar->cursor_width, bar->h, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
  SDL_FillRect(bar->surface_bar, NULL, SDL_MapRGB(bar->surface_bar->format, 255, 255, 255));
  SDL_FillRect(bar->surface_cursor, NULL, SDL_MapRGB(bar->surface_cursor->format, 255, 255, 255));
  bar->draw = xk_scroll_bar_draw;
  bar->del = xk_scroll_bar_del;
  bar->on_click = xk_scroll_bar_on_click;
  bar->on_release = xk_scroll_bar_on_release;
  bar->on_value_changed = _on_value_changed_do_nothing;
}

void xk_scroll_bar_set_min(xk_scroll_bar_t* bar, int value)
{
  bar->min_value = value;
  // resize surfaces
}

void xk_scroll_bar_set_max(xk_scroll_bar_t* bar, int value)
{
  bar->max_value = value;
  // resize surfaces
}

void xk_scroll_bar_set_on_value_changed( xk_scroll_bar_t* bar
                                       , void (*on_value_changed)(int value, void* user_data)
                                       , void* user_data)
{
  bar->on_value_changed = on_value_changed;
  bar->user_data_on_value_changed = user_data;
}

void xk_scroll_bar_draw(xk_widget_t* widget, SDL_Surface* screen)
{
  xk_scroll_bar_t* bar = (xk_scroll_bar_t*)widget;
//   int cw_2 = bar->cursor_width / 2;
  int bary = bar->y + (bar->h / 2);
  SDL_Rect rect_bar = { bar->x, bary - 1, bar->w, 2 };
  SDL_BlitSurface(bar->surface_bar, NULL, screen, &rect_bar);
  SDL_Rect rect_cur = { bar->cursor_pos, bar->y, bar->cursor_width, bar->h };
  SDL_BlitSurface(bar->surface_cursor, NULL, screen, &rect_cur);
}

void xk_scroll_bar_del(xk_widget_t* widget)
{
  xk_scroll_bar_t* bar = (xk_scroll_bar_t*)widget;
  if(bar->surface_bar)
    SDL_FreeSurface(bar->surface_bar);
  if(bar->surface_cursor)
    SDL_FreeSurface(bar->surface_cursor);
  free(widget);
}
