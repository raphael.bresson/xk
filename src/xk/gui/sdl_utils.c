#include <xk/gui/sdl_utils.h>

SDL_Surface* xk_grayscale_blit_buffer(uint32_t w, uint32_t h)
{
  SDL_Color colors[256];
  SDL_Surface* sdl_surface = SDL_CreateRGBSurface(0, w, h, 8, 0, 0, 0, 0);
  for(int i = 0 ; i < 256; i++)
    colors[i].r = colors[i].g = colors[i].b = i;
  SDL_SetPaletteColors(sdl_surface->format->palette, colors, 0, 256);
  return sdl_surface;
}

SDL_Surface* xk_rgb_blit_buffer(uint32_t w, uint32_t h)
{
  return SDL_CreateRGBSurface(0, w, h, 8, 0x0000FF, 0x00FF00, 0xFF0000, 0);
}

SDL_Surface* xk_rgba_blit_buffer(uint32_t w, uint32_t h)
{
  return SDL_CreateRGBSurface(0, w, h, 8, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
}

void xk_gui_blit( SDL_Window* win
                , SDL_Surface *blit_buffer
                , uint8_t** image
                , uint32_t x, uint32_t y
                , uint32_t w, uint32_t h)
{
  SDL_Rect dest = { x, y, w, h };
  SDL_Surface *screen = SDL_GetWindowSurface(win);
  void* pixels = blit_buffer->pixels;
  blit_buffer->pixels = image[0];
  SDL_BlitSurface(blit_buffer, NULL, screen, &dest);
  blit_buffer->pixels = pixels;
}

SDL_Surface* xk_gray_matrix_to_sdl_surface( uint8_t** image
                                     , uint32_t w
                                     , uint32_t h)
{
  SDL_Color colors[256];
  SDL_Surface* sdl_surface = SDL_CreateRGBSurface(0, w, h, 8, 0, 0, 0, 0);
  for(int i = 0 ; i < 256; i++)
    colors[i].r = colors[i].g = colors[i].b = i;
  SDL_SetPaletteColors(sdl_surface->format->palette, colors, 0, 256);
  memcpy(sdl_surface->pixels, image[0], w*h);
  return sdl_surface;
}

SDL_Surface* xk_rgb_matrix_to_sdl_surface( uint8_t** image
                                         , uint32_t w
                                         , uint32_t h)
{
  SDL_Surface* sdl_surface = SDL_CreateRGBSurface(0, w, h, 24, 0x0000FF, 0x00FF00, 0xFF0000, 0);
  memcpy(sdl_surface->pixels, image[0], w*h*3);
  return sdl_surface;
}

SDL_Surface* xk_rgba_matrix_to_sdl_surface( uint8_t** image
                                         , uint32_t w
                                         , uint32_t h)
{
  SDL_Surface* sdl_surface = SDL_CreateRGBSurface(0, w, h, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
  memcpy(sdl_surface->pixels, image[0], w*h*4);
  return sdl_surface;
}


void xk_gui_init()
{
  int ret = SDL_Init(SDL_INIT_VIDEO);
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot init SDL : %s\n", SDL_GetError());
  ret = TTF_Init();
  XK_ERROR(ret >= 0, "XK_GUI_ERROR: Cannot init SDL_TTF : %s\n", TTF_GetError());
}

void xk_gui_close()
{
  TTF_Quit();
  SDL_Quit();
}

SDL_Window* xk_window(const char* name, uint32_t w, uint32_t h)
{
  return SDL_CreateWindow( name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED
                         , w, h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
}

void xk_swap_window(SDL_Window* win)
{
  SDL_UpdateWindowSurface(win);
  SDL_FillRect(SDL_GetWindowSurface(win), NULL, 0);
}
