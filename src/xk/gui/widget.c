#include <xk/gui/widget.h>
#include <xk/gui/widget_list.h>

void draw_do_nothing(xk_widget_t* w, SDL_Surface* s)
{}

void onclick_do_nothing(xk_widget_t* w, uint32_t x, uint32_t y)
{}

void _del_widget(xk_widget_t* widget)
{
  free(widget);
}

void xk_widget_init(xk_widget_t* widget, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  widget->draw = draw_do_nothing;
  widget->on_click = onclick_do_nothing;
  widget->on_release = onclick_do_nothing;
  widget->del = _del_widget;
  widget->x = x;
  widget->y = y;
  widget->w = w;
  widget->h = h;
  widget->children = NULL;
}

xk_widget_t* xk_widget(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  xk_widget_t* widget = (xk_widget_t*)malloc(sizeof(xk_widget_t));
  XK_ERROR("XK_GUI_ERROR: Cannot allocate widget\n");
  xk_widget_init(widget,x,y,w,h);
  return widget;
}
