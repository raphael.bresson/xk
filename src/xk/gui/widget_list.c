#include <xk/gui/widget_list.h>
#include <xk/gui/widget.h>
void _widget_list_destroy(xk_widget_list_node_t* n)
{
  if(n->next)
    _widget_list_destroy(n->next);
  n->widget->del(n->widget);
  free(n);
}

xk_widget_list_t *xk_widget_list()
{
  xk_widget_list_t* list = (xk_widget_list_t*)malloc(sizeof(xk_widget_list_t));
  XK_ERROR(list,"XK_GUI_ERROR: Cannot allocate widget list\n");
  list->head = NULL;
  list->tail = NULL;
  return list;
}

void _xk_widget_list_append( xk_widget_list_t* list
                           , xk_widget_t* w)
{
  xk_widget_list_node_t* n = (xk_widget_list_node_t*) malloc(sizeof(xk_widget_list_node_t));
  XK_ERROR(n,"XK_GUI_ERROR: Cannot allocate widget list node\n");
  n->next = NULL;
  n->widget = w;
  if(!list->head)
  {
    list->head = list->tail = n;
  }else
  {
    list->tail->next = n;
    list->tail = n;
  }
}

void xk_widget_list_clean(xk_widget_list_t *list)
{
  if(list)
  {
    _widget_list_destroy(list->head);
  }
}

void xk_widget_list_del(xk_widget_list_t *list)
{
  if(list)
  {
    _widget_list_destroy(list->head);
    free(list);
  }
}

void xk_widget_list_draw_all(xk_widget_list_t *list, SDL_Surface* screen)
{
  if(list)
  {
    xk_widget_list_node_t* n;
    for(n = list->head ; n != NULL ; n = n->next)
    {
      if(n->widget)
        xk_widget_draw(n->widget, screen);
    }
  }
}

void xk_widget_list_on_click(xk_widget_list_t* list, uint32_t mouse_x, uint32_t mouse_y)
{
  if(list)
  {
    
    xk_widget_list_node_t* n;
    for(n = list->head ; n != NULL ; n = n->next)
    {
      if(  (n->widget->x < mouse_x) 
        && (n->widget->y < mouse_y) 
        && ((n->widget->x + n->widget->w) > mouse_x) 
        && ((n->widget->y + n->widget->h) > mouse_y))
      {
        xk_widget_on_click(n->widget, mouse_x, mouse_y);
      }
    }
  }
}

void xk_widget_list_on_release(xk_widget_list_t* list, uint32_t mouse_x, uint32_t mouse_y)
{
  if(list)
  {
    xk_widget_list_node_t* n;
    for(n = list->head ; n != NULL ; n = n->next)
    {
      if(  (n->widget->x < mouse_x) 
        && (n->widget->y < mouse_y) 
        && ((n->widget->x + n->widget->w) > mouse_x) 
        && ((n->widget->y + n->widget->h) > mouse_y))
      {
        xk_widget_on_release(n->widget, mouse_x, mouse_y);
      }
    }
  }
}
