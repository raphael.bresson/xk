#include <xk/common.h>
#include <xk/image/blit.h>
#include <xk/image/matrix.h>
#include <xk/image/color.h>
#include <xk/debug.h>


void xk_blit_binary_to_rgb_scalar( uint8_t** rgb
                                 , uint8_t** binary
                                 , uint8_t** out
                                 , uint8_t color_r
                                 , uint8_t color_g
                                 , uint8_t color_b
                                 , uint32_t w
                                 , uint32_t h)
{
  uint32_t x,y;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      out[y][x*3+0] = rgb[y][x*3+0] | ((binary[y][x] == 0xFF ? 0xFF : 0x0) & color_r);
      out[y][x*3+1] = rgb[y][x*3+1] | ((binary[y][x] == 0xFF ? 0xFF : 0x0) & color_g);
      out[y][x*3+2] = rgb[y][x*3+2] | ((binary[y][x] == 0xFF ? 0xFF : 0x0) & color_b);
    }
  }
}


#if defined(__x86_64__) || defined(__amd64__) || defined(__i386) || defined(__i386__)

void xk_blit_binary_to_rgb( uint8_t** rgb
                          , uint8_t** binary
                          , uint8_t** out
                          , uint8_t color_r
                          , uint8_t color_g
                          , uint8_t color_b
                          , uint32_t w
                          , uint32_t h)
{ 
  uint32_t x,y;
  __m128i mem0, mem1, mem2;
  __m128i r, g, b;
  __m128i bin;
  const __m128i vmax = _mm_set1_epi8(0xFF);
  const __m128i vr   = _mm_set1_epi8(color_r);
  const __m128i vg   = _mm_set1_epi8(color_g);
  const __m128i vb   = _mm_set1_epi8(color_b);
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,r,g,b,bin,mem0,mem1,mem2) shared(w,h)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x+=16)
    {
      mem0 = _mm_load_si128((__m128i*)&rgb[y][x*3+0]); 
      mem1 = _mm_load_si128((__m128i*)&rgb[y][x*3+16]); 
      mem2 = _mm_load_si128((__m128i*)&rgb[y][x*3+32]);
      bin  = _mm_load_si128((__m128i*)&binary[y][x]);
      xk_desinterleave_rgb_u8(mem0, mem1, mem2, &r, &g, &b);
      r = _mm_or_si128(r, _mm_and_si128(vr, _mm_cmpeq_epi8(bin, vmax)));
      g = _mm_or_si128(g, _mm_and_si128(vg, _mm_cmpeq_epi8(bin, vmax)));
      b = _mm_or_si128(b, _mm_and_si128(vb, _mm_cmpeq_epi8(bin, vmax)));
      xk_interleave_rgb_u8(r, g, b, &mem0, &mem1, &mem2);
      _mm_store_si128((__m128i*)&out[y][x*3+0],mem0);
      _mm_store_si128((__m128i*)&out[y][x*3+16],mem1);
      _mm_store_si128((__m128i*)&out[y][x*3+32],mem2);
    }
  }
}

#else

void xk_blit_binary_to_rgb( uint8_t** rgb
                          , uint8_t** binary
                          , uint8_t** out
                          , uint8_t color_r
                          , uint8_t color_g
                          , uint8_t color_b
                          , uint32_t w
                          , uint32_t h)
{
  uint32_t x,y;
  uint8x16x3_t col;
  uint8x16_t bin;
  const uint8x16_t vmax = vdupq_n_u8(0xFF);
  const uint8x16_t vr = vdupq_n_u8(color_r);
  const uint8x16_t vg = vdupq_n_u8(color_g);
  const uint8x16_t vb = vdupq_n_u8(color_b);
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,bin,col)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x += 16)
    {
      col = vld3q_u8(&rgb[y][x*3]);
      bin = vld1q_u8(&binary[y][x]);
      col.val[0] = vorrq_u8(col.val[0], vandq_u8(vceqq_u8(vmax, bin), vr));
      col.val[1] = vorrq_u8(col.val[1], vandq_u8(vceqq_u8(vmax, bin), vg));
      col.val[2] = vorrq_u8(col.val[2], vandq_u8(vceqq_u8(vmax, bin), vb));
      vst3q_u8(&out[y][x*3],col);
    }
  }
}

#endif
