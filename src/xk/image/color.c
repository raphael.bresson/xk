#include <xk/image/color.h>
#include <xk/debug.h>

void xk_rgb_to_gray_scalar( uint8_t ** in
                          , uint8_t ** out
                          , uint32_t w
                          , uint32_t h)
{
  uint32_t x0, x1, y;
  uint16_t r,g,b;
  uint8_t res;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x0,x1,y,r,g,b,res)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x0 = 0, x1 = 0 ; x0 < w ; x0 += 3, x1++)
    {
      r = in[y][x0]; g = in[y][x0+1]; b = in[y][x0+2];
      res = (((r * 77) + (g * 151) + (b * 26)) >> 8);
      out[y][x1] = res;
    }
  }
}

#if defined(__x86_64__) || defined(__amd64__) || defined(__i386) || defined(__i386__)

#include <x86intrin.h>

void xk_rgb_to_gray( uint8_t ** in
                   , uint8_t ** out
                   , uint32_t w
                   , uint32_t h)
{
  uint32_t x0, x1, y;
  __m128i mem0, mem1, mem2, r, g, b, mr, mg, mb, res;
  __m128i r16_0, r16_1, g16_0, g16_1, b16_0, b16_1;
  __m128i rl16_0, rl16_1, gl16_0, gl16_1, bl16_0, bl16_1;
  __m128i resl16_0, resl16_1;
  const __m128i rev_mask  = _mm_setr_epi8( 8,  9,  10,  11,  12,  13,   14,  15,     -1,  -1,  -1,  -1,  -1,  -1,  -1, -1       );
  const __m128i high_mask = _mm_set_epi8( 14, 12,  10,   8,   6,   4,    2,   0,     -1,  -1,  -1,  -1,  -1,  -1,  -1, -1 );
  const __m128i low_mask  = _mm_set_epi8( -1, -1,  -1,  -1,  -1,  -1,   -1,  -1,     14,  12,  10,   8,   6,   4,   2,  0 );
  mr   = _mm_set1_epi16(77 );
  mg   = _mm_set1_epi16(151);
  mb   = _mm_set1_epi16(28 );
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x0,x1,y,r,g,b,mem0,mem1,mem2,res,r16_0, r16_1, g16_0, g16_1, b16_0, b16_1,rl16_0, rl16_1, gl16_0, gl16_1, bl16_0, bl16_1,resl16_0, resl16_1)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x0 = 0, x1 = 0 ; x0 < w ; x0 += 48, x1 += 16)
    {
      mem0 = _mm_load_si128((__m128i*)&in[y][  x0   ]); 
      mem1 = _mm_load_si128((__m128i*)&in[y][x0 + 16]); 
      mem2 = _mm_load_si128((__m128i*)&in[y][x0 + 32]);
      
      xk_desinterleave_rgb_u8(mem0, mem1, mem2, &r, &g, &b);
      
      r16_0 = _mm_cvtepu8_epi16(r); 
      r16_1 = _mm_cvtepu8_epi16(_mm_shuffle_epi8(r, rev_mask));
      g16_0 = _mm_cvtepu8_epi16(g); 
      g16_1 = _mm_cvtepu8_epi16(_mm_shuffle_epi8(g, rev_mask));
      b16_0 = _mm_cvtepu8_epi16(b); 
      b16_1 = _mm_cvtepu8_epi16(_mm_shuffle_epi8(b, rev_mask));
      
      rl16_0 = _mm_mullo_epi16(r16_0, mr); 
      rl16_1 = _mm_mullo_epi16(r16_1, mr);
      gl16_0 = _mm_mullo_epi16(g16_0, mg); 
      gl16_1 = _mm_mullo_epi16(g16_1, mg);
      bl16_0 = _mm_mullo_epi16(b16_0, mb); 
      bl16_1 = _mm_mullo_epi16(b16_1, mb);
      
      resl16_0 = _mm_srli_epi16(_mm_adds_epu16(_mm_adds_epu16(rl16_0, gl16_0), bl16_0), 8);
      resl16_1 = _mm_srli_epi16(_mm_adds_epu16(_mm_adds_epu16(rl16_1, gl16_1), bl16_1), 8);

      res = _mm_or_si128(_mm_shuffle_epi8(resl16_0, low_mask), _mm_shuffle_epi8(resl16_1, high_mask));
      
      _mm_store_si128((__m128i*)&out[y][x1], res);
    }
  }
}

#else

#include <arm_neon.h>

void xk_rgb_to_gray( uint8_t ** in
                   , uint8_t ** out
                   , uint32_t w
                   , uint32_t h)
{
  uint32_t x0, x1, y;
  uint8x8x3_t d;
  uint8x8_t mr, mg, mb, res;
  uint16x8_t tmp;
  mr = vdup_n_u8(77);
  mg = vdup_n_u8(151);
  mb = vdup_n_u8(28);
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x0,x1,y,tmp,res)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x0 = 0, x1 = 0 ; x0 < w ; x0 += 24, x1 += 8)
    {
      d   = vld3_u8(&in[y][x0]);
      tmp = vmull_u8(d.val[0], mr);
      tmp = vmlal_u8(tmp, d.val[1], mg);
      tmp = vmlal_u8(tmp, d.val[2], mb);
      res = vshrn_n_u16(tmp, 8);
      vst1_u8(&out[y][x1], res);
    }
  }
}

#ifdef XK_ARMV7
void xk_rgb_to_gray_asm( uint8_t ** in
                       , uint8_t ** out
                       , uint32_t w
                       , uint32_t h)
{
  uint32_t x0, x1, y;
  uint8x8_t mr, mg, mb;
  mr = vdup_n_u8(77);
  mg = vdup_n_u8(151);
  mb = vdup_n_u8(28);
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x0,x1,y)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    for(x0 = 0, x1 = 0 ; x0 < w ; x0 += 24, x1 += 8)
    {
      __asm__ volatile ( "vld3.8   {d0-d2}, [%[ref0]]\n\t"   // load and desinterleave
                         "vmull.u8  q3, d0, %[mr]\n\t"       // multiply red part by red factor and store the result in a vector of u16
                         "vmlal.u8  q3, d1, %[mg]\n\t"       // multiply green part by green factor and store the result in a vector of u16
                         "vmlal.u8  q3, d2, %[mb]\n\t"       // multiply blue part by blue factor and store the result in a vector of u16
                         "vshrn.u16 d6, q3, #8\n\t"          // divides by 2^8 (ie 256) and store the result in a vector of u8
                         "vst1.8    {d6}, [%[ref1]]\n\t"     // store u8 values in output address
                       :
                       : [ref0] "r" (&in[y][x0])  // input address reference
                       , [ref1] "r" (&out[y][x1]) // output address reference
                       , [mr] "w" (mr)            // red factor value (vector)
                       , [mg] "w" (mg)            // green factor value (vector)
                       , [mb] "w" (mb)            // blue factor value (vector)
                       );
    }
  }
}

#endif

#endif

#include <stdio.h>
#include <xk/common.h>

void xk_yuv_to_gray_scalar( uint8_t** in
                          , uint8_t** out
                          , uint32_t w
                          , uint32_t h)
{
  uint32_t x,y;
  for(y = 0 ; y < h ; y++)
  {
    for (x = 0; x < w; x++) 
    {
      out[y][x] = in[y][x*2];
    }
  }
}

void xk_yuv_to_rgb_scalar( uint8_t** in
                         , uint8_t** out
                         , uint32_t w
                         , uint32_t h)
{
  int32_t r0, g0, b0, r1, g1, b1;
  int32_t y0, y1, u, v;
  uint32_t x,y;
  for(y = 0 ; y < h ; y++)
  {
    for (x = 0; x < w/2; x++) 
    {
      y0 = in[y][x*4+0] << 8;
      y1 = in[y][x*4+2] << 8;
      u  = in[y][x*4+1] - 128;
      v  = in[y][x*4+3] - 128;

      r0 = (y0 + (359 * v)) >> 8;
      g0 = (y0 - (88  * u) - (183 * v)) >> 8;
      b0 = (y0 + (454 * u)) >> 8;

      r1 = (y1 + (359 * v)) >> 8;
      g1 = (y1 - (88  * u) - (183 * v)) >> 8;
      b1 = (y1 + (454 * u)) >> 8;
      
      out[y][x*6+0] = (uint8_t) max(0,min(r0, 255));
      out[y][x*6+1] = (uint8_t) max(0,min(g0, 255));
      out[y][x*6+2] = (uint8_t) max(0,min(b0, 255));
      
      out[y][x*6+3] = (uint8_t) max(0,min(r1, 255));
      out[y][x*6+4] = (uint8_t) max(0,min(g1, 255));
      out[y][x*6+5] = (uint8_t) max(0,min(b1, 255));
    }
  }
}


#if defined(__x86_64__) || defined(__amd64__) || defined(__i386) || defined(__i386__)

void xk_yuv_to_gray( uint8_t** in
                   , uint8_t** out
                   , uint32_t w
                   , uint32_t h)
{
  __m128i m0, m1;
  uint32_t x0, x1, y;
  for(y = 0 ; y < h ; y++)
  {
    for (x0 = 0, x1 = 0; x0 < (w * 2); x0 += 32, x1 += 16) 
    {
      m0 = _mm_load_si128((__m128i*)&in[y][x0]);
      m1 = _mm_load_si128((__m128i*)&in[y][x0+16]);
      _mm_store_si128((__m128i*)&out[y][x1], xk_yuv_to_gray_pix(m0, m1));
    }
  }
}

void xk_yuv_to_rgb( uint8_t** in
                  , uint8_t** out
                  , uint32_t w
                  , uint32_t h)
{
  __m128i yuv0, yuv1;
  __m128i ry8, ru8, rv8;
  __m128i ry16_0, ry16_1, ru16_0, ru16_1, rv16_0, rv16_1;
  __m128i ry32_0, ry32_1, ru32_0, ru32_1, rv32_0, rv32_1;
  __m128i ry32_2, ry32_3, ru32_2, ru32_3, rv32_2, rv32_3;
  __m128i r32_0, r32_1, r32_2, r32_3;
  __m128i g32_0, g32_1, g32_2, g32_3;
  __m128i b32_0, b32_1, b32_2, b32_3;
  __m128i r16_0, r16_1;
  __m128i g16_0, g16_1;
  __m128i b16_0, b16_1;
  __m128i r8, g8, b8;
  __m128i rgb0, rgb1, rgb2;
  const __m128i vzero  = _mm_set1_epi8(0);
  const __m128i v128  = _mm_set1_epi32(128);
  const __m128i crv   = _mm_set1_epi32(359);
  const __m128i cgu   = _mm_set1_epi32(88);
  const __m128i cgv   = _mm_set1_epi32(183);
  const __m128i cbu   = _mm_set1_epi32(454);
  uint32_t x0, x1, y;
  for(y = 0 ; y < h ; y++)
  {
    for (x0 = 0, x1 = 0; x0 < (w * 2); x0 += 32, x1 += 48) 
    {
      yuv0 = _mm_load_si128((__m128i*)&in[y][x0 + 0]);
      yuv1 = _mm_load_si128((__m128i*)&in[y][x0 + 16]);
      xk_desinterleave_yuv_u8(yuv0, yuv1, &ry8, &ru8, &rv8);
      // convert each yuv8 registers to vector of i16 registers
      ry16_0 = _mm_unpacklo_epi8(ry8, vzero); ry16_1 = _mm_unpackhi_epi8(ry8, vzero);
      ru16_0 = _mm_unpacklo_epi8(ru8, vzero); ru16_1 = _mm_unpackhi_epi8(ru8, vzero);
      rv16_0 = _mm_unpacklo_epi8(rv8, vzero); rv16_1 = _mm_unpackhi_epi8(rv8, vzero);
      ry16_0 = _mm_slli_epi16(ry16_0, 8);   ry16_1 = _mm_slli_epi16(ry16_1, 8); // y = y << 8
      // convert each yuv16 registers to vector of i32 registers
      ry32_0 = _mm_unpacklo_epi16(ry16_0, vzero); ry32_1 = _mm_unpackhi_epi16(ry16_0, vzero);
      ru32_0 = _mm_unpacklo_epi16(ru16_0, vzero); ru32_1 = _mm_unpackhi_epi16(ru16_0, vzero);
      rv32_0 = _mm_unpacklo_epi16(rv16_0, vzero); rv32_1 = _mm_unpackhi_epi16(rv16_0, vzero);
      ry32_2 = _mm_unpacklo_epi16(ry16_1, vzero); ry32_3 = _mm_unpackhi_epi16(ry16_1, vzero);
      ru32_2 = _mm_unpacklo_epi16(ru16_1, vzero); ru32_3 = _mm_unpackhi_epi16(ru16_1, vzero);
      rv32_2 = _mm_unpacklo_epi16(rv16_1, vzero); rv32_3 = _mm_unpackhi_epi16(rv16_1, vzero);
      ru32_0 = _mm_sub_epi32(ru32_0, v128); ru32_1 = _mm_sub_epi32(ru32_1, v128); // u = Cr - 128
      rv32_0 = _mm_sub_epi32(rv32_0, v128); rv32_1 = _mm_sub_epi32(rv32_1, v128); // v = Cb - 128
      ru32_2 = _mm_sub_epi32(ru32_2, v128); ru32_3 = _mm_sub_epi32(ru32_3, v128); // u = Cr - 128
      rv32_2 = _mm_sub_epi32(rv32_2, v128); rv32_3 = _mm_sub_epi32(rv32_3, v128); // v = Cb - 128
      // r = (y + 359 * v) >> 8
      r32_0 = _mm_srli_epi32(_mm_add_epi32(ry32_0, _mm_mullo_epi32(rv32_0,crv)), 8);
      r32_1 = _mm_srli_epi32(_mm_add_epi32(ry32_1, _mm_mullo_epi32(rv32_1,crv)), 8);
      r32_2 = _mm_srli_epi32(_mm_add_epi32(ry32_2, _mm_mullo_epi32(rv32_2,crv)), 8);
      r32_3 = _mm_srli_epi32(_mm_add_epi32(ry32_3, _mm_mullo_epi32(rv32_3,crv)), 8);
      // g = (y - 88 * u - 193 * v) >> 8
      g32_0 = _mm_srli_epi32(_mm_sub_epi32(_mm_sub_epi32(ry32_0, _mm_mullo_epi32(ru32_0,cgu)), _mm_mullo_epi32(rv32_0,cgv)), 8);
      g32_1 = _mm_srli_epi32(_mm_sub_epi32(_mm_sub_epi32(ry32_1, _mm_mullo_epi32(ru32_1,cgu)), _mm_mullo_epi32(rv32_1,cgv)), 8);
      g32_2 = _mm_srli_epi32(_mm_sub_epi32(_mm_sub_epi32(ry32_2, _mm_mullo_epi32(ru32_2,cgu)), _mm_mullo_epi32(rv32_2,cgv)), 8);
      g32_3 = _mm_srli_epi32(_mm_sub_epi32(_mm_sub_epi32(ry32_3, _mm_mullo_epi32(ru32_3,cgu)), _mm_mullo_epi32(rv32_3,cgv)), 8);
      //b = (y + 454 * u) >> 8
      b32_0 = _mm_srli_epi32(_mm_add_epi32(ry32_0, _mm_mullo_epi32(ru32_0,cbu)), 8);
      b32_1 = _mm_srli_epi32(_mm_add_epi32(ry32_1, _mm_mullo_epi32(ru32_1,cbu)), 8);
      b32_2 = _mm_srli_epi32(_mm_add_epi32(ry32_2, _mm_mullo_epi32(ru32_2,cbu)), 8);
      b32_3 = _mm_srli_epi32(_mm_add_epi32(ry32_3, _mm_mullo_epi32(ru32_3,cbu)), 8);
      // pack to vec i16
      r16_0 = _mm_packus_epi32(r32_0, r32_1); r16_1 = _mm_packs_epi32(r32_2, r32_3);
      g16_0 = _mm_packus_epi32(g32_0, g32_1); g16_1 = _mm_packs_epi32(g32_2, g32_3);
      b16_0 = _mm_packus_epi32(b32_0, b32_1); b16_1 = _mm_packs_epi32(b32_2, b32_3);
      // pack to vec u8
      r8 = _mm_packus_epi16(r16_0, r16_1); g8 = _mm_packus_epi16(g16_0, g16_1); b8 = _mm_packus_epi16(b16_0, b16_1);
      xk_interleave_rgb_u8(r8, g8, b8, &rgb0, &rgb1, &rgb2);
      _mm_store_si128((__m128i*)&out[y][x1 + 0 ], rgb0);
      _mm_store_si128((__m128i*)&out[y][x1 + 16], rgb1);
      _mm_store_si128((__m128i*)&out[y][x1 + 32], rgb2);
    }
  }
}

#else // ARM

// TODO
void xk_yuv_to_rgb( uint8_t** in
                  , uint8_t** out
                  , uint32_t w
                  , uint32_t h)
{
  xk_yuv_to_rgb_scalar(in, out, w, h);
}

void xk_yuv_to_gray( uint8_t** in
                   , uint8_t** out
                   , uint32_t w
                   , uint32_t h)
{
  xk_yuv_to_gray_scalar(in, out, w, h);
}

#endif
