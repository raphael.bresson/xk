#include <xk/image/image.h>
#include <xk/image/matrix.h>
#include <xk/common.h>
#include <stdio.h>
#include <string.h>

void _read_magic(FILE* f, char* s)
{
  do
  {
    fgets(s, 255, f);
  }while(s && (s[0] == '#'));
  s[2] = '\0';
}

void _read_dims(FILE* f, char* s, uint32_t * w, uint32_t * h, uint32_t * d)
{
  do
  {
    fgets(s, 255, f);
  }while(s && (s[0] == '#'));
  sscanf(s,"%d %d", w, h);
  fscanf(f, "%d\n", d);
}

void _read_pgm(FILE* f, uint8_t ** image, uint32_t w, uint32_t h)
{
  for(uint32_t y = 0 ; y < h ; y++)
    fread(&image[y][0], sizeof(uint8_t), w, f);
}

uint8_t** xk_load_pgm(const char* filename, uint32_t padw, uint32_t padh, uint32_t* w, uint32_t* h)
{
  char buffer[255];
  uint32_t d;
  uint8_t ** image;
  FILE *f;
  f = fopen(filename, "r");
  XK_ERROR(f,"PGM: CANNOT OPEN FILE (READ): \"%s\"\n", filename);
  _read_magic(f, buffer);
  XK_ERROR(strcmp(buffer, "P5") == 0
            , "PGM: INVALID MAGIC NUMBER: SUPPORTED: \"P5\" GOT: \"%s\" FOR FILE: \"%s\"\n"
            , buffer, filename);
  _read_dims(f, buffer, w, h, &d);
  XK_ERROR( d <= 255
           , "PGM: INVALID MAGIC NUMBER: SUPPORTED: \"<=255\" GOT: \"%d\" FOR FILE: \"%s\"\n"
           , d, filename);
  image = xk_u8_matrix(- padw, *w + padw - 1, - padh, *h + padh - 1);
  _read_pgm(f, image, *w, *h);
  fclose(f);
  return image;
}

void xk_read_pgm(const char* filename, uint8_t ** out, uint32_t w, uint32_t h)
{
  char buffer[255];
  uint32_t realw, realh, d;
  FILE *f;
  f = fopen(filename, "r");
  XK_ERROR(f,"PGM: CANNOT OPEN FILE (READ): \"%s\"\n", filename);
  _read_magic(f, buffer);
  XK_ERROR( strcmp(buffer, "P5") == 0
          , "PGM: INVALID MAGIC NUMBER: SUPPORTED: \"P5\" GOT: \"%s\" FOR FILE: \"%s\"\n"
          , buffer, filename);
  _read_dims(f, buffer, &realw, &realh, &d);
  XK_ERROR( d <= 255
           , "PGM: INVALID MAGIC NUMBER: SUPPORTED: \"<=255\" GOT: \"%d\" FOR FILE: \"%s\"\n"
           , d, filename);
  XK_ERROR( realh <= h
            , "PGM: INVALID W PARAM: EXPECTED: \"w <= %d\" GOT: \"w = %d\" FOR FILE: \"%s\"\n"
            , realw, w, filename);
  XK_ERROR( realh <= h
           , "PGM: INVALID H PARAM: EXPECTED: \"h <= %d\" GOT: \"h = %d\" FOR FILE: \"%s\"\n"
           , realh, h, filename);
  _read_pgm(f, out, w, h);
  fclose(f);
}

void xk_write_pgm(const char* filename, uint8_t ** image, uint32_t w, uint32_t h)
{
  FILE *f;
  f = fopen(filename, "w");
  XK_ERROR(f,"PGM: CANNOT OPEN FILE (WRITE): \"%s\"\n", filename);
  fprintf(f, "P5\n%d %d\n255\n", w, h);
  for(uint32_t y = 0 ; y < h ; y++)
    fwrite(&image[y][0], sizeof(uint8_t), w, f);
  fclose(f);
}

void _read_ppm(FILE* f, uint8_t ** image, uint32_t w, uint32_t h)
{
  for(uint32_t y = 0 ; y < h ; y++)
    fread(&image[y][0], sizeof(uint8_t), w*3, f);
}

uint8_t** xk_load_ppm(const char* filename, uint32_t padw, uint32_t padh, uint32_t* w, uint32_t* h)
{
  char buffer[255];
  uint32_t d;
  uint8_t ** image;
  FILE *f;
  f = fopen(filename, "r");
  XK_ERROR(f,"PPM: CANNOT OPEN FILE (READ): \"%s\"\n", filename);
  _read_magic(f, buffer);
  XK_ERROR(strcmp(buffer, "P6") == 0
           , "PPM: INVALID MAGIC NUMBER: SUPPORTED: \"P6\" GOT: \"%s\" FOR FILE: \"%s\"\n"
           , buffer, filename);
  _read_dims(f, buffer, w, h, &d);
  XK_ERROR( d <= 255
           , "PPM: INVALID MAGIC NUMBER: SUPPORTED: \"<=255\" GOT: \"%d\" FOR FILE: \"%s\"\n"
           , d, filename);
  image = xk_u8_matrix(- padw, (*w * 3) + padw - 1, - padh, (*h * 3) + padh - 1);
  _read_ppm(f, image, *w, *h);
  fclose(f);
  return image;
}

void xk_read_ppm(const char* filename, uint8_t ** out, uint32_t w, uint32_t h)
{
  char buffer[255];
  uint32_t realw, realh, d;
  FILE *f;
  f = fopen(filename, "r");
  XK_ERROR(f,"PPM: CANNOT OPEN FILE (READ): \"%s\"\n", filename);
  _read_magic(f, buffer);
  XK_ERROR( strcmp(buffer, "P6") == 0
           , "PPM: INVALID MAGIC NUMBER: SUPPORTED: \"P6\" GOT: \"%s\" FOR FILE: \"%s\"\n"
           , buffer, filename);
  _read_dims(f, buffer, &realw, &realh, &d);
  XK_ERROR( d <= 255
           , "PPM: INVALID MAGIC NUMBER: SUPPORTED: \"<=255\" GOT: \"%d\" FOR FILE: \"%s\"\n"
            , d, filename);
  XK_ERROR( realh <= h
           , "PPM: INVALID W PARAM: EXPECTED: \"w <= %d\" GOT: \"w = %d\" FOR FILE: \"%s\"\n"
           , realw, w, filename);
  XK_ERROR( realh <= h
           , "PPM: INVALID H PARAM: EXPECTED: \"h <= %d\" GOT: \"h = %d\" FOR FILE: \"%s\"\n"
           , realh, h, filename);
  _read_ppm(f, out, w, h);
  fclose(f);
}

void xk_write_ppm(const char* filename, uint8_t ** image, uint32_t w, uint32_t h)
{
  FILE *f;
  f = fopen(filename, "w");
  XK_ERROR(f,"PPM: CANNOT OPEN FILE (WRITE): \"%s\"\n", filename);
  fprintf(f, "P6\n%d %d\n255\n", w, h);
  for(uint32_t y = 0 ; y < h ; y++)
    fwrite(&image[y][0], sizeof(uint8_t), w * 3, f);
  fclose(f);
}
