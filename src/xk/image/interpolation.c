#include <xk/image/interpolation.h>

void xk_bilinear_interpolation(uint8_t** in, uint8_t** out, uint32_t w, uint32_t h, float factor)
{
  uint32_t x, y, ix, iy;
  float res = 0.f, dest_x, dest_y;
  for(y = 0 ; y < (h * factor)-1  ; y++)
  {
    dest_y = y / factor;
    iy = (uint32_t) min(dest_y, h-2);
    dest_y -= (float) iy;
    for(x = 0 ; x < (w * factor)-1 ; x++)
    {
      dest_x = x / factor;
      ix = (uint32_t)min(dest_x, w-2);
      dest_x -= (ix);
      res  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix]) + (dest_x * (float)in[iy][ix+1]));
      res += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix]) + (dest_x * (float)in[iy+1][ix+1]));
      out[y][x] = (uint8_t)res;
    }
  }
}


void xk_bilinear_interpolation_rgb(uint8_t** in, uint8_t** out, uint32_t w, uint32_t h, float factor)
{
  uint32_t x, y, ix, iy;
  float r = 0.f, g = 0.f, b = 0.f, dest_x, dest_y;
  for(y = 0 ; y < (h * factor)-1 ; y++)
  {
    dest_y = y / factor;
    iy = (uint32_t)dest_y;
    dest_y -= (float) iy;
    for(x = 0 ; x < (((uint32_t)(w * factor)) * 3) ; x+=3)
    {
      dest_x = x / factor;
      ix = (uint32_t)dest_x;
      dest_x -= (ix);
      ix -= ix % 3;
      r  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+0]) + (dest_x * (float)in[iy][ix+3]));
      g  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+1]) + (dest_x * (float)in[iy][ix+4]));
      b  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+2]) + (dest_x * (float)in[iy][ix+5]));
      r += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+0]) + (dest_x * (float)in[iy+1][ix+3]));
      g += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+1]) + (dest_x * (float)in[iy+1][ix+4]));
      b += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+2]) + (dest_x * (float)in[iy+1][ix+5]));
      out[y][x+0] = (uint8_t)r;
      out[y][x+1] = (uint8_t)g;
      out[y][x+2] = (uint8_t)b;
    }
  }
}


void xk_bilinear_interpolation_rgba(uint8_t** in, uint8_t** out, uint32_t w, uint32_t h, float factor)
{
  uint32_t x, y, ix, iy;
  float r = 0.f, g = 0.f, b = 0.f, a= 0.f, dest_x, dest_y;
  for(y = 0 ; y < (h * factor)  ; y++)
  {
    dest_y = y / factor;
    iy = (uint32_t) dest_y;
    dest_y -= (float) iy;
    for(x = 0 ; x < (((uint32_t)(w * factor)) * 4) ; x += 4)
    {
      dest_x = x / factor;
      ix = (uint32_t)dest_x;
      dest_x -= (ix);
      ix -= ix % 4;
      r  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+0]) + (dest_x * (float)in[iy][ix+4]));
      g  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+1]) + (dest_x * (float)in[iy][ix+5]));
      b  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+2]) + (dest_x * (float)in[iy][ix+6]));
      a  = (1.f - dest_y) * (((1.f - dest_x) * (float)in[iy][ix+3]) + (dest_x * (float)in[iy][ix+7]));
      r += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+0]) + (dest_x * (float)in[iy+1][ix+4]));
      g += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+1]) + (dest_x * (float)in[iy+1][ix+5]));
      b += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+2]) + (dest_x * (float)in[iy+1][ix+6]));
      a += dest_y * (((1.f - dest_x) * (float)in[iy+1][ix+3]) + (dest_x * (float)in[iy+1][ix+7]));
      out[y][x+0] = (uint8_t)r;
      out[y][x+1] = (uint8_t)g;
      out[y][x+2] = (uint8_t)b;
      out[y][x+3] = (uint8_t)a;
    }
  }
}
