#include <xk/image/matrix.h>
#include <xk/common.h>
#include <stdlib.h>
#include <string.h>

uint8_t** xk_u8_matrix( int32_t c0
                      , int32_t c1
                      , int32_t r0
                      , int32_t r1)
{
  uint8_t** mat;
  int i;
  mat = (uint8_t**)malloc(sizeof(uint8_t*) * (r1 - r0 + 1));
  XK_ERROR(mat, "error when allocating vector of rows of a matrix\n");
  mat -= r0;
  mat[r0] = (uint8_t*)aligned_alloc(16, sizeof(uint8_t) * (r1 - r0 + 1) * (c1 - c0 + 1));
  memset(mat[r0], 0, sizeof(uint8_t) * (r1 - r0 + 1) * (c1 - c0 + 1));
  XK_ERROR(mat[r0], "error when allocating data of a matrix\n");
  mat[r0] -= c0;
  for(i = r0 + 1 ; i < r1 + 1 ; i++) 
    mat[i] = mat[i-1] + (c1 - c0 + 1);
  return mat;
}

void xk_u8_matrix_del(uint8_t** mat, int32_t c0, int32_t r0)
{
  free(&mat[r0][c0]);
  free(&mat[r0]);
}

void xk_u8_matrix_copy( uint8_t** src
                   , uint8_t** dst
                   , uint32_t w
                   , uint32_t h)
{
  uint32_t y;
  for(y = 0; y < h ; y++)
    memcpy(dst[y], src[y], w * sizeof(uint8_t));
}

void xk_u8_matrix_from_ptr( uint8_t*  src
                       , uint8_t** dst
                       , uint32_t w
                       , uint32_t h)
{
  uint32_t y;
  for(y = 0; y < h ; y++)
    memcpy(dst[y], &src[y*w], w * sizeof(uint8_t));
}

uint8_t** xk_u8_matrix_view( uint32_t w
                        , uint32_t h)
{
  uint8_t** mat;
  //uint32_t i;
  mat = (uint8_t**)malloc(sizeof(uint8_t*) * h);
  XK_ERROR(mat, "error when allocating vector of rows of a matrix\n");
  //for(i = 0 ; i < h ; i++) 
  //  mat[i] = mat[i-1] + w;
  return mat;
}

void xk_u8_matrix_view_update( uint8_t*  src
                             , uint8_t** dst
                             , uint32_t w
                             , uint32_t h)
{
  uint32_t i;
  dst[0] = src;
  for(i = 1 ; i < h ; i++) 
    dst[i] = dst[i-1] + w;
}

void xk_u8_matrix_view_del(uint8_t** mat)
{
  free(mat);
}
