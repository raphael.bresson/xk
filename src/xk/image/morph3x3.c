#include <xk/image/morph.h>
#include <xk/common.h>

#if defined(__ARM_NEON__) || defined(__aarch64__)

#include <arm_neon.h>

void xk_u8_dilate_3x3( uint8_t** in
                     , uint8_t** out
                     , uint32_t w
                     , uint32_t h)
{
  int32_t x,y;
  uint8x16_t a0, b0, c0, ab0, bc0;
  uint8x16_t a1, b1, c1, ab1, bc1;
  uint8x16_t a2, b2 ,c2, ab2, bc2;
  uint8x16_t r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,ab0,ab1,ab2,bc0,bc1,bc2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = vdupq_n_u8(0);
    a1 = vdupq_n_u8(0);
    a2 = vdupq_n_u8(0);
    b0 = vld1q_u8(&in[y-1][0]);
    b1 = vld1q_u8(&in[ y ][0]);
    b2 = vld1q_u8(&in[y+1][0]);
    for(x = 0 ; x < w ; x += 16)
    {
      c0 = vld1q_u8(&in[y-1][x+16]);
      c1 = vld1q_u8(&in[ y ][x+16]);
      c2 = vld1q_u8(&in[y+1][x+16]);
     
      ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
      ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
      ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
     
      r0 = vmaxq_u8(vmaxq_u8(ab0, ab1),ab2);
      r1 = vmaxq_u8(vmaxq_u8( b0,  b1), b2);
      r2 = vmaxq_u8(vmaxq_u8(bc0, bc1),bc2);

      r = vmaxq_u8(vmaxq_u8(r0, r1), r2);

      vst1q_u8(&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}


void xk_u8_erode_3x3( uint8_t** in
                    , uint8_t** out
                    , uint32_t w
                    , uint32_t h)
{
  int32_t x,y;
  uint8x16_t a0, b0, c0, ab0, bc0;
  uint8x16_t a1, b1, c1, ab1, bc1;
  uint8x16_t a2, b2 ,c2, ab2, bc2;
  uint8x16_t r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,ab0,ab1,ab2,bc0,bc1,bc2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = vdupq_n_u8(0);
    a1 = vdupq_n_u8(0);
    a2 = vdupq_n_u8(0);
    b0 = vld1q_u8(&in[y-1][0]);
    b1 = vld1q_u8(&in[ y ][0]);
    b2 = vld1q_u8(&in[y+1][0]);
    for(x = 0 ; x < w ; x += 16)
    {
      c0 = vld1q_u8(&in[y-1][x+16]);
      c1 = vld1q_u8(&in[ y ][x+16]);
      c2 = vld1q_u8(&in[y+1][x+16]);
     
      ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
      ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
      ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
     
      r0 = vminq_u8(vminq_u8(ab0, ab1),ab2);
      r1 = vminq_u8(vminq_u8( b0,  b1), b2);
      r2 = vminq_u8(vminq_u8(bc0, bc1),bc2);

      r = vminq_u8(vminq_u8(r0, r1), r2);

      vst1q_u8(&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}


void xk_u8_dilate_3x3_no_padding( uint8_t ** in
                                , uint8_t ** out
                                , uint32_t w
                                , uint32_t h)
{
  int32_t x,y;
  uint8x16_t a0, b0, c0, ab0, bc0;
  uint8x16_t a1, b1, c1, ab1, bc1;
  uint8x16_t a2, b2 ,c2, ab2, bc2;
  uint8x16_t r0, r1, r2, r;
  
  const uint8x16_t vzero = vdupq_n_u8(0);
  
  /* 1st row */
  a1 = vdupq_n_u8(0);
  a2 = vdupq_n_u8(0);
  b1 = vld1q_u8(&in[0][0]);
  b2 = vld1q_u8(&in[1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c1 = vld1q_u8(&in[0][x+16]);
    c2 = vld1q_u8(&in[1][x+16]);
    
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
    ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
    
    r0 = vmaxq_u8(ab1,ab2);
    r1 = vmaxq_u8(b1, b2);
    r2 = vmaxq_u8(bc1,bc2);
    r = vmaxq_u8(vmaxq_u8(r0, r1), r2);

    vst1q_u8(&out[0][x], r);
    
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);//last col
  ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,vzero,1);
  r0 = vmaxq_u8(ab1,ab2);
  r1 = vmaxq_u8(b1, b2);
  r2 = vmaxq_u8(bc1,bc2);
  r = vmaxq_u8(vmaxq_u8(r0, r1), r2);
  vst1q_u8(&out[0][x], r);
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = vdupq_n_u8(0);
    a1 = vdupq_n_u8(0);
    a2 = vdupq_n_u8(0);
    b0 = vld1q_u8(&in[y-1][0]);
    b1 = vld1q_u8(&in[ y ][0]);
    b2 = vld1q_u8(&in[y+1][0]);
    for(x = 0 ; x < w - 16 ; x += 16)
    {
      c0 = vld1q_u8(&in[y-1][x+16]);
      c1 = vld1q_u8(&in[ y ][x+16]);
      c2 = vld1q_u8(&in[y+1][x+16]);
     
      ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
      ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
      ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
     
      r0 = vmaxq_u8(vmaxq_u8(ab0, ab1),ab2);
      r1 = vmaxq_u8(vmaxq_u8( b0,  b1), b2);
      r2 = vmaxq_u8(vmaxq_u8(bc0, bc1),bc2);

      r = vmaxq_u8(vmaxq_u8(r0, r1), r2);

      vst1q_u8(&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,vzero,1); // last col
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);
    ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,vzero,1);
    r0 = vmaxq_u8(vmaxq_u8(ab0, ab1),ab2);
    r1 = vmaxq_u8(vmaxq_u8( b0,  b1), b2);
    r2 = vmaxq_u8(vmaxq_u8(bc0, bc1),bc2);
    r = vmaxq_u8(vmaxq_u8(r0, r1), r2);
    vst1q_u8(&out[y][x], r);
  }
  /* last row */
  a0 = vdupq_n_u8(0);
  a1 = vdupq_n_u8(0);
  b0 = vld1q_u8(&in[h-2][0]);
  b1 = vld1q_u8(&in[h-1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c0 = vld1q_u8(&in[h-2][x+16]);
    c1 = vld1q_u8(&in[h-1][x+16]);
    
    ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
    
    r0 = vmaxq_u8(ab0, ab1);
    r1 = vmaxq_u8(b0, b1);
    r2 = vmaxq_u8(bc0,bc1);
    r = vmaxq_u8(vmaxq_u8(r0, r1), r2);

    vst1q_u8(&out[h-1][x], r);
    
    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1; 
  }
  ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,vzero,1);//last col
  ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);
  r0 = vmaxq_u8(ab0,ab1);
  r1 = vmaxq_u8(b0, b1);
  r2 = vmaxq_u8(bc0, bc1);
  r = vmaxq_u8(vmaxq_u8(r0, r1), r2);
  vst1q_u8(&out[h-1][x], r);
}

void xk_u8_erode_3x3_no_padding( uint8_t ** in
                               , uint8_t ** out
                               , uint32_t w
                               , uint32_t h)
{
  int32_t x,y;
  uint8x16_t a0, b0, c0, ab0, bc0;
  uint8x16_t a1, b1, c1, ab1, bc1;
  uint8x16_t a2, b2 ,c2, ab2, bc2;
  uint8x16_t r0, r1, r2, r;
  
  const uint8x16_t vzero = vdupq_n_u8(0xFF);
  
  /* 1st row */
  a1 = vdupq_n_u8(0xFF);
  a2 = vdupq_n_u8(0xFF);
  b1 = vld1q_u8(&in[0][0]);
  b2 = vld1q_u8(&in[1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c1 = vld1q_u8(&in[0][x+16]);
    c2 = vld1q_u8(&in[1][x+16]);
    
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
    ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
    
    r0 = vminq_u8(ab1,ab2);
    r1 = vminq_u8(b1, b2);
    r2 = vminq_u8(bc1,bc2);
    r = vminq_u8(vminq_u8(r0, r1), r2);

    vst1q_u8(&out[0][x], r);
    
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);//last col
  ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,vzero,1);
  r0 = vminq_u8(ab1,ab2);
  r1 = vminq_u8(b1, b2);
  r2 = vminq_u8(bc1,bc2);
  r = vminq_u8(vminq_u8(r0, r1), r2);
  vst1q_u8(&out[0][x], r);
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = vdupq_n_u8(0xFF);
    a1 = vdupq_n_u8(0xFF);
    a2 = vdupq_n_u8(0xFF);
    b0 = vld1q_u8(&in[y-1][0]);
    b1 = vld1q_u8(&in[ y ][0]);
    b2 = vld1q_u8(&in[y+1][0]);
    for(x = 0 ; x < w - 16 ; x += 16)
    {
      c0 = vld1q_u8(&in[y-1][x+16]);
      c1 = vld1q_u8(&in[ y ][x+16]);
      c2 = vld1q_u8(&in[y+1][x+16]);
     
      ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
      ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
      ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,c2,1);
     
      r0 = vminq_u8(vminq_u8(ab0, ab1),ab2);
      r1 = vminq_u8(vminq_u8( b0,  b1), b2);
      r2 = vminq_u8(vminq_u8(bc0, bc1),bc2);

      r = vminq_u8(vminq_u8(r0, r1), r2);

      vst1q_u8(&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,vzero,1); // last col
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);
    ab2 = vextq_u8(a2,b2,15); bc2 = vextq_u8(b2,vzero,1);
    r0 = vminq_u8(vminq_u8(ab0, ab1),ab2);
    r1 = vminq_u8(vminq_u8( b0,  b1), b2);
    r2 = vminq_u8(vminq_u8(bc0, bc1),bc2);
    r = vminq_u8(vminq_u8(r0, r1), r2);
    vst1q_u8(&out[y][x], r);
  }
  /* last row */
  a0 = vdupq_n_u8(0xFF);
  a1 = vdupq_n_u8(0xFF);
  b0 = vld1q_u8(&in[h-2][0]);
  b1 = vld1q_u8(&in[h-1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c0 = vld1q_u8(&in[h-2][x+16]);
    c1 = vld1q_u8(&in[h-1][x+16]);
    
    ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,c0,1);
    ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,c1,1);
    
    r0 = vminq_u8(ab0, ab1);
    r1 = vminq_u8(b0, b1);
    r2 = vminq_u8(bc0,bc1);
    r = vminq_u8(vminq_u8(r0, r1), r2);

    vst1q_u8(&out[h-1][x], r);
    
    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1; 
  }
  ab0 = vextq_u8(a0,b0,15); bc0 = vextq_u8(b0,vzero,1);//last col
  ab1 = vextq_u8(a1,b1,15); bc1 = vextq_u8(b1,vzero,1);
  r0 = vminq_u8(ab0,ab1);
  r1 = vminq_u8(b0, b1);
  r2 = vminq_u8(bc0, bc1);
  r = vminq_u8(vminq_u8(r0, r1), r2);
  vst1q_u8(&out[h-1][x], r);
}

#else

#include <x86intrin.h>

void xk_u8_dilate_3x3( uint8_t** in
                     , uint8_t** out
                     , uint32_t w
                     , uint32_t h)
{
  int32_t x,y;
  __m128i a0, b0, c0, ab0, bc0;
  __m128i a1, b1, c1, ab1, bc1;
  __m128i a2, b2 ,c2, ab2, bc2;
  __m128i r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,ab0,ab1,ab2,bc0,bc1,bc2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = _mm_set1_epi8(0);
    a1 = _mm_set1_epi8(0);
    a2 = _mm_set1_epi8(0);
    b0 = _mm_load_si128((__m128i*)&in[y-1][0]);
    b1 = _mm_load_si128((__m128i*)&in[ y ][0]);
    b2 = _mm_load_si128((__m128i*)&in[y+1][0]);
    for(x = 0 ; x < w ; x += 16)
    {
      c0 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c1 = _mm_load_si128((__m128i*)&in[ y ][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
     
      ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
      ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
      ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
     
      r0 = _mm_max_epu8(_mm_max_epu8(ab0, ab1),ab2);
      r1 = _mm_max_epu8(_mm_max_epu8( b0,  b1), b2);
      r2 = _mm_max_epu8(_mm_max_epu8(bc0, bc1),bc2);

      r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);

      _mm_store_si128((__m128i*)&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}


void xk_u8_erode_3x3( uint8_t** in
                    , uint8_t** out
                    , uint32_t w
                    , uint32_t h)
{
  int32_t x,y;
  __m128i a0, b0, c0, ab0, bc0;
  __m128i a1, b1, c1, ab1, bc1;
  __m128i a2, b2 ,c2, ab2, bc2;
  __m128i r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,ab0,ab1,ab2,bc0,bc1,bc2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = _mm_set1_epi8(0xFF);
    a1 = _mm_set1_epi8(0xFF);
    a2 = _mm_set1_epi8(0xFF);
    b0 = _mm_load_si128((__m128i*)&in[y-1][0]);
    b1 = _mm_load_si128((__m128i*)&in[ y ][0]);
    b2 = _mm_load_si128((__m128i*)&in[y+1][0]);
    for(x = 0 ; x < w ; x += 16)
    {
      c0 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c1 = _mm_load_si128((__m128i*)&in[ y ][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
     
      ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
      ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
      ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
     
      r0 = _mm_min_epu8(_mm_min_epu8(ab0, ab1),ab2);
      r1 = _mm_min_epu8(_mm_min_epu8( b0,  b1), b2);
      r2 = _mm_min_epu8(_mm_min_epu8(bc0, bc1),bc2);

      r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);

      _mm_store_si128((__m128i*)&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}


void xk_u8_dilate_3x3_no_padding( uint8_t ** in
                                , uint8_t ** out
                                , uint32_t w
                                , uint32_t h)
{
  int32_t x,y;
  __m128i a0, b0, c0, ab0, bc0;
  __m128i a1, b1, c1, ab1, bc1;
  __m128i a2, b2 ,c2, ab2, bc2;
  __m128i r0, r1, r2, r;
  
  const __m128i vzero = _mm_set1_epi8(0);
  
  /* 1st row */
  a1 = _mm_set1_epi8(0);
  a2 = _mm_set1_epi8(0);
  b1 = _mm_load_si128((__m128i*)&in[0][0]);
  b2 = _mm_load_si128((__m128i*)&in[1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c1 = _mm_load_si128((__m128i*)&in[0][x+16]);
    c2 = _mm_load_si128((__m128i*)&in[1][x+16]);
    
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
    ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
    
    r0 = _mm_max_epu8(ab1,ab2);
    r1 = _mm_max_epu8(b1, b2);
    r2 = _mm_max_epu8(bc1,bc2);
    r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);

    _mm_store_si128((__m128i*)&out[0][x], r);
    
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);//last col
  ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(vzero,b2,1);
  r0 = _mm_max_epu8(ab1,ab2);
  r1 = _mm_max_epu8(b1, b2);
  r2 = _mm_max_epu8(bc1,bc2);
  r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);
  _mm_store_si128((__m128i*)&out[0][x], r);
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = _mm_set1_epi8(0);
    a1 = _mm_set1_epi8(0);
    a2 = _mm_set1_epi8(0);
    b0 = _mm_load_si128((__m128i*)&in[y-1][0]);
    b1 = _mm_load_si128((__m128i*)&in[ y ][0]);
    b2 = _mm_load_si128((__m128i*)&in[y+1][0]);
    for(x = 0 ; x < w-16 ; x += 16)
    {
      c0 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c1 = _mm_load_si128((__m128i*)&in[ y ][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
     
      ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
      ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
      ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
     
      r0 = _mm_max_epu8(_mm_max_epu8(ab0, ab1),ab2);
      r1 = _mm_max_epu8(_mm_max_epu8( b0,  b1), b2);
      r2 = _mm_max_epu8(_mm_max_epu8(bc0, bc1),bc2);

      r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);

      _mm_store_si128((__m128i*)&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(vzero,b0,1);//last col
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);
    ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(vzero,b2,1);
    r0 = _mm_max_epu8(ab0, _mm_max_epu8(ab1,ab2));
    r1 = _mm_max_epu8(b0 , _mm_max_epu8(b1, b2));
    r2 = _mm_max_epu8(bc0, _mm_max_epu8(bc1,bc2));
    r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);
    _mm_store_si128((__m128i*)&out[y][x], r);
  }
  /* last row */
  a0 = _mm_set1_epi8(0);
  a1 = _mm_set1_epi8(0);
  b0 = _mm_load_si128((__m128i*)&in[h-2][0]);
  b1 = _mm_load_si128((__m128i*)&in[h-1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c0 = _mm_load_si128((__m128i*)&in[h-2][x+16]);
    c1 = _mm_load_si128((__m128i*)&in[h-1][x+16]);
    
    ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
    
    r0 = _mm_max_epu8(ab0,ab1);
    r1 = _mm_max_epu8(b0, b1);
    r2 = _mm_max_epu8(bc0,bc1);
    r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);

    _mm_store_si128((__m128i*)&out[h-1][x], r);
    
    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1; 
  }
  ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(vzero,b0,1);//last col
  ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);
  r0 = _mm_max_epu8(ab0,ab1);
  r1 = _mm_max_epu8(b0, b1);
  r2 = _mm_max_epu8(bc0, bc1);
  r = _mm_max_epu8(_mm_max_epu8(r0, r1), r2);
  _mm_store_si128((__m128i*)&out[h-1][x], r);
}

void xk_u8_erode_3x3_no_padding( uint8_t ** in
                               , uint8_t ** out
                               , uint32_t w
                               , uint32_t h)
{
  int32_t x,y;
  __m128i a0, b0, c0, ab0, bc0;
  __m128i a1, b1, c1, ab1, bc1;
  __m128i a2, b2 ,c2, ab2, bc2;
  __m128i r0, r1, r2, r;
  
  const __m128i vzero = _mm_set1_epi8(0xFF);
  
  /* 1st row */
  a1 = _mm_set1_epi8(0xFF);
  a2 = _mm_set1_epi8(0xFF);
  b1 = _mm_load_si128((__m128i*)&in[0][0]);
  b2 = _mm_load_si128((__m128i*)&in[1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c1 = _mm_load_si128((__m128i*)&in[0][x+16]);
    c2 = _mm_load_si128((__m128i*)&in[1][x+16]);
    
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
    ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
    
    r0 = _mm_min_epu8(ab1,ab2);
    r1 = _mm_min_epu8(b1, b2);
    r2 = _mm_min_epu8(bc1,bc2);
    r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);

    _mm_store_si128((__m128i*)&out[0][x], r);
    
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);//last col
  ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(vzero,b2,1);
  r0 = _mm_min_epu8(ab1,ab2);
  r1 = _mm_min_epu8(b1, b2);
  r2 = _mm_min_epu8(bc1,bc2);
  r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);
  _mm_store_si128((__m128i*)&out[0][x], r);
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = _mm_set1_epi8(0xFF);
    a1 = _mm_set1_epi8(0xFF);
    a2 = _mm_set1_epi8(0xFF);
    b0 = _mm_load_si128((__m128i*)&in[y-1][0]);
    b1 = _mm_load_si128((__m128i*)&in[ y ][0]);
    b2 = _mm_load_si128((__m128i*)&in[y+1][0]);
    for(x = 0 ; x < w-16 ; x += 16)
    {
      c0 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c1 = _mm_load_si128((__m128i*)&in[ y ][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
     
      ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
      ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
      ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(c2,b2,1);
     
      r0 = _mm_min_epu8(_mm_min_epu8(ab0, ab1),ab2);
      r1 = _mm_min_epu8(_mm_min_epu8( b0,  b1), b2);
      r2 = _mm_min_epu8(_mm_min_epu8(bc0, bc1),bc2);

      r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);

      _mm_store_si128((__m128i*)&out[y][x], r);

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(vzero,b0,1);//last col
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);
    ab2 = _mm_alignr_epi8(b2,a2,15); bc2 = _mm_alignr_epi8(vzero,b2,1);
    r0 = _mm_min_epu8(ab0, _mm_min_epu8(ab1,ab2));
    r1 = _mm_min_epu8(b0 , _mm_min_epu8(b1, b2));
    r2 = _mm_min_epu8(bc0, _mm_min_epu8(bc1,bc2));
    r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);
    _mm_store_si128((__m128i*)&out[y][x], r);
  }
  /* last row */
  a0 = _mm_set1_epi8(0xFF);
  a1 = _mm_set1_epi8(0xFF);
  b0 = _mm_load_si128((__m128i*)&in[h-2][0]);
  b1 = _mm_load_si128((__m128i*)&in[h-1][0]);
  for(x = 0 ; x < w-16 ; x += 16)
  {
    c0 = _mm_load_si128((__m128i*)&in[h-2][x+16]);
    c1 = _mm_load_si128((__m128i*)&in[h-1][x+16]);
    
    ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(c0,b0,1);
    ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(c1,b1,1);
    
    r0 = _mm_min_epu8(ab0,ab1);
    r1 = _mm_min_epu8(b0, b1);
    r2 = _mm_min_epu8(bc0,bc1);
    r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);

    _mm_store_si128((__m128i*)&out[h-1][x], r);
    
    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1; 
  }
  ab0 = _mm_alignr_epi8(b0,a0,15); bc0 = _mm_alignr_epi8(vzero,b0,1);//last col
  ab1 = _mm_alignr_epi8(b1,a1,15); bc1 = _mm_alignr_epi8(vzero,b1,1);
  r0 = _mm_min_epu8(ab0,ab1);
  r1 = _mm_min_epu8(b0, b1);
  r2 = _mm_min_epu8(bc0, bc1);
  r = _mm_min_epu8(_mm_min_epu8(r0, r1), r2);
  _mm_store_si128((__m128i*)&out[h-1][x], r);
}

#endif

void xk_u8_dilate_3x3_scalar( uint8_t ** in
                            , uint8_t ** out
                            , uint32_t w
                            , uint32_t h)
{ 
  int32_t x,y;
  uint8_t a0, b0, c0;
  uint8_t a1, b1, c1;
  uint8_t a2, b2 ,c2;
  uint8_t r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = 0;            a1 = 0;            a2 = 0;
    b0 = in[y-1][0];   b1 = in[ y ][0];   b2 = in[y+1][0];
    for(x = 0 ; x < w ; x ++)
    {
      c0 = in[y-1][x+1]; 
      c1 = in[ y ][x+1];
      c2 = in[y+1][x+1];
     
      r0 = max(max(a0, a1) , a2);
      r1 = max(max(b0, b1),  b2);
      r2 = max(max(c0, c1) , c2);
      r  = max(max(r0, r1) , r2);

      out[y][x] = r;

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}


void xk_u8_erode_3x3_scalar( uint8_t ** in
                           , uint8_t ** out
                           , uint32_t w
                           , uint32_t h)
{
  int32_t x,y;
  uint8_t a0, b0, c0;
  uint8_t a1, b1, c1;
  uint8_t a2, b2 ,c2;
  uint8_t r0, r1, r2, r;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 0 ; y < h ; y++)
  {
    a0 = 0xFF;            a1 = 0xFF;            a2 = 0xFF;
    b0 = in[y-1][0];   b1 = in[ y ][0];   b2 = in[y+1][0];
    for(x = 0 ; x < w ; x ++)
    {
      c0 = in[y-1][x+1]; 
      c1 = in[ y ][x+1];
      c2 = in[y+1][x+1];
     
      r0 = min(min(a0, a1), a2);
      r1 = min(min(b0, b1), b2);
      r2 = min(min(c0, c1), c2);
      r  = min(min(r0, r1), r2);

      out[y][x] = r;

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
  }
}



void xk_u8_dilate_3x3_scalar_no_padding( uint8_t ** in
                                       , uint8_t ** out
                                       , uint32_t w
                                       , uint32_t h)
{
  int32_t x,y;
  uint8_t a0, b0, c0;
  uint8_t a1, b1, c1;
  uint8_t a2, b2 ,c2;
  uint8_t r0, r1, r2, r;
  
  /* 1st row */
  a1 = 0;            a2 = 0;
  b1 = in[0][0];   b2 = in[1][0];
  for(x = 0 ; x < w-1 ; x ++)
  {
    c1 = in[0][x+1];
    c2 = in[1][x+1];
    
    r0 = max(a1, a2);
    r1 = max(b1, b2);
    r2 = max(c1, c2);
    r  = max(max(r0, r1), r2);

    out[0][x] = r;
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  r0 = max(a1, a2); // last col
  r1 = max(b1, b2);
  r  = max(r0, r1);
  out[0][w-1] = r;
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = 0;            a1 = 0;            a2 = 0;
    b0 = in[y-1][0];   b1 = in[ y ][0];   b2 = in[y+1][0];
    for(x = 0 ; x < w-1 ; x ++)
    {
      c0 = in[y-1][x+1]; 
      c1 = in[ y ][x+1];
      c2 = in[y+1][x+1];
     
      r0 = max(max(a0, a1) , a2);
      r1 = max(max(b0, b1),  b2);
      r2 = max(max(c0, c1) , c2);
      r  = max(max(r0, r1) , r2);

      out[y][x] = r;

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    r0 = max(max(a0, a1) , a2); // last col
    r1 = max(max(b0, b1) , b2);
    r  = max(r0, r1);
    out[y][w-1] = r;
  }
  /* last row */
  a0 = 0;            a1 = 0;         
  b0 = in[h-2][0];   b1 = in[h-1][0];
  for(x = 0 ; x < w-1 ; x ++)
  {
    c0 = in[h-2][x+1]; 
    c1 = in[h-1][x+1];
    
    r0 = max(a0, a1);
    r1 = max(b0, b1);
    r2 = max(c0, c1);
    r  = max(max(r0, r1), r2);

    out[h-1][x] = r;

    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1;
  }
  r0 = max(a0, a1); // last col
  r1 = max(b0, b1);
  r  = max(r0, r1);
  out[h-1][w-1] = r;
}

void xk_u8_erode_3x3_scalar_no_padding( uint8_t ** in
                                      , uint8_t ** out
                                      , uint32_t w
                                      , uint32_t h)
{
  int32_t x,y;
  uint8_t a0, b0, c0;
  uint8_t a1, b1, c1;
  uint8_t a2, b2 ,c2;
  uint8_t r0, r1, r2, r;
  
  /* 1st row */
  a1 = 0xFF;            a2 = 0xFF;
  b1 = in[0][0];   b2 = in[1][0];
  for(x = 0 ; x < w-1 ; x ++)
  {
    c1 = in[0][x+1];
    c2 = in[1][x+1];
    
    r0 = min(a1, a2);
    r1 = min(b1, b2);
    r2 = min(c1, c2);
    r  = min(min(r0, r1), r2);

    out[0][x] = r;
    a1 = b1; b1 = c1;
    a2 = b2; b2 = c2; 
  }
  r0 = min(a1, a2); // last col
  r1 = min(b1, b2);
  r  = min(r0, r1);
  out[0][w-1] = r;
  
  #ifdef MULTI_THREAD // center rows
    #pragma omp parallel for private(x,y,a0,a1,a2,b0,b1,b2,c0,c1,c2,r0,r1,r2,r)
  #endif
  for(y = 1 ; y < h-1 ; y++)
  {
    a0 = 0xFF;         a1 = 0xFF;         a2 = 0xFF;
    b0 = in[y-1][0];   b1 = in[ y ][0];   b2 = in[y+1][0];
    for(x = 0 ; x < w-1 ; x ++)
    {
      c0 = in[y-1][x+1]; 
      c1 = in[ y ][x+1];
      c2 = in[y+1][x+1];
     
      r0 = min(min(a0, a1) , a2);
      r1 = min(min(b0, b1),  b2);
      r2 = min(min(c0, c1) , c2);
      r  = min(min(r0, r1) , r2);

      out[y][x] = r;

      a0 = b0; b0 = c0;
      a1 = b1; b1 = c1;
      a2 = b2; b2 = c2; 
    }
    r0 = min(min(a0, a1) , a2); // last col
    r1 = min(min(b0, b1) , b2);
    r  = min(r0, r1);
    out[y][w-1] = r;
  }
  /* last row */
  a0 = 0xFF;         a1 = 0xFF;         
  b0 = in[h-2][0];   b1 = in[h-1][0];
  for(x = 0 ; x < w-1 ; x ++)
  {
    c0 = in[h-2][x+1]; 
    c1 = in[h-1][x+1];
    
    r0 = min(a0, a1);
    r1 = min(b0, b1);
    r2 = min(c0, c1);
    r  = min(min(r0, r1), r2);

    out[h-1][x] = r;

    a0 = b0; b0 = c0;
    a1 = b1; b1 = c1;
  }
  r0 = min(a0, a1); // last col
  r1 = min(b0, b1);
  r  = min(r0, r1);
  out[h-1][w-1] = r;
}
