#include <xk/common.h>
#include <xk/image/morph.h>


void xk_u8_dilate_5x5_scalar( uint8_t** in
                            , uint8_t** out
                            , uint32_t w
                            , uint32_t h)
{
  int32_t x, y;
  uint8_t r0, r1, r2, r3, r4;
  for(y = 0 ; y < h ; y++)
  {
    r0 = 0; r1 = 0;
    r2  = max(max(max(max(in[y-2][0], in[y-1][0]), in[y+0][0]), in[y+1][0]), in[y+2][0]);
    r3  = max(max(max(max(in[y-2][1], in[y-1][1]), in[y+0][1]), in[y+1][1]), in[y+2][1]);
    for(x = 0 ; x < w ; x++)
    {
      r4  = max(max(max(max(in[y-2][x+2], in[y-1][x+2]), in[y+0][x+2]), in[y+1][x+2]), in[y+2][x+2]);
      out[y][x] = max(max(max(max(r0, r1), r2), r3), r4);
      r0 = r1; r1 = r2; r2 = r3; r3 = r4;
    }
  }
}

void xk_u8_erode_5x5_scalar( uint8_t** in
                           , uint8_t** out
                           , uint32_t w
                           , uint32_t h)
{
  int32_t x, y;
  uint8_t r0, r1, r2, r3, r4;
  for(y = 0 ; y < h ; y++)
  {
    r0 = 255; r1 = 255;
    r2  = min(min(min(min(in[y-2][0], in[y-1][0]), in[y+0][0]), in[y+1][0]), in[y+2][0]);
    r3  = min(min(min(min(in[y-2][1], in[y-1][1]), in[y+0][1]), in[y+1][1]), in[y+2][1]);
    for(x = 0 ; x < w ; x++)
    {
      r4  = min(min(min(min(in[y-2][x+2], in[y-1][x+2]), in[y+0][x+2]), in[y+1][x+2]), in[y+2][x+2]);
      out[y][x] = min(min(min(min(r0, r1), r2), r3), r4);
      r0 = r1; r1 = r2; r2 = r3; r3 = r4;
    }
  }
}

#if defined(__ARM_NEON__) || defined(__aarch64__)
// TODO
void xk_u8_dilate_5x5( uint8_t** in
                     , uint8_t** out
                     , uint32_t w
                     , uint32_t h)
{
  xk_u8_dilate_5x5_scalar(in,out,w,h);
}

// TODO
void xk_u8_erode_5x5( uint8_t** in
                    , uint8_t** out
                    , uint32_t w
                    , uint32_t h)
{
  xk_u8_dilate_5x5_scalar(in,out,w,h);
}

#else

#include <x86intrin.h>

void xk_u8_dilate_5x5( uint8_t** in
                     , uint8_t** out
                     , uint32_t w
                     , uint32_t h)
{
  int32_t x, y;
  __m128i a0, a1, a2;
  __m128i b0, b1, b2;
  __m128i c0, c1, c2;
  __m128i d0, d1, d2;
  __m128i e0, e1, e2;
  __m128i sa_2, sa_1, sa1, sa2;
  __m128i sb_2, sb_1, sb1, sb2;
  __m128i sc_2, sc_1, sc1, sc2;
  __m128i sd_2, sd_1, sd1, sd2;
  __m128i se_2, se_1, se1, se2;
  __m128i r0, r1, r2, r3, r4;
  for(y = 0 ; y < h ; y++)
  {
    a0 = b0 = c0 = d0 = e0 = _mm_set1_epi8(0);
    a1 = _mm_load_si128((__m128i*)&in[y-2][0]);
    b1 = _mm_load_si128((__m128i*)&in[y-1][0]);
    c1 = _mm_load_si128((__m128i*)&in[y+0][0]);
    d1 = _mm_load_si128((__m128i*)&in[y+1][0]);
    e1 = _mm_load_si128((__m128i*)&in[y+2][0]);
    
    for(x = 0 ; x < w ; x+=16)
    {
      a2 = _mm_load_si128((__m128i*)&in[y-2][x+16]);
      b2 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+0][x+16]);
      d2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
      e2 = _mm_load_si128((__m128i*)&in[y+2][x+16]);
      
      sa_2 = _mm_alignr_epi8(a1, a0, 14);
      sb_2 = _mm_alignr_epi8(b1, b0, 14);
      sc_2 = _mm_alignr_epi8(c1, c0, 14);
      sd_2 = _mm_alignr_epi8(d1, d0, 14);
      se_2 = _mm_alignr_epi8(e1, e0, 14);
      
      sa_1 = _mm_alignr_epi8(a1, a0, 15);
      sb_1 = _mm_alignr_epi8(b1, b0, 15);
      sc_1 = _mm_alignr_epi8(c1, c0, 15);
      sd_1 = _mm_alignr_epi8(d1, d0, 15);
      se_1 = _mm_alignr_epi8(e1, e0, 15);
      
      sa1 = _mm_alignr_epi8(a2, a1, 1);
      sb1 = _mm_alignr_epi8(b2, b1, 1);
      sc1 = _mm_alignr_epi8(c2, c1, 1);
      sd1 = _mm_alignr_epi8(d2, d1, 1);
      se1 = _mm_alignr_epi8(e2, e1, 1);
      
      sa2 = _mm_alignr_epi8(a2, a1, 2);
      sb2 = _mm_alignr_epi8(b2, b1, 2);
      sc2 = _mm_alignr_epi8(c2, c1, 2);
      sd2 = _mm_alignr_epi8(d2, d1, 2);
      se2 = _mm_alignr_epi8(e2, e1, 2);
      
      r0 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(sa_2, sa_1), a1), sa1), sa2);
      r1 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(sb_2, sb_1), b1), sb1), sb2);
      r2 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(sc_2, sc_1), c1), sc1), sc2);
      r3 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(sd_2, sd_1), d1), sd1), sd2);
      r4 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(se_2, se_1), e1), se1), se2);
      r0 = _mm_max_epu8(_mm_max_epu8(_mm_max_epu8(_mm_max_epu8(r0, r1), r2), r3), r4);
      
      _mm_store_si128((__m128i*)&out[y][x], r0);
      
      a0 = a1; b0 = b1; c0 = c1; d0 = d1; e0 = e1;
      a1 = a2; b1 = b2; c1 = c2; d1 = d2; e1 = e2;
    }
  }
}

void xk_u8_erode_5x5( uint8_t** in
                    , uint8_t** out
                    , uint32_t w
                    , uint32_t h)
{
  int32_t x, y;
  __m128i a0, a1, a2;
  __m128i b0, b1, b2;
  __m128i c0, c1, c2;
  __m128i d0, d1, d2;
  __m128i e0, e1, e2;
  __m128i sa_2, sa_1, sa1, sa2;
  __m128i sb_2, sb_1, sb1, sb2;
  __m128i sc_2, sc_1, sc1, sc2;
  __m128i sd_2, sd_1, sd1, sd2;
  __m128i se_2, se_1, se1, se2;
  __m128i r0, r1, r2, r3, r4;
  for(y = 0 ; y < h ; y++)
  {
    a0 = b0 = c0 = d0 = e0 = _mm_set1_epi8(-1);
    a1 = _mm_load_si128((__m128i*)&in[y-2][0]);
    b1 = _mm_load_si128((__m128i*)&in[y-1][0]);
    c1 = _mm_load_si128((__m128i*)&in[y+0][0]);
    d1 = _mm_load_si128((__m128i*)&in[y+1][0]);
    e1 = _mm_load_si128((__m128i*)&in[y+2][0]);
    
    for(x = 0 ; x < w ; x+=16)
    {
      a2 = _mm_load_si128((__m128i*)&in[y-2][x+16]);
      b2 = _mm_load_si128((__m128i*)&in[y-1][x+16]);
      c2 = _mm_load_si128((__m128i*)&in[y+0][x+16]);
      d2 = _mm_load_si128((__m128i*)&in[y+1][x+16]);
      e2 = _mm_load_si128((__m128i*)&in[y+2][x+16]);
      
      sa_2 = _mm_alignr_epi8(a1, a0, 14);
      sb_2 = _mm_alignr_epi8(b1, b0, 14);
      sc_2 = _mm_alignr_epi8(c1, c0, 14);
      sd_2 = _mm_alignr_epi8(d1, d0, 14);
      se_2 = _mm_alignr_epi8(e1, e0, 14);
      
      sa_1 = _mm_alignr_epi8(a1, a0, 15);
      sb_1 = _mm_alignr_epi8(b1, b0, 15);
      sc_1 = _mm_alignr_epi8(c1, c0, 15);
      sd_1 = _mm_alignr_epi8(d1, d0, 15);
      se_1 = _mm_alignr_epi8(e1, e0, 15);
      
      sa1 = _mm_alignr_epi8(a2, a1, 1);
      sb1 = _mm_alignr_epi8(b2, b1, 1);
      sc1 = _mm_alignr_epi8(c2, c1, 1);
      sd1 = _mm_alignr_epi8(d2, d1, 1);
      se1 = _mm_alignr_epi8(e2, e1, 1);
      
      sa2 = _mm_alignr_epi8(a2, a1, 2);
      sb2 = _mm_alignr_epi8(b2, b1, 2);
      sc2 = _mm_alignr_epi8(c2, c1, 2);
      sd2 = _mm_alignr_epi8(d2, d1, 2);
      se2 = _mm_alignr_epi8(e2, e1, 2);
      
      r0 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(sa_2, sa_1), a1), sa1), sa2);
      r1 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(sb_2, sb_1), b1), sb1), sb2);
      r2 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(sc_2, sc_1), c1), sc1), sc2);
      r3 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(sd_2, sd_1), d1), sd1), sd2);
      r4 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(se_2, se_1), e1), se1), se2);
      r0 = _mm_min_epu8(_mm_min_epu8(_mm_min_epu8(_mm_min_epu8(r0, r1), r2), r3), r4);
      
      _mm_store_si128((__m128i*)&out[y][x], r0);
   
      a0 = a1; b0 = b1; c0 = c1; d0 = d1; e0 = e1;
      a1 = a2; b1 = b2; c1 = c2; d1 = d2; e1 = e2;
    }
  }
}

#endif

