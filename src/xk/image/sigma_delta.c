#include <xk/common.h>
#include <xk/image/matrix.h>
#include <xk/image/sigma_delta.h>
#include <string.h>
#include <math.h>

void xk_sigma_delta_config_init( struct sigma_delta_config * conf
                               , uint8_t ** in
                               , uint8_t N
                               , uint32_t w
                               , uint32_t h)
{
  conf->w          = w;
  conf->h          = h;
#ifdef __ARM_NEON__
  conf->N          = N;
#else
  conf->N          = log2(N);
#endif
  conf->background = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->difference = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->variance   = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->etq        = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  
  memcpy(conf->background[0], in[0], sizeof(uint8_t) * w * h);
  memset(conf->variance[0], 2, w * h);
  memset(conf->difference[0], 2, w*h);
}


void xk_sigma_delta_config_ext_out_init( struct sigma_delta_config * conf
                                       , uint8_t ** in
                                       , uint8_t ** out
                                       , uint8_t N
                                       , uint32_t w
                                       , uint32_t h)
{
  conf->w          = w;
  conf->h          = h;
#ifdef __ARM_NEON__
  conf->N          = N;
#else
  conf->N          = log2(N);
#endif
  conf->background = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->difference = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->variance   = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->etq        = out;
  
  memcpy(conf->background[0], in[0], sizeof(uint8_t) * w * h);
  memset(conf->variance[0], 2, w * h);
  memset(conf->difference[0], 2, w*h);
}


void xk_sigma_delta_config_minimal_init( struct sigma_delta_config * conf
                                       , uint8_t ** out
                                       , uint8_t N
                                       , uint32_t w
                                       , uint32_t h)
{
  conf->w          = w;
  conf->h          = h;
#ifdef __ARM_NEON__
  conf->N          = N;
#else
  conf->N          = log2(N);
#endif
  conf->background = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->difference = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->variance   = xk_u8_matrix(     0    ,       w-1      ,     0     ,       h-1      );
  conf->etq        = out;
  
  memset(conf->background[0], 0, conf->w * conf->h);
  memset(conf->variance[0], 2, w * h);
  memset(conf->difference[0], 2, w*h);
}

void xk_sigma_delta_init_background( struct sigma_delta_config* conf
                                   , uint8_t** in)
{
  memcpy(conf->background[0], in[0], conf->w * conf->h);
}

void xk_sigma_delta_config_del(struct sigma_delta_config* conf)
{
  xk_u8_matrix_del(conf->background,     0      ,  0 );
  xk_u8_matrix_del(conf->difference,     0      ,  0 );
  xk_u8_matrix_del(conf->variance  ,     0      ,  0 );
  xk_u8_matrix_del(conf->etq       ,     0      ,  0 );
}

void xk_sigma_delta_config_ext_out_del(struct sigma_delta_config* conf)
{
  xk_u8_matrix_del(conf->background,     0      ,  0 );
  xk_u8_matrix_del(conf->difference,     0      ,  0 );
  xk_u8_matrix_del(conf->variance  ,     0      ,  0 );
}

static inline uint8_t subs(uint8_t a, uint8_t b)
{
  uint8_t c = a - b;
  return c > a ? 0x00 : c;
}

static inline uint8_t adds(uint8_t a, uint8_t b)
{
  uint8_t c = a + b;
  return c < a ? 0xFF : c;
}

#if defined(__ARM_NEON__) || defined(__aarch64__)

#include <arm_neon.h>

void xk_sigma_delta( uint8_t** input
                   , struct sigma_delta_config *conf)
{
  uint32_t x, y; 
  uint8x16_t m, i, o, v, e; 
  const uint8x16_t vone = vdupq_n_u8(1);
  const uint8x16_t vtwo = vdupq_n_u8(2);
  uint8x16_t mlt, mgt, vgt, vlt, no;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,m,i,o,v,e,mlt,mgt,vlt,vgt,no, no16l, no16h, no_uzp)
  #endif
  for(y = 0 ; y < conf->h ; y++)
  {
    for(x = 0 ; x < conf->w ; x += 16)
    {
      // load registers
      m = vld1q_u8(&conf->background[y][x]);
      i = vld1q_u8(&input[y][x]);
      o = vld1q_u8(&conf->difference[y][x]);
      v = vld1q_u8(&conf->variance[y][x]);
      e = vld1q_u8(&conf->etq[y][x]);
      // sigma delta process
      //background
      mgt = vcgtq_u8(m,i);
      mlt = vcltq_u8(m,i);                     // if(i > m) m++;     |     | if(i >= m) m++;
      m  = vaddq_u8(m, vandq_u8(mlt, vone));  // else if(i < m) m--;| <=> | if(i <= m) m--;
      m  = vsubq_u8(m, vandq_u8(mgt, vone));  // else m = m;        |     |
      //difference
      o = vmaxq_u8(m, i) - vminq_u8(m, i);     // o = abs(m - i) = max(m, i) - min(m, i)
#if defined(__aarch64__)
      no = vshlq_n_u8(o, conf->N);
#else
      no = vmulq_u8(o, vdupq_n_u8(conf->N));
#endif
      //variance
      vgt = vcgtq_u8(v, no);
      vlt = vcltq_u8(v, no);                   // if(no > v) v++;
      v  = vaddq_u8(v, vandq_u8(vlt, vone));  // else if(no < v) v--;
      v  = vsubq_u8(v, vandq_u8(vgt, vone));  // else v = v;
      v  = vmaxq_u8(v, vtwo);
      //etq process
      e = vcgeq_u8(o,v);
      // store results
      vst1q_u8(&conf->background[y][x], m); 
      vst1q_u8(&conf->difference[y][x], o);
      vst1q_u8(&conf->variance[y][x], v);
      vst1q_u8(&conf->etq[y][x], e);
    }
  }
}

#else

#include <x86intrin.h>

#define xk_mm_srli_epi8(mm, Imm) _mm_and_si128(_mm_set1_epi8(0xFF >> Imm), _mm_srli_epi32(mm, Imm))
#define xk_mm_slli_epi8(mm, Imm) _mm_and_si128(_mm_set1_epi8(0xFF << Imm), _mm_slli_epi32(mm, Imm))

void xk_sigma_delta( uint8_t** input
                , struct sigma_delta_config *conf)
{
  uint32_t x, y;
  __m128i m, i, o, v, e;
  __m128i mge, mle, vge, vle, no;
  const __m128i vzero = _mm_set1_epi8(0x00);
  const __m128i vone  = _mm_set1_epi8(0x01);
  const __m128i vtwo  = _mm_set1_epi8(0x02);
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,m,i,o,v,e,mle,mge,vle,vge,no)
  #endif
  for(y = 0 ; y < conf->h ; y++)
  {
    for(x = 0 ; x < conf->w ; x += 16)
    {
      // load registers
      m = _mm_load_si128((__m128i*)&conf->background[y][x]);
      i = _mm_load_si128((__m128i*)&input[y][x]);
      o = _mm_load_si128((__m128i*)&conf->difference[y][x]);
      v = _mm_load_si128((__m128i*)&conf->variance[y][x]);
      e = _mm_load_si128((__m128i*)&conf->etq[y][x]);
      // sigma delta process
      //background
      mle = _mm_cmpeq_epi8(_mm_subs_epu8(m, i), vzero);
      mge = _mm_cmpeq_epi8(_mm_subs_epu8(i, m), vzero);// if(i > m) m++;     |     | if(i >= m) m++;
      m  = _mm_add_epi8(m, _mm_and_si128(mle, vone)); // else if(i < m) m--;| <=> | if(i <= m) m--;
      m  = _mm_sub_epi8(m, _mm_and_si128(mge, vone)); // else m = m;
      //difference
      o = _mm_max_epu8(m, i) - _mm_min_epu8(m, i); // o = abs(m - i) = max(m, i) - min(m, i)
      no = o;
      no = xk_mm_slli_epi8(o, conf->N);
      //variance
      vle = _mm_cmpeq_epi8(_mm_subs_epu8(v, no), vzero);
      vge = _mm_cmpeq_epi8(_mm_subs_epu8(no, v), vzero);// if(no > v) v++;
      v  = _mm_add_epi8(v, _mm_and_si128(vle, vone)); // else if(no < v) v--;
      v  = _mm_sub_epi8(v, _mm_and_si128(vge, vone)); // else v = v;
      v  = _mm_max_epu8(v, vtwo);
      //etq process
      e = _mm_cmpeq_epi8(_mm_subs_epu8(v, o), vzero); // e = (v <= o ? 255 : 0) <=> ((v - o) <= 0 ? 255 : 0)
      // store results
      _mm_store_si128((__m128i*)&conf->background[y][x], m); 
      _mm_store_si128((__m128i*)&conf->difference[y][x], o);
      _mm_store_si128((__m128i*)&conf->variance[y][x], v);
      _mm_store_si128((__m128i*)&conf->etq[y][x], e);
    }
  }
}

#endif

void xk_sigma_delta_scalar( uint8_t** input
                          , struct sigma_delta_config *conf)
{
  uint32_t x, y;
  uint8_t m, i, o, v, no;
  #ifdef MULTI_THREAD
    #pragma omp parallel for private(x,y,m,i,o,v,no)
  #endif
  for(y = 0 ; y < conf->h ; y++)
  {
    for(x = 0 ; x < conf->w ; x++)
    {
      m = conf->background[y][x];
      i = input[y][x];
      o = conf->difference[y][x];
      v = conf->variance[y][x];
      if(m < i)
        m++;
      else if(m > i)
        m--;
      o  = max(m, i) - min(m, i);
//       no = min(o * conf->N, 0xFF);
      no = o << conf->N;
      if(v < no)
        v++;
      else if(v > no)
        v--;
      v = max(v, 2);
      conf->etq[y][x] = ((v <= o) ? 255 : 0);
      conf->background[y][x] = m;
      conf->difference[y][x] = o;
      conf->variance[y][x]   = v;
    }
  }
}
