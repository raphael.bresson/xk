#include <xk/network/common.h>
#include <fcntl.h>

void xk_get_local_address(int sock_desc, char* address)
{
  struct sockaddr_in local_addr;
  memset(&local_addr, 0, sizeof(struct sockaddr_in));
  uint32_t addr_len = sizeof(struct sockaddr_in);
  int ret = getsockname(sock_desc, (struct sockaddr*) &local_addr, (socklen_t* ) &addr_len);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Local address fetching failed: Cannot resolve socketname: %s\n", strerror(errno));
  strcpy(address, inet_ntoa(local_addr.sin_addr));
}

uint16_t xk_get_local_port(int sock_desc)
{
  struct sockaddr_in local_addr;
  memset(&local_addr, 0, sizeof(struct sockaddr_in));
  uint32_t addr_len = sizeof(struct sockaddr_in);
  int ret = getsockname(sock_desc, (struct sockaddr*) &local_addr, (socklen_t* ) &addr_len);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Local port fetching failed: Cannot resolve socket name: %s\n", strerror(errno));
  return ntohs(local_addr.sin_port);
}

void xk_set_local_port(int sock_desc, uint16_t localport)
{
  struct sockaddr_in local_addr;
  memset(&local_addr, 0, sizeof(struct sockaddr_in));
  local_addr.sin_family = AF_INET;
  local_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  local_addr.sin_port = htons(localport);
  int ret = bind(sock_desc, (struct sockaddr*) &local_addr, sizeof(struct sockaddr_in));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Local port settings failed: Cannot bind socket to port %d: %s\n", localport, strerror(errno));
}

void xk_set_local_address_and_port(int sock_desc, const char* addr , uint16_t localport)
{
  struct sockaddr_in local_addr;
  memset(&local_addr, 0, sizeof(struct sockaddr_in));
  struct hostent *host;
  host = gethostbyname(addr);
  XK_ERROR( host != NULL 
          , "XK_NETWORK_ERROR: Local settings failed: Cannot resolve host with address %s: %s\n", addr, strerror(errno));
  local_addr.sin_family = AF_INET;
  local_addr.sin_addr.s_addr = *(uint64_t*) host->h_addr_list[0];
  local_addr.sin_port = htons(localport);
  int ret = bind(sock_desc, (struct sockaddr*) &local_addr, sizeof(struct sockaddr_in));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Local settings failed: Cannot bind socket to address %s and port %d: %s\n", addr, localport, strerror(errno));
}


void xk_get_peer_address(int sock_desc, char* addr)
{
  struct sockaddr_in peer_addr;
  memset(&peer_addr, 0, sizeof(struct sockaddr_in));
  uint32_t addr_len = sizeof(struct sockaddr_in);
  int ret = getpeername(sock_desc, (struct sockaddr*) &peer_addr, (socklen_t*) &addr_len);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Cannot get peer address: Cannot resolve peer name: %s\n", strerror(errno));
  strcpy(addr, inet_ntoa(peer_addr.sin_addr));
}

uint16_t xk_get_peer_port(int sock_desc)
{
  struct sockaddr_in peer_addr;
  memset(&peer_addr, 0, sizeof(struct sockaddr_in));
  uint32_t addr_len = sizeof(struct sockaddr_in);
  int ret = getpeername(sock_desc, (struct sockaddr*) &peer_addr, (socklen_t*) &addr_len);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Cannot get peer port: Cannot resolve peer name: %s\n", strerror(errno));
  return ntohs(peer_addr.sin_port);
}


int xk_socket(int type, int protocol)
{
  int sock = socket(AF_INET, type, protocol);
  int flags = fcntl(sock, F_GETFL);
  fcntl(sock, F_SETFL, flags);
  XK_ERROR( sock >= 0
          , "XK_NETWORK_ERROR: Cannot get socket: %s\n", strerror(errno));
  return sock;
}

void xk_connect_socket( int sock_desc
                      , const char* peer_addr
                      , uint16_t peer_port)
{
  struct sockaddr_in addr;
  struct hostent *host;
  host = gethostbyname(peer_addr);
  XK_ERROR( host != NULL 
          , "XK_NETWORK_ERROR: Cannot connect socket: Cannot resolve host with address %s: %s\n", peer_addr, strerror(errno));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = *(uint64_t*) host->h_addr_list[0];
  addr.sin_port = htons(peer_port);
  int ret = connect(sock_desc, (struct sockaddr*) &addr, sizeof(struct sockaddr_in));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Cannot connect socket(peer_addr=%s, peer_port=%d): %s\n"
          , peer_addr, peer_port, strerror(errno));
}

void xk_socket_send(int sock_desc, const void* buffer, uint32_t len)
{
  int ret = send(sock_desc, buffer, len, 0);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Cannot send message: %s\n", strerror(errno));
}

size_t xk_socket_recv(int sock_desc, void* buffer, uint32_t len)
{
  int ret = recv(sock_desc, buffer, len, 0);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: Cannot send message: %s\n", strerror(errno));
  return (size_t)ret;
}
