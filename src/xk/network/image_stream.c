#include <xk/network/image_stream.h>
#include <xk/image/matrix.h>
struct stream_image_list_t* _stream_list()
{
  struct stream_image_list_t* list = (struct stream_image_list_t*)malloc(sizeof(struct stream_image_list_t));
  XK_ERROR(list != NULL, "XK_NETWORK_ERROR: Image stream: Cannot allocate memory for list\n");
  list->semaphore = xk_semaphore(0);
  list->head = NULL;
  list->tail = NULL;
  return list;
}

void _stream_list_append(struct stream_image_list_t* list, uint8_t** image)
{
  struct stream_image_list_node_t* node = (struct stream_image_list_node_t*)malloc(sizeof(struct stream_image_list_node_t));
  XK_ERROR(node != NULL, "XK_NETWORK_ERROR: Image stream: Cannot allocate memory for list elem\n");
  node->image = image;
  node->next = NULL;
  if(list->tail)
    list->tail->next = node;
  else
    list->head = node;
  list->tail = node;
  xk_semaphore_unlock(list->semaphore);
}

uint8_t** _stream_list_pop(struct stream_image_list_t* list)
{
  xk_semaphore_lock(list->semaphore);
  uint8_t** image = NULL;
  if(list->head)
  {
    struct stream_image_list_node_t* node = list->head;
    image = node->image;
    if(node->next)
      list->head = node->next;
    else
    {
      list->head = NULL;
      list->tail = NULL;
    }
    free(node);
  }
  return image;
}

void _stream_list_node_del(struct stream_image_list_node_t* node)
{
  if(node->next)
    _stream_list_node_del(node->next);
  free(node->image);
  free(node);
}

void _stream_list_del(struct stream_image_list_t* list)
{
  if(list->head)
    _stream_list_node_del(list->head);
  xk_semaphore_del(list->semaphore);
  free(list);
}

void* _input_callback(void * args)
{
  xk_image_stream_t* stream = (xk_image_stream_t*)args;
  uint8_t** buffer = NULL;
  while(stream->running)
  {
    buffer = xk_u8_matrix(0, stream->w-1, 0, stream->h);
    xk_udp_recv_u8_image(stream->sockfd, buffer, stream->w, stream->h, stream->peer_addr, &stream->peer_port);
    _stream_list_append(stream->ims, buffer);
  }
  return NULL;
}

void* _output_callback(void * args)
{
  xk_image_stream_t* stream = (xk_image_stream_t*)args;
  while(stream->running)
  {
    uint8_t** buffer = _stream_list_pop(stream->ims);
    if(buffer)
    {
      xk_udp_send_u8_image(stream->sockfd, buffer, stream->w, stream->h, stream->peer_addr, stream->peer_port);
      xk_u8_matrix_del(buffer,0,0);
    }
    usleep(20);
  }
  return NULL;
}

xk_image_stream_t* xk_image_stream( uint32_t w
                                  , uint32_t h
                                  , const char* local_addr
                                  , uint16_t local_port
                                  , const char* peer_addr
                                  , uint16_t peer_port)
{
  xk_image_stream_t* stream = (xk_image_stream_t*)malloc(sizeof(xk_image_stream_t));
  XK_ERROR(stream != NULL, "XK_NETWORK_ERROR: Image stream: Cannot allocate memory for stream\n");
  stream->sockfd = xk_udp_socket();
  xk_set_local_port(stream->sockfd, local_port);
  stream->ims = _stream_list();
  stream->w = w; 
  stream->h = h;
  strcpy(stream->local_addr, local_addr);
  strcpy(stream->peer_addr, peer_addr);
  stream->local_port = local_port;
  stream->peer_port = peer_port;
  stream->running = 1;
  return stream;
}

xk_image_stream_t* xk_input_image_stream( uint32_t w
                                        , uint32_t h
                                        , const char* local_addr
                                        , uint16_t local_port
                                        , const char* peer_addr
                                        , uint16_t peer_port)
{
  xk_image_stream_t* stream = xk_image_stream(w,h,local_addr,local_port,peer_addr,peer_port);
  int ret = pthread_create(&stream->network_thread, NULL, _input_callback, stream);
  XK_ERROR(ret >= 0, "XK_NETWORK_ERROR: Cannot create stream thread: %s\n", strerror(errno));
  return stream;
}

xk_image_stream_t* xk_output_image_stream( uint32_t w
                                         , uint32_t h
                                         , const char* local_addr
                                         , uint16_t local_port
                                         , const char* peer_addr
                                         , uint16_t peer_port)
{
  xk_image_stream_t* stream = xk_image_stream(w,h,local_addr,local_port,peer_addr,peer_port);
  int ret = pthread_create(&stream->network_thread, NULL, _output_callback, stream);
  XK_ERROR(ret >= 0, "XK_NETWORK_ERROR: Cannot create stream thread: %s\n", strerror(errno));
  return stream;
}

uint8_t** xk_input_image_stream_next_frame(xk_image_stream_t* stream)
{
  uint8_t** m = _stream_list_pop(stream->ims);
  return m;
}

void xk_output_image_stream_next_frame(xk_image_stream_t* stream, uint8_t** allocated_image)
{
  _stream_list_append(stream->ims, allocated_image);
}

void xk_image_stream_del(xk_image_stream_t* stream)
{
  stream->running = 0;
  usleep(100000);
  xk_semaphore_unlock(stream->ims->semaphore);
  pthread_cancel(stream->network_thread);
  pthread_join(stream->network_thread, NULL);
  _stream_list_del(stream->ims);
  close(stream->sockfd);
  free(stream);
}
