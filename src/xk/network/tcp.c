#include <xk/network/tcp.h>

int xk_tcp_socket()
{
  return xk_socket(SOCK_STREAM, IPPROTO_TCP);
}

int xk_tcp_accept(int sock_desc)
{
  int peer_desc = accept(sock_desc, NULL, 0);
  XK_ERROR( peer_desc >= 0
          , "XK_NETWORK_ERROR: tcp accept failed: %s\n", strerror(errno));
  return peer_desc;
}

void xk_tcp_listen(int sock_desc, uint32_t nb_clients_max)
{
  int ret = listen(sock_desc, nb_clients_max);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: tcp listen failed: %s\n", strerror(errno));
}
