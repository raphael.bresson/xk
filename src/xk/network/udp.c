#define _XOPEN_SOURCE
#include <xk/network/udp.h>
#include <netinet/ip.h>
#include <pthread.h>

static pthread_mutex_t udp_send_mutex= PTHREAD_MUTEX_INITIALIZER;

struct ip_mreq {
    struct in_addr imr_multiaddr; /* IP multicast address of group */
    struct in_addr imr_interface; /* local IP address of interface */
};


int xk_udp_socket()
{
  int sock = xk_socket(SOCK_DGRAM, IPPROTO_UDP);
  xk_udp_broadcast(sock);
  return sock;
}

void xk_udp_broadcast(int sock_desc)
{
  int perm = 1;
  int ret = setsockopt(sock_desc, SOL_SOCKET, SO_BROADCAST, (void*) &perm, sizeof(int));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP cannot set broadcast: %s\n"
          , strerror(errno));
}

void xk_udp_sendto( int sock_desc
                  , const void* buffer
                  , uint32_t len
                  , const char* peer_address
                  , uint16_t peer_port)
{
  struct sockaddr_in addr;
  pthread_mutex_lock(&udp_send_mutex);
  struct hostent *host;
  host = gethostbyname(peer_address);
  XK_ERROR( host != NULL 
          , "XK_NETWORK_ERROR: Cannot connect socket: Cannot resolve host with address %s: %s\n"
          , peer_address
          , strerror(errno));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = *(uint64_t*) host->h_addr_list[0];
  addr.sin_port = htons(peer_port);
  pthread_mutex_unlock(&udp_send_mutex);
  int ret = sendto(sock_desc, buffer, len, 0, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP sendto failed %s\n"
          , strerror(errno));
}

size_t xk_udp_recvfrom( int sock_desc
                      , void* buffer
                      , uint32_t len
                      , char* peer_address
                      , uint16_t* peer_port)
{
  struct sockaddr_in addr;
  socklen_t addr_len = sizeof(struct sockaddr_in);
  int ret = recvfrom(sock_desc, buffer, len, 0, (struct sockaddr*)&addr, (socklen_t*) &addr_len);
  XK_ERROR( ret >= 0
          , "XK_ERROR_NETWORK: UDP recvfrom failed: %s\n"
          , strerror(errno));
  strcpy(peer_address, inet_ntoa(addr.sin_addr));
  *peer_port = ntohs(addr.sin_port);
  return ret;
}

void xk_udp_set_multicast_ttl(int sock_desc, uint8_t multicastTTL)
{
  int ret = setsockopt(sock_desc, IPPROTO_IP, IP_MULTICAST_TTL, (void*)&multicastTTL, sizeof(uint8_t));
  XK_ERROR( ret >= 0
          , "XK_ERROR_NETWORK: UDP set multicast TTL failed: %s\n"
          , strerror(errno));
}

void xk_udp_join_group(int sock_desc, const char* group)
{
  struct ip_mreq request;
  request.imr_multiaddr.s_addr = inet_addr(group);
  request.imr_multiaddr.s_addr = htonl(INADDR_ANY);
  int ret = setsockopt( sock_desc
                      , IPPROTO_IP
                      , IP_ADD_MEMBERSHIP
                      , (void*) &request
                      , sizeof(struct ip_mreq));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP: Cannot join multicast group %s\n"
          , strerror(errno));
}

void xk_udp_leave_group(int sock_desc, const char* group)
{
  struct ip_mreq request;
  request.imr_multiaddr.s_addr = inet_addr(group);
  request.imr_multiaddr.s_addr = htonl(INADDR_ANY);
  int ret = setsockopt( sock_desc
                      , IPPROTO_IP
                      , IP_DROP_MEMBERSHIP
                      , (void*) &request
                      , sizeof(struct ip_mreq));
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP: Cannot leave multicast group %s\n"
          , strerror(errno));
}

void xk_udp_send_iov( int sock_desc
                    , struct iovec *iov
                    , const char* peer_address
                    , uint16_t peer_port)
{
  struct sockaddr_in addr;
  struct hostent *host;
  struct msghdr msg;
  host = gethostbyname(peer_address);
  XK_ERROR( host != NULL 
          , "XK_NETWORK_ERROR: Cannot connect socket: Cannot resolve host with address %s: %s\n"
          , peer_address
          , strerror(errno));
  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = *(uint64_t*) host->h_addr_list[0];
  addr.sin_port        = htons(peer_port);
  msg.msg_name         = &addr;
  msg.msg_namelen      = sizeof(struct sockaddr_in);
  msg.msg_iov          = iov;
  msg.msg_control      = NULL;
  msg.msg_controllen   = 0;
  msg.msg_flags        = 0;
  int ret = sendmsg(sock_desc, &msg, 0);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP sendmsg failed %s\n"
          , strerror(errno));
}

void xk_udp_recv_iov( int sock_desc
                    , struct iovec *iov
                    , const char* peer_address
                    , uint16_t peer_port)
{
  struct msghdr msg;
  struct sockaddr_in addr;
  struct hostent *host;
  host = gethostbyname(peer_address);
  XK_ERROR( host != NULL 
          , "XK_NETWORK_ERROR: Cannot connect socket: Cannot resolve host with address %s: %s\n"
          , peer_address
          , strerror(errno));
  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = *(uint64_t*) host->h_addr_list[0];
  addr.sin_port        = htons(peer_port);
  msg.msg_name         = &addr;
  msg.msg_namelen      = sizeof(struct sockaddr_in);
  msg.msg_iov          = iov;
  msg.msg_control      = NULL;
  msg.msg_controllen  = 0;
  msg.msg_flags            = 0;
  int ret = sendmsg(sock_desc, &msg, 0);
  XK_ERROR( ret >= 0
          , "XK_NETWORK_ERROR: UDP recvmsg failed %s\n"
          , strerror(errno));
}
