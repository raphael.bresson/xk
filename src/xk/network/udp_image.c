#include <xk/network/udp_image.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

void xk_udp_send_u8_image(int sockfd, uint8_t* const* image, uint32_t w, uint32_t h, const char* addr, uint16_t port)
{
  uint32_t nb_chunks = ((w*h) / XK_UDP_IMAGE_CHUNK) + (((w*h) % XK_UDP_IMAGE_CHUNK) > 0 ? 1 : 0);
  char buffer[XK_UDP_IMAGE_CHUNK + sizeof(uint32_t)];
  for(uint32_t i = 0 ; i < nb_chunks - 1 ; i++)
  {
    memcpy(&buffer[4], &image[0][i * XK_UDP_IMAGE_CHUNK], XK_UDP_IMAGE_CHUNK);
    int* pid = (int*)&buffer[0];
    *pid = i;
    xk_udp_sendto(sockfd, buffer, sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
  }
  uint32_t last_chunk_size = (w*h) % XK_UDP_IMAGE_CHUNK;
  if(last_chunk_size != 0)
  {
    memcpy(&buffer[4], &image[0][(nb_chunks - 1) * XK_UDP_IMAGE_CHUNK], last_chunk_size);
    int* pid = (int*)&buffer[0];
    *pid = nb_chunks - 1;
    xk_udp_sendto(sockfd, &image[0][(nb_chunks - 1) * XK_UDP_IMAGE_CHUNK], sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
  }
  uint32_t end_signal = 0xFFFFFFFF;
  xk_udp_sendto(sockfd, &end_signal, sizeof(uint32_t), addr, port);
}

void xk_udp_recv_u8_image(int sockfd, uint8_t** image, uint32_t w, uint32_t h, char* addr, uint16_t* port)
{
  uint32_t nb_chunks = (w*h / XK_UDP_IMAGE_CHUNK) + ((w*h % XK_UDP_IMAGE_CHUNK) > 0 ? 1 : 0);
  char buffer[XK_UDP_IMAGE_CHUNK + sizeof(uint32_t)];
  uint32_t chunck_id = 0, chunck_size = 0;
  uint32_t last_chunk_size = (w*h)%XK_UDP_IMAGE_CHUNK;
  int ret;
  fd_set fds;
  struct timeval tv;
  while(chunck_id != 0xFFFFFFFF)
  {
    FD_ZERO(&fds);
    FD_SET(sockfd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 20000;
    ret = select(sockfd + 1, &fds, NULL, NULL, &tv);
    if(EINTR == errno) continue;
    XK_ERROR(ret >= 0, "XK_NETWORK_ERROR: select() : %s\n", strerror(errno));
    if(ret == 0)
    {
//       XK_WARNING(0, "XK_NETWORK_WARNING: select timeout elapsed \n");
      continue;
    }
    xk_udp_recvfrom(sockfd, buffer, sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
    int* pid = (int*)&buffer[0];
    chunck_id = *pid;
    if(chunck_id == 0xFFFFFFFF)
      break;
    chunck_size = (chunck_id == (nb_chunks-1) ? last_chunk_size : XK_UDP_IMAGE_CHUNK);
    memcpy(&image[0][chunck_id * XK_UDP_IMAGE_CHUNK], &buffer[4], chunck_size); 
  }
}

void xk_udp_send_data(int sockfd, const void * data, uint32_t size, const char* addr, uint16_t port)
{
  uint32_t nb_chunks = (size / XK_UDP_IMAGE_CHUNK) + ((size % XK_UDP_IMAGE_CHUNK) > 0 ? 1 : 0);
  char buffer[XK_UDP_IMAGE_CHUNK + sizeof(uint32_t)];
  for(uint32_t i = 0 ; i < nb_chunks - 1 ; i++)
  {
    memcpy(&buffer[4], &((char*)data)[i * XK_UDP_IMAGE_CHUNK], XK_UDP_IMAGE_CHUNK);
    int* pid = (int*)&buffer[0];
    *pid = i;
    xk_udp_sendto(sockfd, buffer, sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
  }
  uint32_t last_chunk_size = (size) % XK_UDP_IMAGE_CHUNK;
  if(last_chunk_size != 0)
  {
    memcpy(&buffer[4], &((char*)data)[(nb_chunks - 1) * XK_UDP_IMAGE_CHUNK], last_chunk_size);
    int* pid = (int*)&buffer[0];
    *pid = nb_chunks - 1;
    xk_udp_sendto(sockfd, &((char*)data)[(nb_chunks - 1) * XK_UDP_IMAGE_CHUNK], sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
  }
  uint32_t end_signal = 0xFFFFFFFF;
  xk_udp_sendto(sockfd, &end_signal, sizeof(uint32_t), addr, port);
}

void xk_udp_recv_data(int sockfd, void* data, uint32_t size, char* addr, uint16_t* port)
{
  uint32_t nb_chunks = (size / XK_UDP_IMAGE_CHUNK) + ((size % XK_UDP_IMAGE_CHUNK) > 0 ? 1 : 0);
  char buffer[XK_UDP_IMAGE_CHUNK + sizeof(uint32_t)];
  uint32_t chunck_id = 0, chunck_size = 0;
  uint32_t last_chunk_size = (size)%XK_UDP_IMAGE_CHUNK;
  while(chunck_id != 0xFFFFFFFF)
  {
    xk_udp_recvfrom(sockfd, buffer, sizeof(uint32_t) + XK_UDP_IMAGE_CHUNK, addr, port);
    int* pid = (int*)&buffer[0];
    chunck_id = *pid;
    if(chunck_id == 0xFFFFFFFF)
      break;
    chunck_size = (chunck_id == (nb_chunks-1) ? last_chunk_size : XK_UDP_IMAGE_CHUNK);
    memcpy(&((char*)data)[chunck_id * XK_UDP_IMAGE_CHUNK], &buffer[4], chunck_size); 
  }
}
