#include <xk/stats/mean.h>
#include <math.h>
double xk_mean_f64(double* data, uint32_t len)
{
  double sum = 0;
  for(uint32_t i = 0; i < len ; i++)
  {
    sum += data[i];
  }
  return sum / len;
}
