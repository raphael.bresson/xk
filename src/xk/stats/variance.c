#include <xk/stats/variance.h>
#include <math.h>
double xk_variance_f64(double* data, uint32_t len)
{
  double sum = 0, sum_square = 0, mean = 0;
  for(uint32_t i = 0; i < len ; i++)
  {
    sum += data[i];
    sum_square = fma(data[i], data[i], sum_square);
  }
  mean = sum / len;
  return -fma(mean, sum, -sum_square); //sum_square - ((sum * sum) / len); 
}
