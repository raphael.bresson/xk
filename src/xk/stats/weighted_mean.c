#include <xk/stats/weighted_mean.h>
#include <math.h>
double xk_weighted_mean_f64(double* data, double* weights, uint32_t len)
{
  double sum = 0, total_weights = 0;
  for(uint32_t i = 0; i < len ; i++)
  {
    sum = fma(data[i], weights[i], sum);
    total_weights += weights[i];
  }
  return sum / total_weights; 
}
