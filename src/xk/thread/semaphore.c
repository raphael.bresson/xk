#include <xk/thread/semaphore.h>
#include <xk/common.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

sem_t* xk_semaphore(uint32_t initval)
{
  sem_t* semAddr = (sem_t*)malloc(sizeof(sem_t));;
  XK_ERROR( semAddr != NULL
          , "XK_THREAD_ERROR: Cannot allocate semaphore\n");
  int ret = sem_init(semAddr, 0, initval);
  XK_ERROR( ret >= 0
          , "XK_THREAD_ERROR: Cannot initialize semaphore: %s\n", strerror(errno));
  return semAddr;
}

void xk_semaphore_lock(sem_t* sem)
{
  int ret = sem_wait(sem);
  XK_ERROR( (ret >= 0)
          , "XK_THREAD_ERROR: Cannot lock semaphore: %s\n", strerror(errno));
}

void xk_semaphore_unlock(sem_t* sem)
{
  int ret = sem_post(sem);
  XK_ERROR( ret >= 0
          , "XK_THREAD_ERROR: Cannot unlock semaphore: %s\n", strerror(errno));
}

void xk_semaphore_del(sem_t* sem)
{
  int ret = sem_destroy(sem);
  XK_ERROR( ret >= 0
          , "XK_THREAD_ERROR: Cannot destroy semaphore: %s\n", strerror(errno));
  free(sem);
}

int xk_semaphore_get_value(sem_t* sem)
{
  int res = 0;
  int ret = sem_getvalue(sem, &res);
  XK_ERROR( ret >= 0
          , "XK_THREAD_ERROR: Cannot get value of semaphore: %s\n", strerror(errno));
  return res;
}

