#include <xk/thread/signal.h>
#include <unistd.h>

typedef struct xk_signal_args_queue_node_t
{
  void* args;
  struct xk_signal_args_queue_node_t* prev;
  struct xk_signal_args_queue_node_t* next;
}xk_signal_args_queue_node_t;

typedef struct xk_signal_args_queue_t
{
  struct xk_signal_args_queue_node_t* head;
  struct xk_signal_args_queue_node_t* tail;
} xk_signal_args_queue_t;

xk_signal_args_queue_t* xk_signal_args_queue()
{
  xk_signal_args_queue_t* queue = (xk_signal_args_queue_t*)malloc(sizeof(xk_signal_args_queue_t));
  XK_ERROR(queue, "XK_THREAD_ERROR: Cannot allocate signal args queue\n");
  queue->head = NULL;
  queue->tail = NULL;
  return queue;
}

void xk_signal_args_queue_push(xk_signal_args_queue_t* queue, void* allocated_data)
{
  if(allocated_data)
  {
    xk_signal_args_queue_node_t* node = (xk_signal_args_queue_node_t*)malloc(sizeof(xk_signal_args_queue_node_t));
    XK_ERROR(queue, "XK_THREAD_ERROR: Cannot allocate node\n");
    node->next = NULL;
    node->prev = NULL;
    node->args = allocated_data;
    if(queue->head)
    {
      queue->tail->next = node;
      queue->tail = node;
    }else
      queue->tail = queue->head = node;
  }
}

void* xk_signal_args_queue_pop(xk_signal_args_queue_t* queue)
{
  if(queue->head)
  {
    xk_signal_args_queue_node_t* node = queue->tail;
    if(queue->head == queue->tail)
    {
      queue->tail = NULL;
      queue->head = NULL;
    }else
    {
      queue->tail = node->prev;
      queue->tail->next = NULL;
    }
    void* args = node->args;
    free(node);
    return args;
  }
  return NULL;
}

void xk_signal_args_queue_node_del(xk_signal_args_queue_node_t* node)
{
  if(node)
  {
    if(node->next)
      xk_signal_args_queue_node_del(node);
    free(node);
  }
}

void xk_signal_args_queue_del(xk_signal_args_queue_t* queue)
{
  xk_signal_args_queue_node_del(queue->head);
}

void* signal_callback(void* data)
{
  xk_signal_t* signal = (xk_signal_t*)data;
  while(signal->running)
  {
    xk_semaphore_lock(signal->semaphore);
//     signal->callback(signal->args);
    signal->callback(xk_signal_args_queue_pop(signal->args));
  }
  return NULL;
}

xk_signal_t* xk_signal(void (*callback)(void*))
{
  xk_signal_t* signal = (xk_signal_t*)malloc(sizeof(xk_signal_t));
  XK_ERROR(signal, "XK_THREAD_ERROR: Cannot allocate signal\n");
  signal->callback = callback;
  signal->semaphore = xk_semaphore(0);
  signal->args = xk_signal_args_queue();
  signal->running = 1;
  pthread_create(&signal->thread, NULL, signal_callback, signal);
  return signal;
}

void xk_signal_notify(xk_signal_t* signal, void* args)
{
//   if(signal->args)
//     free(signal->args);
//   signal->args = args;
  if(args)
    xk_signal_args_queue_push(signal->args, args);
  xk_semaphore_unlock(signal->semaphore);
}

void xk_signal_del(xk_signal_t* signal)
{
  signal->running = 0;
  xk_semaphore_unlock(signal->semaphore);
  usleep(20000);
  pthread_join(signal->thread, NULL);
  xk_semaphore_del(signal->semaphore);
  xk_signal_args_queue_del(signal->args);
//   if(signal->args)
//     free(signal->args);
  free(signal);
}
