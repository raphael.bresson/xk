#include <xk/container/list.h>

struct st_test
{
  char str[64];
  struct xk_list_node node;
};

void print_elem(void * data, void* userdata)
{
  struct st_test *test = data;
  printf("%s\n", test->str);
}

int main()
{
  struct st_test obj0,obj1,obj2,*it;
  struct xk_list list;
  struct xk_list_node *n;
  strcpy(obj0.str, "toto");
  strcpy(obj1.str, "est");
  strcpy(obj2.str, "cucu");
  xk_list_init(&list);
  xk_list_node_init(&obj0.node, &obj0);
  xk_list_node_init(&obj1.node, &obj1);
  xk_list_node_init(&obj2.node, &obj2);
  xk_list_append(&list,&obj0.node);
  xk_list_append(&list,&obj1.node);
  xk_list_append(&list,&obj2.node);
  xk_list_foreach(&list, it, n)
  {
    printf("%s\n",it->str);
  }
  printf("\n");
  xk_list_rforeach(&list, it, n)
  {
    printf("%s\n",it->str);
  }
  printf("\n");
  xk_list_remove(&list, &obj1.node);
  xk_list_foreach(&list, it, n)
  {
    printf("%s\n",it->str);
  }
  printf("\n");
  xk_list_rforeach(&list, it, n)
  {
    printf("%s\n",it->str);
  }
  printf("\n");
  xk_list_rec(&list, print_elem, NULL); 
  printf("\n");
  xk_list_rrec(&list, print_elem, NULL); 
  printf("\n");
  return 0;
}
