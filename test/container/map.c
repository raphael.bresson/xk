#include <xk/container/map.h>
#include <xk/common.h>
int main()
{
  int a=1, b=2, c=3, *d=NULL;
  d = (int*)malloc(sizeof(int));
  *d = 4;
  xk_map_t* m = xk_map();
  xk_map_put(m, "a", &a);
  xk_map_put(m, "b", &b);
  xk_map_put(m, "c", &c);
  xk_map_put(m, "d", d);
  
  free(d);
  printf("%d, %d, %d, %d\n", *((int*)xk_map_get(m, "a"))
                           , *((int*)xk_map_get(m, "b"))
                           , *((int*)xk_map_get(m, "c"))
                           , *((int*)xk_map_get(m, "d")));
  
  xk_map_del(m);
  return 0;
}
