#include <xk/container/quad_tree.h>

int main()
{
  struct xk_quad_tree tree;
  xk_quad_tree_init(&tree, 4, 128, 128);
  xk_quad_tree_print(&tree, NULL, stdout);
  return 0;
}
