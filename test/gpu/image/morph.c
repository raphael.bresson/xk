#include <xk/gpu/image/morph.h>
#include <xk/gpu/common.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xk/gpu/image/run.h>

int main()
{
  uint32_t w, h;
  uint8_t** in  = xk_load_pgm("../data_test/testin0.pgm",0,0, &w, &h);
  printf("dimensions: w: %d, h: %d\n", w, h);
  uint8_t** out = xk_u8_matrix(0, w, 0, h);
  
  xk_gpu_env_t* env = xk_gpu_env_default();
  xk_gpu_task_t* task = xk_gpu_dilate_task(env, in, out, w, h, 3, 3);
  
  cl_command_queue queue = xk_gpu_command_queue(env->context, env->device);
  for(int i = 0 ; i < 100 ; i++)
    xk_gpu_image_run(task, queue, in, out);
//   xk_write_pgm("toto.pgm", out, w, h);
//   xk_write_pgm("titi.pgm", in, w, h);
  
  xk_gpu_command_queue_del(queue);
  xk_gpu_task_del(task);
  xk_gpu_env_del(env);
  xk_u8_matrix_del(in,0,0);
  xk_u8_matrix_del(out,0,0);
  return 0;
}
