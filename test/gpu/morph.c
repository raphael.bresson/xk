#include <xk/gpu/image/kernels/erode.h>
#include <xk/gpu/common.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>

int main()
{
  uint32_t w, h;
  uint8_t** in  = xk_load_pgm("../data_test/testin0.pgm",0,0, &w, &h);
  printf("dimensions: w: %d, h: %d\n", w, h);
  uint8_t** out = xk_u8_matrix(0, w, 0, h);
  uint32_t nb_platforms = 0, nb_devices = 0; 
  cl_platform_id* platforms = xk_gpu_platforms(&nb_platforms);
  cl_device_id*   devices   = xk_gpu_devices(platforms[0], &nb_devices);
  cl_context      context   = xk_gpu_context(platforms[0], devices, nb_devices);
  
  cl_program program = xk_gpu_program(context, devices, nb_devices, xk_gpu_erode_src, "-DMORPH_WIDTH=3 -DMORPH_HEIGHT=3"); 
  cl_kernel kernel   = xk_gpu_kernel(program, "erode_gray");
  cl_mem input  = xk_gpu_buffer(context, in[0], w*h, CL_MEM_READ_WRITE);
  cl_mem output = xk_gpu_buffer(context, out[0], w*h, CL_MEM_READ_WRITE);
  
  cl_command_queue queue = xk_gpu_command_queue(context, devices[0]);
  xk_gpu_set_buffer(queue, input, in[0], w*h);
  xk_gpu_append_buffer_arg(kernel, input, 0);
  xk_gpu_append_buffer_arg(kernel, output, 1);
  
  xk_gpu_run_kernel2D(kernel, queue, w , h);
  xk_gpu_get_buffer(queue, output, out[0], w*h); 
  xk_write_pgm("toto.pgm", out, w, h);
  xk_write_pgm("titi.pgm", in, w, h);
  
  xk_gpu_command_queue_del(queue);
  xk_gpu_buffer_del(input);
  xk_gpu_buffer_del(output);
  xk_gpu_context_del(context);
  xk_gpu_devices_del(devices, nb_devices);
  xk_gpu_platforms_del(platforms, nb_platforms);
  xk_u8_matrix_del(in,0,0);
  xk_u8_matrix_del(out,0,0);
  return 0;
}
