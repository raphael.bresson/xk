#include <xk/gpu/image/kernels/sigma_delta.h>
#include <xk/gpu/common.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>

static uint32_t w = 0, h = 0;

uint8_t** sigma_delta_image(uint32_t i)
{
  char name[255];
  sprintf(name,"../data_test/car_3%03d.pgm", i);
  return xk_load_pgm(name, 0, 0, &w, &h);
}

void sigma_delta_image_write( uint32_t i
                            , uint8_t** in
                            , const char* suffix)
{
  char name[255];
  sprintf(name,"sigma_delta_%s_%03d.pgm", suffix, i);
  xk_write_pgm(name, in, w, h);
}

int main()
{
  uint8_t** I  = sigma_delta_image(0);
  uint8_t** M  = sigma_delta_image(0);
  uint8_t** O  = xk_u8_matrix(0, w, 0, h);
  uint8_t** V  = xk_u8_matrix(0, w, 0, h);
  uint8_t** E  = xk_u8_matrix(0, w, 0, h);
  memset(O[0], 0, w*h);
  memset(V[0], 0, w*h);
  memset(E[0], 0, w*h);
  uint32_t nb_platforms = 0, nb_devices = 0; 
  cl_platform_id* platforms = xk_gpu_platforms(&nb_platforms);
  cl_device_id*   devices   = xk_gpu_devices(platforms[0], &nb_devices);
  cl_context      context   = xk_gpu_context(platforms[0], devices, nb_devices);
  
  cl_program program = xk_gpu_program(context, devices, nb_devices, xk_gpu_sigma_delta_src, "-DN=4"); 
  cl_kernel kernel   = xk_gpu_kernel(program, "sigma_delta_gray");
  
  cl_mem I_buf = xk_gpu_buffer(context, I[0], w*h, CL_MEM_READ_WRITE);
  cl_mem M_buf = xk_gpu_buffer(context, M[0], w*h, CL_MEM_READ_WRITE);
  cl_mem O_buf = xk_gpu_buffer(context, O[0], w*h, CL_MEM_READ_WRITE);
  cl_mem V_buf = xk_gpu_buffer(context, V[0], w*h, CL_MEM_READ_WRITE);
  cl_mem E_buf = xk_gpu_buffer(context, E[0], w*h, CL_MEM_READ_WRITE);
  
  cl_command_queue queue = xk_gpu_command_queue(context, devices[0]);
  
  xk_gpu_set_buffer(queue, M_buf, M[0], w*h);
  xk_gpu_set_buffer(queue, O_buf, O[0], w*h);
  xk_gpu_set_buffer(queue, V_buf, V[0], w*h);
  xk_gpu_set_buffer(queue, E_buf, E[0], w*h);
  
  xk_gpu_append_buffer_arg(kernel, I_buf, 0);
  xk_gpu_append_buffer_arg(kernel, M_buf, 1);
  xk_gpu_append_buffer_arg(kernel, O_buf, 2);
  xk_gpu_append_buffer_arg(kernel, V_buf, 3);
  xk_gpu_append_buffer_arg(kernel, E_buf, 4);
  
  for(int i = 0 ; i < 200 ; i++)
  {
    I = sigma_delta_image(i);
    xk_gpu_set_buffer(queue, I_buf, I[0], w*h);
    xk_gpu_run_kernel2D(kernel, queue, w , h);
    xk_gpu_get_buffer(queue, E_buf, E[0], w*h); 
    sigma_delta_image_write(i, E, "gpu");
  }   
  xk_gpu_command_queue_del(queue);
  xk_gpu_buffer_del(I_buf);
  xk_gpu_buffer_del(M_buf);
  xk_gpu_buffer_del(O_buf);
  xk_gpu_buffer_del(V_buf);
  xk_gpu_buffer_del(E_buf);
  
  xk_gpu_context_del(context);
  xk_gpu_devices_del(devices, nb_devices);
  xk_gpu_platforms_del(platforms, nb_platforms);
  xk_u8_matrix_del(I,0,0);
  xk_u8_matrix_del(M,0,0);
  xk_u8_matrix_del(O,0,0);
  xk_u8_matrix_del(V,0,0);
  xk_u8_matrix_del(E,0,0);
  return 0;
}
