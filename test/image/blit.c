#include <xk/image/blit.h>
#include <xk/image/color.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/unit.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void draw_square_gray_u8( uint8_t** in
                        , uint8_t** out
                        , uint8_t val
                        , uint32_t posx
                        , uint32_t posy
                        , uint32_t w
                        , uint32_t h)
{
  uint32_t x,y;
  for(x = posx ; x < (posx + w) ; x++)
  {
    out[posy][x] = in[ posy ][x] | val; 
    out[posy+h][x] = in[posy+h][x] | val;
  }
  for(y = posy; y < (posy + h) ; y++)
  {
    out[y][posx] = in[y][posx] | val;
    out[y][posx+w] = in[y][posx+w] | val;
  }
}


int main(int argc, char** argv)
{
  uint8_t **in, **ing, **out0, **out1;
  uint32_t x,y,w,h;
  xkt_unit_args(argc,argv);
  in   = xk_load_ppm("../data_test/testin.ppm", 0, 0, &w, &h);
  ing  = xk_u8_matrix(0, w-1  , 0, h-1);
  out0 = xk_u8_matrix(0, w*3-1, 0, h-1);
  out1 = xk_u8_matrix(0, w*3-1, 0, h-1);
  draw_square_gray_u8(ing, ing, 0xFF, 50, 50, w-100, h-100);
  xk_blit_binary_to_rgb_scalar(in, ing, out0, 0x00, 0xFF, 0x00, w, h);
  xk_blit_binary_to_rgb       (in, ing, out1, 0x00, 0xFF, 0x00, w, h);
  xk_write_ppm("out_blit0.ppm", out0, w, h);
  xk_write_ppm("out_blit1.ppm", out1, w, h);
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      XKT_EQUALI(out0[y][x], out1[y][x]);
    }
  }
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(ing, 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
  return xkt_return("blit");
}
