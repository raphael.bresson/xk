#include <xk/image/color.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/unit.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char** argv)
{
  uint8_t **in, **out0, **out1;
  uint32_t x,y,w,h;
  #ifdef XK_ARMV7
  uint8_t **out2;
  #endif
  xkt_unit_args(argc,argv);
  in   = xk_load_ppm("../data_test/testin.ppm", 0, 0, &w, &h);
  out0 = xk_u8_matrix(0, w-1, 0, h-1);
  out1 = xk_u8_matrix(0, w-1, 0, h-1);
  #ifdef XK_ARMV7
  out2 = xk_u8_matrix(0, w-1, 0, h-1);
  #endif
  xk_rgb_to_gray_scalar(in, out0, w*3, h);
  xk_rgb_to_gray(in, out1, w*3, h);
  #ifdef XK_ARMV7
  xk_rgb_to_gray_asm(in, out2, w*3, h);
  xk_write_pgm("out2.pgm", out2, w, h);
  #endif
  xk_write_pgm("out0.pgm", out0, w, h);
  xk_write_pgm("out1.pgm", out1, w, h);
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      XKT_APPROX_EQUAL(out0[y][x], out1[y][x], 2);
      #ifdef XK_ARMV7
      XKT_APPROX_EQUAL(out0[y][x], out2[y][x], 2);
      #endif
    }
  }
  xk_u8_matrix_del(in, 0, 0);
  xk_u8_matrix_del(out0, 0, 0);
  xk_u8_matrix_del(out1, 0, 0);
  #ifdef XK_ARMV7
  xk_u8_matrix_del(out2, 0, 0);
  #endif
  return xkt_return("rgb_to_gray");
}
