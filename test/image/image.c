#include <xk/image/image.h>
#include <xk/image/matrix.h>

int main(int argc, char** argv)
{
  uint32_t w, h;
  uint8_t ** image;
  image = xk_load_pgm("../data_test/testin.pgm", 0, 0, &w, &h);
  xk_write_pgm("testout.pgm", image, w, h);
  xk_u8_matrix_del(image, 0, 0);
  
  image = xk_load_ppm("../data_test/testin.ppm", 0, 0, &w, &h);
  xk_write_ppm("testout.ppm", image, w, h);
  xk_u8_matrix_del(image, 0, 0);
  return 0;
}
