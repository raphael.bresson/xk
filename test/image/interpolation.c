#include <xk/image/interpolation.h>
#include <xk/image/image.h>
#include <xk/image/matrix.h>

int main()
{
  uint32_t w, h;
  float factor = 0.2;
  uint8_t ** image, **out;
  
  image = xk_load_pgm("../data_test/testin.pgm", 0, 0, &w, &h);
  out = xk_u8_matrix(0, w*factor-1, 0, h*factor-1);
  xk_bilinear_interpolation(image, out, w, h, factor);
  xk_write_pgm("test_bilinear_interpolation.pgm", out, w * factor, h * factor);
  xk_u8_matrix_del(image, 0, 0);
  xk_u8_matrix_del(out, 0, 0);
  
  image = xk_load_ppm("../data_test/testin.ppm", 0, 0, &w, &h);
  out = xk_u8_matrix(0, w*3*factor-1, 0, h*factor-1);
  xk_bilinear_interpolation_rgb(image, out, w, h, factor);
  xk_write_ppm("test_bilinear_interpolation.ppm", out, w * factor, h * factor);
  xk_u8_matrix_del(image, 0, 0);
  xk_u8_matrix_del(out, 0, 0);
  return 0;
}
