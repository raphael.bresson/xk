#include <xk/image/matrix.h>
#include <stdio.h>
int main(int argc, char** argv)
{
  uint8_t** mat = xk_u8_matrix(-1, 1, -1, 1);
  printf( "%d\t%d\t%d\n%d\t%d\t%d\n%d\t%d\t%d\n", mat[-1][-1], mat[-1][0], mat[-1][1], mat[0][-1], mat[0][0], mat[0][1], mat[1][-1], mat[1][0], mat[1][1]);
  xk_u8_matrix_del(mat,-1, -1);
  return 0;
}
