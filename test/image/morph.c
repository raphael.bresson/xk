#include <xk/image/morph.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/unit.h>
#include <stdio.h>
#include <string.h>

int with_padding()
{
  uint32_t w, h, x, y;
  uint8_t ** in, **iner, ** out0, ** out1, ** out2, ** out3;
  in   = xk_load_pgm("../data_test/testin.pgm", 16, 1, &w, &h);
  iner = xk_u8_matrix(-16, w+15, -1, h);
  memset(&iner[-1][-16], 1, (w + 32) * (h + 2) * sizeof(uint8_t));
  xk_read_pgm("../data_test/testin.pgm", iner, w, h);
  out0 = xk_u8_matrix(0, w-1, 0, h-1);
  out1 = xk_u8_matrix(0 , w-1 , 0, h-1);
  out2 = xk_u8_matrix(0, w-1, 0, h-1);
  out3 = xk_u8_matrix(0, w-1, 0, h-1);
  xk_u8_dilate_3x3_scalar(in, out0, w, h);
  xk_u8_dilate_3x3(in, out1, w, h);
  xk_u8_erode_3x3_scalar(iner, out2, w, h);
  xk_u8_erode_3x3(iner, out3, w, h);
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      XKT_EQUALI(out0[y][x], out1[y][x]);
      XKT_EQUALI(out2[y][x], out3[y][x]);
    }
  }
  xk_u8_matrix_del(in  , -16, -1);
  xk_u8_matrix_del(iner, -16, -1);
  xk_u8_matrix_del(out0,  0 ,  0);
  xk_u8_matrix_del(out1,  0 ,  0);
  xk_u8_matrix_del(out2,  0 ,  0);
  xk_u8_matrix_del(out3,  0 ,  0);
  return xkt_return("morph with padding (3x3)");
}

int without_padding()
{
  uint32_t w, h, x, y;
  uint8_t ** in, ** out0, ** out1, ** out2, ** out3;
  in   = xk_load_pgm("../data_test/testin.pgm", 0, 0, &w, &h);
  out0 = xk_u8_matrix(0, w-1, 0, h-1);
  out1 = xk_u8_matrix(0, w-1, 0, h-1);
  out2 = xk_u8_matrix(0, w-1, 0, h-1);
  out3 = xk_u8_matrix(0, w-1, 0, h-1);
  xk_u8_dilate_3x3_scalar_no_padding(in, out0, w, h);
  xk_u8_dilate_3x3_no_padding(in, out1, w, h);
  xk_u8_erode_3x3_scalar_no_padding(in, out2, w, h);
  xk_u8_erode_3x3_no_padding(in, out3, w, h);
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      XKT_EQUALI(out0[y][x], out1[y][x]);
      XKT_EQUALI(out2[y][x], out3[y][x]);
    }
  }
  xk_u8_matrix_del(in  ,  0,   0);
  xk_u8_matrix_del(out0,  0 ,  0);
  xk_u8_matrix_del(out1,  0 ,  0);
  xk_u8_matrix_del(out2,  0 ,  0);
  xk_u8_matrix_del(out3,  0 ,  0);
  return xkt_return("morph without padding (3x3)");
}

int with_padding5x5()
{
  uint32_t w, h, x, y;
  uint8_t ** in, **iner, ** out0, ** out1, ** out2, ** out3;
  in   = xk_load_pgm("../data_test/testin.pgm", 16, 2, &w, &h);
  iner = xk_u8_matrix(-16, w+15, -2, h+1);
  memset(&iner[-2][-16], 0xFF, (w + 32) * (h + 4) * sizeof(uint8_t));
  xk_read_pgm("../data_test/testin.pgm", iner, w, h);
  out0 = xk_u8_matrix(0, w-1, 0, h-1);
  out1 = xk_u8_matrix(0, w-1, 0, h-1);
  out2 = xk_u8_matrix(0, w-1, 0, h-1);
  out3 = xk_u8_matrix(0, w-1, 0, h-1);
  xk_u8_dilate_5x5_scalar(in, out0, w, h);
  xk_u8_dilate_5x5(in, out1, w, h);
  xk_u8_erode_5x5_scalar(iner, out2, w, h);
  xk_u8_erode_5x5(iner, out3, w, h);
  for(y = 0 ; y < h ; y++)
  {
    for(x = 0 ; x < w ; x++)
    {
      XKT_EQUALI(out0[y][x], out1[y][x]);
      XKT_EQUALI(out2[y][x], out3[y][x]);
    }
  }
  xk_write_pgm("dscalar.pgm", out0, w, h);
  xk_write_pgm("dsimd.pgm"  , out1, w, h);
  xk_write_pgm("escalar.pgm", out2, w, h);
  xk_write_pgm("esimd.pgm"  , out3, w, h);
  xk_u8_matrix_del(in  , -16, -2);
  xk_u8_matrix_del(iner, -16, -2);
  xk_u8_matrix_del(out0,  0 ,  0);
  xk_u8_matrix_del(out1,  0 ,  0);
  xk_u8_matrix_del(out2,  0 ,  0);
  xk_u8_matrix_del(out3,  0 ,  0);
  return xkt_return("morph with padding (5x5)");
}

int main(int argc, char** argv)
{
  int ret = 1;
  xkt_unit_args(argc,argv);
//   ret &= (with_padding() != 0);
//   ret &= (without_padding() != 0);
  ret &= (with_padding5x5() != 0);
  return ret;
}
