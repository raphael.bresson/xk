#include <xk/image/sigma_delta.h>
#include <xk/image/matrix.h>
#include <xk/image/image.h>
#include <xktest/unit.h>

#include <stdio.h>

#define W 320
#define H 240
#define BEG_IMG 0
#define END_IMG 30

#define N 2

uint8_t** sigma_delta_image(uint32_t i)
{
  char name[255];
  uint32_t w, h;
  sprintf(name,"../data_test/car_3%03d.pgm", i);
  return xk_load_pgm(name, 0, 0, &w, &h);
}

void sigma_delta_image_write( uint32_t i
                            , uint8_t** in
                            , const char* suffix)
{
  char name[255];
  sprintf(name,"sigma_delta_%s_%03d.pgm", suffix, i);
  xk_write_pgm(name, in, W, H);
}

int main(int argc, char** argv)
{
  uint32_t x, y, i;
  uint8_t ** in;
  xkt_unit_args(argc,argv);
  struct sigma_delta_config conf_scal, conf_simd;
  in = sigma_delta_image(BEG_IMG);
  xk_sigma_delta_config_init(&conf_scal,in,N, W, H);
  xk_sigma_delta_config_init(&conf_simd,in,N, W, H);
  for(i = BEG_IMG ; i < END_IMG ; i++)
  {
    in = sigma_delta_image(i);
    xk_sigma_delta_scalar(in, &conf_scal);
    xk_sigma_delta(in, &conf_simd);
    for(y = 0 ; y < H ; y++)
    {
      for(x = 0 ; x < W ; x++)
      {
        XKT_EQUALI(conf_scal.etq[y][x], conf_simd.etq[y][x]);
      }
    }
    sigma_delta_image_write(i, conf_scal.etq, "scalar");
    sigma_delta_image_write(i, conf_simd.etq, "simd");
  }
  xk_u8_matrix_del(in  , 0, 0);
  xk_sigma_delta_config_del(&conf_scal);
  xk_sigma_delta_config_del(&conf_simd);
  return xkt_return("sigma delta");
}
