#define _POSIX_C_SOURCE	199309L
#include <xktest/bench.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pcre.h>


#ifdef __AARCH64__
#define ARCH64
#elif defined __ARM_ARCH
// #define ARCH32
// static void init_perfcounters()
// {
//   int32_t value = 1;
//   value |= 2; 
//   value |= 4; 
//   value |= 8; 
//   value |= 16;
//   __asm__ __volatile__ ("mcr p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));
//   __asm__ __volatile__ ("mcr p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));
//   __asm__ __volatile__ ("mcr p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
// }
#elif defined(__x86_64__) || defined(__amd64__)
#define ARCH64
#else
#define ARCH32
#endif

static int print_headers_benchmark = 1;
static char report_file_name[255] = "";
static double _cpu_freq = 0;

void xkt_benchmark_args(int argc, char** argv)
{
  int i;
  print_headers_benchmark = 1;
  report_file_name[0] = '\0';
//   #ifdef __ARM_ARCH
//    init_perfcounters(); 
//   #endif
  
  for(i = 1 ; i < argc-1 ; i++)
  {
    if((strcmp(argv[i], "--print-headers") == 0) || (strcmp(argv[i], "-h") == 0))
      print_headers_benchmark = atoi(argv[i+1]);
    if((strcmp(argv[i], "--report-file") == 0) || (strcmp(argv[i], "-r") == 0))
      strcpy(report_file_name, argv[i+1]);
  }
}

int save_file()
{
  return (report_file_name != '\0');
}

void xkt_benchmark_init(int print_headers, const char* filename)
{
  print_headers_benchmark = print_headers;
  if(filename)
  {
    strcpy(report_file_name, filename);
  }
}

void xkt_bench_init(struct xkt_bench* benchmark, const char* name, uint64_t iter, uint64_t repeat, uint64_t data_size, uint64_t elem_size)
{
  
  benchmark->name = (char*)malloc(21 * sizeof(char));
  XK_ERROR(name, "XKTEST: BENCH: Cannot allocate bench name\n");
  strncpy(benchmark->name, name,20);
  benchmark->iter = iter;
  benchmark->repeat = repeat;
  benchmark->data_size = data_size;
  benchmark->elem_size = elem_size;
}

void xkt_bench_del(struct xkt_bench* benchmark)
{
  free(benchmark->name);
}

double xkt_get_cpu_freq()
{
  double cpu_freq = 0;
  const char* pattern = "^cpu MHz\\s*:\\s*(\\d+)";
  const char* error = NULL;
  int offset = 0;

  pcre* regex = pcre_compile( pattern
                            , PCRE_ANCHORED
                            , &error
                            , &offset
                            , NULL);
  XK_ERROR(regex, "XKTEST_ERROR: Cannot compile regular expression: %s [%d]\n", error, offset);
  FILE* proc_file = fopen("/proc/cpuinfo", "r");
  XK_ERROR(proc_file, "XKTEST_ERROR: Cannot open /proc/cpuinfo\n");
  char line[512];
  int res[10];
  while(fgets(line, 512, proc_file))
  {
      int res_count = pcre_exec(regex, 0, line, strlen(line), 0, 0, res, 10);
      if(res_count < 0)
        continue;
      const char* match = NULL;
      pcre_get_substring(line, res, res_count, 1, &(match));
      cpu_freq = atof(match);
      pcre_free_substring(match);
      break;
  }
  
  fclose(proc_file);
  pcre_free(regex);
  cpu_freq *= 1000000;
  return cpu_freq;
//   struct timespec sleep_time;
//   sleep_time.tv_sec = 1;
//   sleep_time.tv_nsec = 0;
//   double t0,t1;
//   xkt_cycles(t0);
//   while(nanosleep(&sleep_time, &sleep_time) != 0 && errno != EINTR);
//   xkt_cycles(t1);
//   return (t1 - t0);
}

double cpu_frequency()
{
  if(_cpu_freq == 0)
    _cpu_freq = xkt_get_cpu_freq();
  return _cpu_freq;
}

void xkt_bench_update(struct xkt_bench* benchmark, double ticks)
{
  benchmark->ticks = ticks;
  benchmark->time = ticks / cpu_frequency();
  benchmark->data_rate = (benchmark->iter * benchmark->data_size) / benchmark->time;
  benchmark->elem_rate = (benchmark->iter * benchmark->elem_size) / benchmark->time;
}


void xkt_bench_print_header()
{
  if(print_headers_benchmark)
  {
    printf( COL_CYAN   "%20s" COL_RESET " | " COL_CYAN   "%9s"  COL_RESET " | "
            COL_CYAN   "%9s"  COL_RESET " | " COL_GREEN  "%10s" COL_RESET " | "
            COL_GREEN  "%11s" COL_RESET " | " COL_YELLOW "%14s" COL_RESET " | "
            COL_YELLOW "%14s" COL_RESET "\n"
          , "name"
          , "data size"
          , "elem size"
          , "cpu clocks"
          , "cpu time"
          , "data rate"
          , "elem rate");
  }
}

void xkt_bench_print_header_speedup()
{
  if(print_headers_benchmark)
  {
    printf( COL_CYAN   "%20s" COL_RESET " | " COL_CYAN   "%9s"  COL_RESET " | " 
            COL_CYAN   "%9s"  COL_RESET " | " COL_GREEN  "%10s" COL_RESET " | "
            COL_GREEN  "%10s" COL_RESET " | " COL_GREEN  "%11s" COL_RESET " | " 
            COL_GREEN  "%11s" COL_RESET " | " COL_YELLOW "%14s" COL_RESET " | "
            COL_YELLOW "%14s" COL_RESET " | " COL_YELLOW "%14s" COL_RESET " | "
            COL_YELLOW "%14s" COL_RESET " | " COL_RED    "%9s"  COL_RESET "\n"
          , "name"
          , "data size"
          , "elem size"
          , "ref clk0"
          , "test clk1"
          , "ref time"
          , "test time"
          , "ref drate"
          , "test drate"
          , "ref erate"
          , "test erate"
          , "speedup");
  }
}

void xkt_bench_csv_header(FILE* file_report)
{
  fprintf(file_report, "name;data size;elem size;ticks;time;data rate;elem rate\n");
}

void xkt_bench_csv_header_speedup(FILE* file_report)
{
  fprintf(file_report, "name;data size;elem size;ticks ref;ticks test;time ref;time test;data rate ref;data rate test;elem rate ref; elem_rate test\n");
}

#ifdef XK_ARCH32
void xkt_bench_print_infos(struct xkt_bench* benchmark)
{
  printf( COL_CYAN   "%20s"    COL_RESET " | " COL_CYAN   "%9llu" COL_RESET " | " COL_CYAN "%9llu" COL_RESET " | " 
          COL_GREEN  "%10.0lf" COL_RESET " | " COL_GREEN  "%.5e" COL_RESET " | "
          COL_YELLOW "%.8e"    COL_RESET " | " COL_YELLOW "%.8e" COL_RESET "\n"
        , benchmark->name
        , benchmark->data_size
        , benchmark->elem_size
        , benchmark->ticks
        , benchmark->time
        , benchmark->data_rate
        , benchmark->elem_rate);
}

void xkt_bench_print_infos_speedup(struct xkt_bench* benchmark0, struct xkt_bench* benchmark1)
{
  printf( COL_CYAN "%20s"     COL_RESET " | " COL_CYAN "%9llu"    COL_RESET " | " COL_CYAN "%9llu"  COL_RESET " | "
          COL_GREEN "%10.0lf" COL_RESET " | " COL_GREEN "%10.0lf" COL_RESET " | " COL_GREEN "%.5e"  COL_RESET " | " COL_GREEN "%.5e"  COL_RESET " | "
          COL_YELLOW "%.8e"   COL_RESET " | " COL_YELLOW "%.8e"   COL_RESET " | " COL_YELLOW "%.8e" COL_RESET " | " COL_YELLOW "%.8e" COL_RESET " | " 
          COL_RED    "%.3e" COL_RESET "\n"
        , benchmark0->name
        , benchmark0->data_size
        , benchmark0->elem_size
        , benchmark0->ticks
        , benchmark1->ticks
        , benchmark0->time
        , benchmark1->time
        , benchmark0->data_rate
        , benchmark1->data_rate
        , benchmark0->elem_rate
        , benchmark1->elem_rate
        , benchmark0->ticks / benchmark1->ticks);
}

void xkt_bench_csv_infos( struct xkt_bench* benchmark)
{
  if(report_file_name[0] != '\0')
  {
    FILE* file_report = fopen(report_file_name, "a+");
    XK_ERROR(file_report, "XKT UNIT: Cannot allocate test summary\n");
    if(ftell(file_report) == 0)
      xkt_bench_csv_header(file_report);
    fprintf( file_report
          , "%s;%llu;%llu;%lf;%lf;%lf;%lf\n"
          , benchmark->name
          , benchmark->data_size
          , benchmark->elem_size
          , benchmark->ticks
          , benchmark->time
          , benchmark->data_rate
          , benchmark->elem_rate);
    fclose(file_report);
  }
}

void xkt_bench_csv_infos_speedup( struct xkt_bench* benchmark0
                                , struct xkt_bench* benchmark1)
{
  if(report_file_name[0] != '\0')
  {
    FILE* file_report = fopen(report_file_name, "a+");
    XK_ERROR(file_report, "XKT UNIT: Cannot allocate test summary\n");
    if(ftell(file_report) == 0)
      xkt_bench_csv_header_speedup(file_report);
    fprintf( file_report
          , "%s;%llu;%llu;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf\n"
          , benchmark0->name
          , benchmark0->data_size
          , benchmark0->elem_size
          , benchmark0->ticks
          , benchmark1->ticks
          , benchmark0->time
          , benchmark1->time
          , benchmark0->data_rate
          , benchmark1->data_rate
          , benchmark0->elem_rate
          , benchmark1->elem_rate
          , benchmark0->ticks / benchmark1->ticks);
    fclose(file_report);
  }
}

#else

void xkt_bench_print_infos(struct xkt_bench* benchmark)
{
  printf( COL_CYAN   "%20s"    COL_RESET " | " COL_CYAN   "%9lu" COL_RESET " | " COL_CYAN "%9lu" COL_RESET " | " 
          COL_GREEN  "%10.0lf" COL_RESET " | " COL_GREEN  "%.5e" COL_RESET " | "
          COL_YELLOW "%.8e"    COL_RESET " | " COL_YELLOW "%.8e" COL_RESET "\n"
        , benchmark->name
        , benchmark->data_size
        , benchmark->elem_size
        , benchmark->ticks
        , benchmark->time
        , benchmark->data_rate
        , benchmark->elem_rate);
}

void xkt_bench_print_infos_speedup(struct xkt_bench* benchmark0, struct xkt_bench* benchmark1)
{
  printf( COL_CYAN "%20s"     COL_RESET " | " COL_CYAN "%9lu"     COL_RESET " | " COL_CYAN "%9lu"   COL_RESET " | "
          COL_GREEN "%10.0lf" COL_RESET " | " COL_GREEN "%10.0lf" COL_RESET " | " COL_GREEN "%.5e"  COL_RESET " | " COL_GREEN "%.5e"  COL_RESET " | "
          COL_YELLOW "%.8e"   COL_RESET " | " COL_YELLOW "%.8e"   COL_RESET " | " COL_YELLOW "%.8e" COL_RESET " | " COL_YELLOW "%.8e" COL_RESET " | " 
          COL_RED    "%.3e" COL_RESET "\n"
        , benchmark0->name
        , benchmark0->data_size
        , benchmark0->elem_size
        , benchmark0->ticks
        , benchmark1->ticks
        , benchmark0->time
        , benchmark1->time
        , benchmark0->data_rate
        , benchmark1->data_rate
        , benchmark0->elem_rate
        , benchmark1->elem_rate
        , benchmark0->ticks / benchmark1->ticks);
}

void xkt_bench_csv_infos( struct xkt_bench* benchmark)
{
  if(report_file_name[0] != '\0')
  {
    FILE* file_report = fopen(report_file_name, "a");
    XK_ERROR(file_report, "XKT UNIT: Cannot allocate test summary\n");
    fseek (file_report, 0, SEEK_END);
    if(ftell(file_report) == 0)
      xkt_bench_csv_header(file_report);
    fprintf( file_report
           , "%s;%lu;%lu;%lf;%lf;%lf;%lf\n"
           , benchmark->name
           , benchmark->data_size
           , benchmark->elem_size
           , benchmark->ticks
           , benchmark->time
           , benchmark->data_rate
           , benchmark->elem_rate);
    fclose(file_report);
  }
}

void xkt_bench_csv_infos_speedup( struct xkt_bench* benchmark0
                                , struct xkt_bench* benchmark1)
{
  if(report_file_name[0] != '\0')
  {
    FILE* file_report = fopen(report_file_name, "a");
    XK_ERROR(file_report, "XKT UNIT: Cannot allocate test summary\n");
    if(ftell(file_report) == 0)
      xkt_bench_csv_header_speedup(file_report);
    fprintf( file_report
          , "%s;%lu;%lu;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf;%lf\n"
          , benchmark0->name
          , benchmark0->data_size
          , benchmark0->elem_size
          , benchmark0->ticks
          , benchmark1->ticks
          , benchmark0->time
          , benchmark1->time
          , benchmark0->data_rate
          , benchmark1->data_rate
          , benchmark0->elem_rate
          , benchmark1->elem_rate
          , benchmark0->ticks / benchmark1->ticks);
    fclose(file_report); 
  }
}

#endif


