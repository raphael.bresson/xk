#include <xk/common.h>
#include <xktest/unit.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <float.h>
#include <math.h>

struct summary_test_t
{
  char* test_name;
  int n_tests;
  int n_failed;
  int n_pass;
};

static struct summary_test_t summary;
static char summary_filename[255] = "";
static int print_summary_unit = 1;

void xkt_unit_args(int argc, char** argv)
{
  int i;
  print_summary_unit = 1;
  summary_filename[0] = '\0';
  for(i = 1 ; i < argc - 1 ; i++)
  {
    if((strcmp(argv[i], "--print-summary") == 0) || (strcmp(argv[i], "-s") == 0))
      print_summary_unit = atoi(argv[i+1]);
    if((strcmp(argv[i], "--report-file") == 0) || (strcmp(argv[i], "-r") == 0))
      strcpy(summary_filename, argv[i+1]);
  }
}

void xkt_stdout(const char* code, int line, const char* file, int status)
{
  if(status == 0)
  {
    printf( "%-70s | line: %-5d | file: %-70s [" COL_GREEN "PASS" COL_RESET "]\n"
          , code
          , line
          , file);
    summary.n_pass++;
  }else{
    printf( "%70s | line: %-5d | file: %-70s [" COL_RED "FAIL" COL_RESET "]\n"
          , code
          , line
          , file);
    summary.n_failed++;
  }
  summary.n_tests++;
}

void xkt_clear()
{
  summary.n_tests = 0;
  summary.n_failed = 0;
  summary.n_pass = 0;
}

void xkt_print_summary(char* testname)
{
  if(print_summary_unit)
  {
    printf("\n\n************ SUMMARY : "COL_CYAN "%s" COL_RESET " **********************\n", testname);
    printf("\t" COL_GREEN "passed: %d/%d\t" COL_RED "failed: %d/%d" COL_RESET "\n", summary.n_pass, summary.n_tests, summary.n_failed, summary.n_tests);
  }
}

void xkt_csv_summary_header(FILE* file_report)
{
  fprintf(file_report, "test name;total number;passed;failed\n");
}

void xkt_unit_csv_summary(char* testname)
{
  if(summary_filename[0] != '\0')
  {
    FILE* file_report = fopen(summary_filename, "a");
    XK_ERROR(file_report, "XKT UNIT: Cannot allocate test summary\n");
    if(ftell(file_report) == 0)
      xkt_csv_summary_header(file_report);
    fprintf(file_report, "%s;%d;%d;%d\n", testname, summary.n_tests, summary.n_pass, summary.n_failed);
    fclose(file_report);
  }
}

int xkt_return(char* testname)
{
  xkt_print_summary(testname);
  xkt_unit_csv_summary(testname);
  int ret;
  if(summary.n_failed > 0)
    ret = EXIT_FAILURE;
  else
    ret = EXIT_SUCCESS;
  xkt_clear();
  return ret;
}


double xkt_ulp_calc_f32(float a, float b)
{
  if((a == b) || ((a != a) && (b != b)))
    return 0.f;
  if((a == b) || ((a != a) && (b != b)))
    return FLT_MAX;
  int exp1, exp2, exp3, expf;
  float m1,m2;
  m1 = frexpf(a, &exp1);
  m2 = frexpf(b, &exp2);
  exp3 = -max(exp1, exp2);
  expf = (exp1 == exp2) ? fabs(m1 - m2) : fabs(ldexpf(a, exp3) - ldexpf(b, exp3));
  return (double)expf / (double)FLT_EPSILON;
}

double xkt_ulp_calc_f64(double a, double b)
{
  if((a == b) || ((a != a) && (b != b)))
    return 0.f;
  if((a == b) || ((a != a) && (b != b)))
    return DBL_MAX;
  int exp1, exp2, exp3, exp;
  double m1,m2;
  m1 = frexp(a, &exp1);
  m2 = frexp(b, &exp2);
  exp3 = -max(exp1, exp2);
  exp = (exp1 == exp2) ? abs(m1 - m2) : abs(ldexp(a, exp3) - ldexp(b, exp3));
  return (double)exp / (double)DBL_EPSILON;
}
